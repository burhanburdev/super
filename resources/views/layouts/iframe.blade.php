<!DOCTYPE html>
<html>
  <!-- css here -->
  @include('layouts.head')
  <!-- /css here -->

  <body class="hold-transition sidebar-mini layout-fixed" data-panel-auto-height-mode="height">
  <div class="wrapper">
    <!-- Navbar -->
    @include('layouts.header')
    <!-- /.navbar -->

    @include('layouts.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper iframe-mode" data-widget="iframe" data-loading-screen="750">
      <!-- <div class="content-header"></div> -->

      <div class="nav navbar navbar-expand navbar-white navbar-light border-bottom p-0">
        <div class="nav-item dropdown">
          <a class="nav-link bg-danger dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Tutup</a>
          <div class="dropdown-menu mt-0">
            <a class="dropdown-item" href="#" data-widget="iframe-close" data-type="all">Tutup Semua</a>
            <a class="dropdown-item" href="#" data-widget="iframe-close" data-type="all-other">Tutup Tab Lain</a>
          </div>
        </div>
        <a class="nav-link bg-light" href="#" data-widget="iframe-scrollleft"><i class="fas fa-angle-double-left"></i></a>
        <ul class="navbar-nav overflow-hidden" role="tablist"></ul>
        <a class="nav-link bg-light" href="#" data-widget="iframe-scrollright"><i class="fas fa-angle-double-right"></i></a>
        <a class="nav-link bg-light" href="#" data-widget="iframe-fullscreen"><i class="fas fa-expand"></i></a>
      </div>
      
      <div class="tab-content">
        <div class="tab-empty">
          <h2 class="display-4">Tidak ada menu yang dipilih</h2>
        </div>
        <div class="tab-loading">
          <div>
            <h2 class="display-4">Harap menunggu <i class="fa fa-sync fa-spin"></i></h2>
          </div>
        </div>
      </div>

      <!-- Main content -->
      {{-- <section class="content">
        @yield('content')
      </section> --}}
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      @include('layouts.footer')
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  @include('layouts.javascript')
</body>
</html>