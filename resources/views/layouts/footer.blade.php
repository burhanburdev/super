	<strong>Copyright &copy; {{ date('Y') }} <a href="https://super.universitaspertamina.ac.id" target="_blank">@yield('title', config('app.name'))</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>