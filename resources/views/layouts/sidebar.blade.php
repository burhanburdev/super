  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{ route('home') }}" class="brand-link">
      <img src="{{ asset('assets/logo.png') }}" alt="@yield('title', config('app.name'))" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">@yield('title', config('app.name'))</span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('assets/dist/img/default.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ route('home') }}" class="d-block"><?= (Auth::check() ? Auth::user()->name : 'Administrator') ?></a>
        </div>
      </div>

      {{-- <!-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> --> --}}

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          <li class="nav-item has-treeview">
            <a href="{{ route('home') }}" class="nav-link @if (Request::is('home*') || Request::is('/')) active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Beranda
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview @if (Request::is('students*')) menu-open @endif">
            <a href="{{ route('students') }}" class="nav-link @if (Request::is('students*')) active @endif">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Peserta
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview @if (Request::is('courses*') || Request::is('exercises*') || Request::is('questions*')) menu-open @endif">
            <a href="{{ route('courses') }}" class="nav-link @if (Request::is('courses*') || Request::is('exercises*') || Request::is('questions*')) active @endif">
              <i class="nav-icon fab fa-discourse"></i>
              <p>
                Mata Kuliah
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview @if (Request::is('tournaments*')) menu-open @endif">
            <a href="{{ route('tournaments') }}" class="nav-link @if (Request::is('tournaments*')) active @endif">
              <i class="nav-icon fas fa-chess"></i>
              <p>
                Turnamen
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview @if (Request::is('groups*')) menu-open @endif">
            <a href="{{ route('groups') }}" class="nav-link @if (Request::is('groups*')) active @endif">
              <i class="nav-icon fas fa-layer-group"></i>
              <p>
                Kelompok
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview @if (Request::is('members*')) menu-open @endif">
            <a href="{{ route('leaves') }}" class="nav-link @if (Request::is('members*')) active @endif">
              <i class="nav-icon fas fa-user-check"></i>
              <p>
                Pengajuan Keluar
              </p>
            </a>
          </li>

          {{-- <!-- <li class="nav-item has-treeview @if (Request::is('questionbank*')) menu-open @endif">
            <a href="{{ route('questionbank') }}" class="nav-link @if (Request::is('questionbank*')) active @endif">
              <i class="nav-icon fab fa-docker"></i>
              <p>
                Bank Soal
              </p>
            </a>
          </li> --> --}}

          <li class="nav-item has-treeview 
          @if (Request::is('activities*')) menu-open @endif">
            <a href="#" class="nav-link 
            @if (Request::is('logs*')) active @endif">
              <i class="nav-icon fas fa-shoe-prints"></i>
              <p>
                Riwayat Pengguna
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('activities') }}" class="nav-link  @if (Request::is('activities/user*')) active @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Aktivitas Pengguna</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('exercise.activity') }}" class="nav-link  @if (Request::is('activities/exercise*')) active @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jawaban Latihan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('tournament.activity') }}" class="nav-link  @if (Request::is('activities/tournament*')) active @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jawaban Turnamen</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview @if (Request::is('reports*')) menu-open @endif">
            <a href="" class="nav-link @if (Request::is('reports*')) active @endif">
              <i class="nav-icon fas fa-print"></i>
              <p>
                Laporan
              </p>
            </a>
          </li>

        </ul>
      </nav>
    </div>
    
  </aside>