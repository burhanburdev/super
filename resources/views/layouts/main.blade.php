<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    @include('layouts.header')

    @include('layouts.sidebar')

    <div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <!-- <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Dashboard v2</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Dashboard v2</li>
              </ol>
            </div>
          </div> -->
        </div>
      </div>

      <section class="content">        
        <div class="container-fluid">
          @yield('content')
        </div>
      </section>
    </div>

    <footer class="main-footer">
      @include('layouts.footer')
    </footer>

  </div>

  @include('layouts.javascript')
</body>
</html>