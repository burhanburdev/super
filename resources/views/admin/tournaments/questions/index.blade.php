@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">

      <div class="card card-danger">
          <div class="card-header">
            <h3 class="card-title">SOAL NONAKTIF TURNAMEN</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <form action="{{ route('include.riddle') }}" method="POST">
            	@csrf

            	<div class="">
                    <input type="submit" class="btn btn-success" value="Aktifkan Soal">
                    

    					<div style="float: right;">						
    						<a href="{{ route('tournaments') }}" class="btn btn-info"><i class="fas fa-undo"></i> Kembali</a>
    					</div>
    			</div>

    			<br>

                <table id="deactive" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="width: 5%" class="center">No</th>
                      <th class="center">Sub-bab</th>
                      <th class="center">Soal</th>
                      <th class="center">Level</th>
                      <th class="center">Status</th>
    				  <th style="width: 5%" class="center"><input type="checkbox" id="checkAllInclude"></th>
                    </tr>
                  </thead>
                </table>
    			</form>
          </div>
        </div>

      <div class="card card-success">
          <div class="card-header">
            <h3 class="card-title">SOAL AKTIF TURNAMEN</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>

          <div class="card-body">
            <form action="{{ route('exclude.riddle') }}" method="POST">
            	@csrf
            	<div class="">
                    <input type="submit" class="btn btn-danger" value="Nonaktifkan Soal">
                    <input type="hidden" name="tournament_id" value="{{ $id }}">
    			</div>

    			<br>

                <table id="active" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="width: 5%" class="center">No</th>
                      <th class="center">Sub-bab</th>
                      <th class="center">Soal</th>
                      <th class="center">Level</th>
                      <th class="center">Status</th>
    				  <th style="width: 5%" class="center"><input type="checkbox" id="checkAllExclude"></th>
                    </tr>
                  </thead>
                </table>
    			</form>
          </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

  <script>
  	$(document).ready(function(){

	  // Check/Uncheck ALl
	  $('#checkAllInclude').change(function(){
	    if($(this).is(':checked')){
	      $('input[name="include[]"]').prop('checked',true);
	    }else{
	      $('input[name="include[]"]').each(function(){
	         $(this).prop('checked',false);
	      });
	    }
	  });

	  // Checkbox click
	  $('input[name="include[]"]').click(function(){
	    var total_checkboxes = $('input[name="include[]"]').length;
	    var total_checkboxes_checked = $('input[name="include[]"]:checked').length;

	    if(total_checkboxes_checked == total_checkboxes){
	       $('#checkAllInclude').prop('checked',true);
	    }else{
	       $('#checkAllInclude').prop('checked',false);
	    }
	  });

	   // Check/Uncheck ALl
	  $('#checkAllExclude').change(function(){
	    if($(this).is(':checked')){
	      $('input[name="exclude[]"]').prop('checked',true);
	    }else{
	      $('input[name="exclude[]"]').each(function(){
	         $(this).prop('checked',false);
	      });
	    }
	  });

	  // Checkbox click
	  $('input[name="exclude[]"]').click(function(){
	    var total_checkboxes = $('input[name="exclude[]"]').length;
	    var total_checkboxes_checked = $('input[name="exclude[]"]:checked').length;

	    if(total_checkboxes_checked == total_checkboxes){
	       $('#checkAllExclude').prop('checked',true);
	    }else{
	       $('#checkAllExclude').prop('checked',false);
	    }
	  });
	});

 //  	function checkBoxExclude() {
	// 	return '<input type="checkbox" name="exclude[]" value="">';
	// }

 //  	function checkBoxInclude() {
	// 	return '<input type="checkbox" name="include[]" value="">';
	// }

  	$('#active').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('ajax.question.active', [$id => $id]) !!}',
        // columnDefs: [
        //     {
        //       render: checkBoxInclude, 
        //       data: null, 
        //       class: 'center',
        //       targets: [0]
        //     }
        // ],
        columns: [
            { 
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                class: 'center',
                orderable: false,
                searchable: false
            },
            {
                data: 'subbab',
                name: 'subbab',
            },
            {
                data: 'question',
                name: 'question'
            },
            {
                data: 'level',
                name: 'level',
                class: 'center'
            },
            {
                data: 'active_status',
                name: 'active_status',
                class: 'center'
            },
            {
                data: 'action',
                name: 'action',                
                class: 'center',
                orderable: false,
                searchable: false
            }
        ]
    });
	
  	$('#deactive').DataTable({
        processing: true,
        serverSide: false,
        ajax: '{!! route('ajax.question.deactive', [$id => $id]) !!}',
        // columnDefs: [
        //     {
        //       render: checkBoxExclude, 
        //       data: null, 
        //       targets: [0]
        //     }
        // ],
        columns: [
            { 
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                class: 'center',
                orderable: false,
                searchable: false
            },
            {
                data: 'subbab',
                name: 'subbab',
            },
            {
                data: 'question',
                name: 'question'
            },
            {
                data: 'level',
                name: 'level',
                class: 'center'
            },
            {
                data: 'active_status',
                name: 'active_status',
                class: 'center'
            },
            {
                data: 'action',
                name: 'action',                
                class: 'center',
                orderable: false,
                searchable: false
            }
        ]
    });
  </script>
@endsection