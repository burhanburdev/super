        <div class="modal fade" id="modal-add-data">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Pengecualian Soal Turnamen</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              </div>

              <form method="POST" action="{{ route('exclude.riddle') }}">
                @csrf

              <div class="modal-body">
                <div class="form-group">
                  <div id="addQuestion">
                    <p>
                      <div class="input-group">
                        <div class="col-md-10">
                          <select class="form-control select2" name="question[]">                      
                            <option value=""> -- Pilih Pertanyaan -- </option>
                            @foreach($active as $act)
                              <option value="{{ $act->id }}">{{ substr(strip_tags($act->question_text), 0, 50) }} ...</option>
                            @endforeach
                          </select> &nbsp;
                        </div>

                        <div class="col-md-2">  
                          <label style="visibility: hidden;">a</label> 
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <input type="button" class="btn btn-xs btn-success" id="addrow" value="Tambah" />
                            </span>
                          </div>
                        </div>

                      </div>
                      </p>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <div class="form-group">
                  <button class="btn btn-primary" type="submit">Simpan</button>
                  <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
                </div>
              </div>

              </form>

            </div>
          </div>
        </div>