@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
      <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">SOAL TURNAMEN</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
          	{{--<a href="" class="btn btn-success" data-target="#modal-add-data" data-toggle="modal"><i class="fas fa-university"></i> &nbsp;Tambah Soal</a>
            @include('admin.questions.add_question')
            <br><br>--}}

            <table id="deactive" class="table table-bordered table-striped">
              <thead>
                <tr>
        				  <th style="width: 5%" class="center"><input type="checkbox" id="checkAllExclude"></th>
                  <th style="width: 5%" class="center">No</th>
                  <th class="center">Soal</th>
                  <th class="center">Level</th>
                  <th class="center"></th>
                </tr>
              </thead>
            </table>

            <table id="active" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 5%" class="center"><input type="checkbox" id="checkAllInclude"></th>
                  <th style="width: 5%" class="center">No</th>
                  <th class="center">Soal</th>
                  <th class="center">Level</th>
                  <th class="center"></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

  <script>
  	$(document).ready(function(){

	  // Check/Uncheck ALl
	  $('#checkAllInclude').change(function(){
	    if($(this).is(':checked')){
	      $('input[name="approval[]"]').prop('checked',true);
	    }else{
	      $('input[name="approval[]"]').each(function(){
	         $(this).prop('checked',false);
	      });
	    }
	  });

	  // Checkbox click
	  $('input[name="approval[]"]').click(function(){
	    var total_checkboxes = $('input[name="approval[]"]').length;
	    var total_checkboxes_checked = $('input[name="approval[]"]:checked').length;

	    if(total_checkboxes_checked == total_checkboxes){
	       $('#checkAllInclude').prop('checked',true);
	    }else{
	       $('#checkAllInclude').prop('checked',false);
	    }
	  });

	   // Check/Uncheck ALl
	  $('#checkAllExclude').change(function(){
	    if($(this).is(':checked')){
	      $('input[name="approval[]"]').prop('checked',true);
	    }else{
	      $('input[name="approval[]"]').each(function(){
	         $(this).prop('checked',false);
	      });
	    }
	  });

	  // Checkbox click
	  $('input[name="approval[]"]').click(function(){
	    var total_checkboxes = $('input[name="approval[]"]').length;
	    var total_checkboxes_checked = $('input[name="approval[]"]:checked').length;

	    if(total_checkboxes_checked == total_checkboxes){
	       $('#checkAllExclude').prop('checked',true);
	    }else{
	       $('#checkAllExclude').prop('checked',false);
	    }
	  });
	});

  	$('#active').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('tournaments') !!}',
        columns: [
            { 
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                class: 'center',
                orderable: false,
                searchable: false
            },
            {
                data: 'title',
                name: 'title'
            },
            {
                data: 'mulai',
                name: 'mulai'
            },
            {
                data: 'selesai',
                name: 'selesai'
            },
            {
                data: 'status',
                name: 'status',
                class: 'center'
            },
            {
                data: 'action',
                name: 'action',
                class: 'center',
                orderable: false,
                searchable: false
            }
        ]
    });
	
  	$('#deactive').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('tournaments') !!}',
        columns: [
            { 
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                class: 'center',
                orderable: false,
                searchable: false
            },
            {
                data: 'title',
                name: 'title'
            },
            {
                data: 'mulai',
                name: 'mulai'
            },
            {
                data: 'selesai',
                name: 'selesai'
            },
            {
                data: 'status',
                name: 'status',
                class: 'center'
            },
            {
                data: 'action',
                name: 'action',
                class: 'center',
                orderable: false,
                searchable: false
            }
        ]
    });
  </script>
@endsection