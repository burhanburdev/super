@extends('layouts.main')

@section('content')
<link href="{{ asset('assets/plugins/bootstrap-datepicker/datepicker.css') }}" rel="stylesheet" type="text/css" />

<div class="container-fluid">
  <div class="row">
  	<div class="col-md-12">
  		<div class="card">
  			<form method="POST" action="{{ route('update.tournament', $data->id) }}">
  			@csrf
  			@method('PUT')
  				<div class="card-body">
  					<label>Judul Turnamen</label>
					<div class="form-group">
						<input type="text" name="title" class="form-control" value="{{ $data->title }}" required>
					</div>

  					<label>Mulai</label>
	                <div class="form-group">
	                  <div class="input-group">
	                    <div class="input-group-prepend">
	                          <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
	                        </div>
	                    <input type="text" class="form-control datepicker5" name="start_date" data-date-format="yyyy-mm-dd" value="{{ $data->start_date }}" required>
	                  </div>                  
	                </div>

	                <label>Selesai</label>
	                <div class="form-group">
	                  <div class="input-group">
	                    <div class="input-group-prepend">
	                          <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
	                        </div>
	                    <input type="text" class="form-control datepicker5" name="end_date" data-date-format="yyyy-mm-dd" value="{{ $data->end_date }}" required>
	                  </div>
	                </div>

  					<label>Jumlah Soal</label>
					<div class="form-group">
						<input type="number" name="question" class="form-control" value="{{ $data->question }}" required>
					</div>

					<label>Status</label>
					<div class="form-group">
						<input type="checkbox" id="is_active" name="is_active" @if ($data->is_active) checked @endif data-bootstrap-switch data-off-color="danger" data-on-color="success">
					</div>
  				</div>

  				<div class="card-footer">
  					<div class="form-group" style="float: right;">
		              <button class="btn btn-primary" type="submit">Simpan</button>
		              <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
		            </div>
  				</div>
  			</form>
  		</div>
  	</div>
  </div>
</div>

<script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
	$(function() {
		$("input[data-bootstrap-switch]").each(function(){
	      $(this).bootstrapSwitch('state', 
	      	$(this).prop('checked')
	      );
	    });

	    $(".datepicker5").datepicker({
	      autoclose: true,
	      // todayHighlight: true
	    });
	});
</script>
@endsection