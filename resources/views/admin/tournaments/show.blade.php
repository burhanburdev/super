@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link href="{{ asset('assets/plugins/bootstrap-datepicker/datepicker.css') }}" rel="stylesheet" type="text/css" />
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
      <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">KELOMPOK TURNAMEN {{ strtoupper($data->title) }}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
          	<a href="" class="btn btn-success" data-target="#modal-add-data" data-toggle="modal"><i class="fas fa-university"></i> &nbsp;Tambah Kelompok</a>
            <div style="float: right;">           
              <a href="{{ route('tournaments') }}" class="btn btn-info"><i class="fas fa-undo"></i> Kembali</a>
            </div>
            @include('admin.tournaments.add_group')
            <br><br>

            <table id="groups" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 5%" class="center">No</th>
                  <th class="center">Kode</th>
                  <th class="center">Kelompok</th>
                  <th class="center">Total Poin</th>
                  <th class="center">Anggota</th>
                  <th class="center"></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>

  <script type="text/javascript">
  	var counter = 0;

	$("#addrow").on("click", function () {
	  var newRow = $("<p>");
	  var cols = "";

	  cols += '<div class="input-group"> <div class="col-md-10"><select class="form-control select2" name="group[]"> <option value=""> -- Pilih Kelompok -- </option> @foreach($dropdownGroups as $gr) <option value="{{ $gr->id }}">{{ $gr->code }} - {{ $gr->name }}</option> @endforeach </select> &nbsp; </div> <div class="col-md-2"> <div class="input-group-prepend"> <span class="input-group-text"> <input type="button" class="btn btn-xs btn-danger ibtnDel" value="Hapus" /> </span> </div> </div> </div>';

	  cols += '<script>$(".select2").select2({ theme: "bootstrap4" });<' + '/' + 'script>';

	  newRow.append(cols);
	  $("#addGroup").append(newRow);
	  counter++;
	});

	$("#addGroup").on("click", ".ibtnDel", function (event) {
	  $(this).closest("p").remove();       
	  counter -= 1
	});

  $(document).ready(function(){
  	$(".datepicker5").datepicker({
	      autoclose: true,
	      // todayHighlight: true
	    });
  	
    $('#groups').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('show.tournament', [$id => $id]) !!}',
        columns: [
            { 
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                class: 'center',
                orderable: false,
                searchable: false
            },
            {
                data: 'group_code',
                name: 'group_code'
            },
            {
                data: 'group_name',
                name: 'group_name'
            },
            {
                data: 'total_point',
                name: 'total_point'
            },
            {
                data: 'members[</br>]',
                name: 'members',
            },
            {
                data: 'action',
                name: 'action',
                class: 'center',
                orderable: false,
                searchable: false
            }
        ]
    });
  });

  $(function () {
      $("input[data-bootstrap-switch]").each(function(){
	      $(this).bootstrapSwitch('state', $(this).prop('checked'));
	    })
  });
  </script>
@endsection