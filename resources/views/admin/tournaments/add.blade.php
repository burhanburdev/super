        <div class="modal fade" id="modal-add-data">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Tambah Turnamen</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              </div>

              <form method="POST" action="{{ route('store.tournament') }}">
                @csrf

              <div class="modal-body">
                <label>Nama Turnamen</label>
                <div class="form-group">
                  <input type="text" class="form-control" name="title" required>
                </div>

                <label>Mulai</label>
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                          <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                    <input type="text" class="form-control datepicker5" name="start_date" data-date-format="yyyy-mm-dd" required>
                  </div>                  
                </div>

                <label>Selesai</label>
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                          <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                    <input type="text" class="form-control datepicker5" name="end_date" data-date-format="yyyy-mm-dd" required>
                  </div>
                </div>

                <label>Jumlah Soal</label>
                <div class="form-group">
                  <input type="number" name="question" class="form-control" required>
                </div>

                <label>Apakah turnamen aktif?</label>
                <div class="form-group">
                  <input type="checkbox" name="is_active" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
                </div>
              </div>

              <div class="modal-footer">
                <div class="form-group">
                  <button class="btn btn-primary" type="submit">Simpan</button>
                  <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
                </div>
              </div>

              </form>

            </div>
          </div>
        </div>