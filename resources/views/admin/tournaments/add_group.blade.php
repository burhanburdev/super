        <div class="modal fade" id="modal-add-data">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Tambah Kelompok</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              </div>

              <form method="POST" action="{{ route('store.group.tournament') }}">
                @csrf

              <div class="modal-body">
                <div class="form-group">
                  <input type="hidden" name="tournament_id" value="{{ $id }}">
                  <div id="addGroup">
                    <p>
                      <div class="input-group">
                        <div class="col-md-10">
                          <label>Tambah Kelompok</label> 
                          <select class="form-control select2" name="group[]">                      
                            <option value=""> -- Pilih Kelompok -- </option>
                            @foreach($dropdownGroups as $gr)
                              <option value="{{ $gr->id }}">{{ $gr->code }} - {{ $gr->name }}</option>
                            @endforeach
                          </select> &nbsp;
                        </div>

                        <div class="col-md-2">  
                          <label style="visibility: hidden;">a</label> 
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <input type="button" class="btn btn-xs btn-success" id="addrow" value="Tambah" />
                            </span>
                          </div>
                        </div>

                      </div>
                      </p>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <div class="form-group">
                  <button class="btn btn-primary" type="submit">Simpan</button>
                  <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
                </div>
              </div>

              </form>

            </div>
          </div>
        </div>