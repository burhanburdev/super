@extends('layouts.main')

@section('css')
<style>
	fieldset.fieldset-border {
	    border: 1px groove #ddd !important;
	    padding: 0 1.4em 1.4em 1.4em !important;
	    margin: 0 0 1.5em 0 !important;
	    -webkit-box-shadow:  0px 0px 0px 0px #000;
	            box-shadow:  0px 0px 0px 0px #000;
	}

	legend.fieldset-border {
	    width:inherit; /* Or auto */
	    padding:0 10px; /* To give a bit of padding on the left and right */
	    border-bottom:none;
	}
</style>

<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
    	<div class="card card-primary">
    		<div class="card-header">
    			<h5>Tambah Pertanyaan</h5>
    		</div>

    		<form method="POST" action="{{ route('update.question', $data->id) }}" enctype="multipart/form-data">
    		@csrf
    		@method('PUT')

    		<input type="hidden" name="exercise_id" value="{{ $data->exercise_id }}">
    		<div class="card-body">
    			<div class="form-group">
    				<label>Kode Soal</label>
    				<input type="text" name="question_code" class="form-control" value="{{ $data->question_code }}">
    			</div>

    			<div class="form-group">
    				<label>Pertanyaan</label>
	    			<textarea id="question" class="form-control" name="question_text" rows="10" cols="50">{{ $data->question_text }}</textarea>
    			</div>

    			<div class="row">
    				<div class="col-md-6">
		    			<div class="form-group">
		    				<label>Gambar</label>
		    				<input style="border: none;" type="file" class="form-control" name="question_image" accept="image/png, image/jpg, image/jpeg">	
		    			</div>

		    			<div class="row">		    				
			    			<div class="col-md-4">
		    					<div class="form-group">
				    				<label>Level</label>
				    				<input type="number" class="form-control" min="0" name="level" value="{{ $data->level }}" required>	
				    			</div>		    				
			    			</div>

			    			<div class="col-md-4">
			    				<div class="form-group">
		    						<label>Apakah soal grup?</label>
		    						<br>
				                	<input type="checkbox" name="is_group" @if ($data->is_group) checked @endif data-bootstrap-switch data-off-color="danger" data-on-color="success">
				                </div>
			    			</div>

			    			<div class="col-md-4">
			    				<div class="form-group">
		    						<label>Apakah soal aktif?</label>
		    						<br>
				                	<input type="checkbox" name="is_active" @if ($data->is_active) checked @endif data-bootstrap-switch data-off-color="danger" data-on-color="success">
				                </div>
			    			</div>
		    			</div>
    				</div>

    				<div class="col-md-6">
    					@if ($data->question_image)
	    					<img style="width: 200px; height: auto;" src="{{ asset('storage/questions') }}/{{ $data->question_image }}">
    					@endif
    				</div>
    			</div>

    			<h6><strong>Sub Pertanyaan</strong></h6>

                  <div id="subquestions">
                    <div class="form-group">
                      <a href="" class="btn btn-xs btn-success" id="addrow"> + Tambah Sub Pertanyaan</a>
                    </div>

                    @php $no = 1; @endphp
                    @foreach ($data->subquestions as $sub)                    
                    <fieldset class="fieldset-border">
					  <legend class="fieldset-border">
					    <h5>Sub Pertanyaan {{ $no }}</h5>
					  </legend>
					  <div class="row">
					    <div class="col-md-8">
					      <input type="hidden" name="u_id[]" value="{{ $sub->id }}">
					      <textarea id="subquestion{{ $no }}" class="form-control" name="u_text[]" required>{{ $sub->text }}</textarea>
					    </div>
					    <div class="col-md-4">
					      <div class="row">
					        <div class="col-md-12">
					          <div class="form-group">
					            <div class="input-group-prepend">
					              <span class="input-group-text">
					                <a href="{{ route('destroy.subquestion', $sub->id) }}" class="btn btn-xs btn-danger" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')" /><i class="fa fa-trash"></i></a>
					              </span>
					            </div>
					          </div>
					        </div>
					      </div>
					      <div class="row">
					        <div class="col-md-12">
					          <div class="form-group">
					            <select class="form-control" name="u_answer[]" required>
					              <option value="">Pilih Jawaban Benar</option>
					              <option @if ($sub->answer == 'option_1') selected @endif value="option_1">Option 1</option>
					              <option @if ($sub->answer == 'option_2') selected @endif value="option_2">Option 2</option>
					              <option @if ($sub->answer == 'option_3') selected @endif value="option_3">Option 3</option>
					              <option @if ($sub->answer == 'option_4') selected @endif value="option_4">Option 4</option>
					            </select>
					          </div>
					        </div>
					      </div>
					    </div>
					  </div>
					  <br>
					  <div class="row">
					    <div class="col-md-6">
					      <div class="form-group">
					        <label>Option 1</label>
					        <textarea id="option_1{{ $no }}" name="u_option_1[]" required class="form-control">{{ $sub->option_1 }}</textarea>
					      </div>
					    </div>
					    <div class="col-md-6">
					      <div class="form-group">
					        <label>Option 2</label>
					        <textarea id="option_2{{ $no }}" name="u_option_2[]" required class="form-control">{{ $sub->option_2 }}</textarea>
					      </div>
					    </div>
					  </div>
					  <div class="row">
					    <div class="col-md-6">
					      <div class="form-group">
					        <label>Option 3</label>
					        <textarea id="option_3{{ $no }}" name="u_option_3[]" required class="form-control">{{ $sub->option_3 }}</textarea>
					      </div>
					    </div>
					    <div class="col-md-6">
					      <div class="form-group">
					        <label>Option 4</label>
					        <textarea id="option_4{{ $no }}" name="u_option_4[]" required class="form-control">{{ $sub->option_4 }}</textarea>
					      </div>
					    </div>
					  </div>
					</fieldset>

					  <script>
					  var subquestion{{ $no }} = CKEDITOR.replace("subquestion{{ $no }}", {
					    height: 75,
					    toolbarCanCollapse: true
					  });

					  subquestion{{ $no }}.on("required", function(evt) {
					    subquestion{{ $no }}.showNotification("Sub Pertanyaan tidak boleh kosong.", "warning");
					    evt.cancel();
					  });

					  var option_1{{ $no }} = CKEDITOR.replace("option_1{{ $no }}", {
					    height: 50,
					    toolbarCanCollapse: true
					  });

					  option_1{{ $no }}.on("required", function(evt) {
					    option_1{{ $no }}.showNotification("Option 1 tidak boleh kosong.", "warning");
					    evt.cancel();
					  });
					  
					  var option_2{{ $no }} = CKEDITOR.replace("option_2{{ $no }}", {
					    height: 50,
					    toolbarCanCollapse: true
					  });

					  option_2{{ $no }}.on("required", function(evt) {
					    option_2{{ $no }}.showNotification("Option 2 tidak boleh kosong.", "warning");
					    evt.cancel();
					  });
					  
					  var option_3{{ $no }} = CKEDITOR.replace("option_3{{ $no }}", {
					    height: 50,
					    toolbarCanCollapse: true
					  });

					  option_3{{ $no }}.on("required", function(evt) {
					    option_3{{ $no }}.showNotification("Option 3 tidak boleh kosong.", "warning");
					    evt.cancel();
					  });
					  
					  var option_4{{ $no }} = CKEDITOR.replace("option_4{{ $no }}", {
					    height: 50,
					    toolbarCanCollapse: true
					  });

					  option_4{{ $no }}.on("required", function(evt) {
					    option_4{{ $no }}.showNotification("Option 4 tidak boleh kosong.", "warning");
					    evt.cancel();
					  });
					  </script>
					@php $no++; @endphp
                    @endforeach
                  </div>
    		</div>

    		<div class="card-footer">
    			<div class="form-group" style="float: right;">
                    <button class="btn btn-primary" type="submit">Simpan</button>
                    <a href="{{ route('show.exercise', $data->exercise_id) }}" class="btn btn-default">Kembali</a>
				</div>
    		</div>
    		</form>
		</div>
	</div>
</div>
@endsection

@section('javascript')
	<script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <script>
    	$(function () {
			$("input[data-bootstrap-switch]").each(function(){
				$(this).bootstrapSwitch('state', $(this).prop('checked'));
			})
		});

      //   CKEDITOR.replace('question').on('required', function(evt) {
      //   	alert( 'Pertanyaan tidak boleh kosong.' );
		    // evt.cancel();
      //   });
        
        var ckeditor = CKEDITOR.replace('question');
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'notification, widget, dialog, mathjax';
        CKEDITOR.config.mathJaxLib = '{{ asset("assets/plugins/mathjax/MathJax.js") }}?config=TeX-AMS_HTML';

        var counter = 0;
        var increment = {{ $countSub }};

		$("#addrow").on("click", function (e) {
		e.preventDefault();
		var newRow = $("<p>");
		var cols = "";

		cols += '<fieldset class="fieldset-border"><legend class="fieldset-border"><h5>Sub Pertanyaan ' + increment +'</h5></legend><div class="row"> <div class="col-md-8"> <textarea id="subquestion' + counter +'" class="form-control" name="text[]" required></textarea> </div> <div class="col-md-4"> <div class="row"> <div class="col-md-12"> <div class="form-group"> <div class="input-group-prepend"> <span class="input-group-text"> <input type="button" class="btn btn-xs btn-danger ibtnDel" value="Hapus" /> </span> </div> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="form-group"> <select class="form-control" name="answer[]" required> <option value="">Pilih Jawaban Benar</option> <option value="option_1">Option 1</option> <option value="option_2">Option 2</option> <option value="option_3">Option 3</option> <option value="option_4">Option 4</option> </select> </div> </div> </div> </div> </div>';

		cols += '<br>'

		cols += '<div class="row"> <div class="col-md-6"> <div class="form-group"> <label>Option 1</label> <textarea id="option_1' + counter + '" name="option_1[]" required class="form-control"></textarea> </div> </div> <div class="col-md-6"> <div class="form-group"> <label>Option 2</label> <textarea id="option_2' + counter + '" name="option_2[]" required class="form-control"></textarea> </div> </div> </div> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label>Option 3</label> <textarea id="option_3' + counter + '" name="option_3[]" required class="form-control"></textarea> </div> </div> <div class="col-md-6"> <div class="form-group"> <label>Option 4</label> <textarea id="option_4' + counter + '" name="option_4[]" required class="form-control"></textarea> </div> </div> </div></fieldset>';

		cols += '<script>var subquestion' + counter + ' = CKEDITOR.replace("subquestion'+ counter +'", {height: 75, toolbarCanCollapse: true});';
		cols += 'var option_1' + counter + ' = CKEDITOR.replace("option_1'+ counter +'", {height: 50, toolbarCanCollapse: true});';
		cols += 'var option_2' + counter + ' = CKEDITOR.replace("option_2'+ counter +'", {height: 50, toolbarCanCollapse: true});';
		cols += 'var option_3' + counter + ' = CKEDITOR.replace("option_3'+ counter +'", {height: 50, toolbarCanCollapse: true});';
		cols += 'var option_4' + counter + ' = CKEDITOR.replace("option_4'+ counter +'", {height: 50, toolbarCanCollapse: true});';

		cols += 'subquestion' + counter + '.on("required", function(evt) { subquestion' + counter + '.showNotification( "Sub Pertanyaan tidak boleh kosong.", "warning" ); evt.cancel(); });';
		cols += 'option_1' + counter + '.on("required", function(evt) { option_1' + counter +'.showNotification( "Option 1 tidak boleh kosong.", "warning" ); evt.cancel(); });';
		cols += 'option_2' + counter + '.on("required", function(evt) { option_2' + counter +'.showNotification( "Option 2 tidak boleh kosong.", "warning" ); evt.cancel(); });';
		cols += 'option_3' + counter + '.on("required", function(evt) { option_3' + counter +'.showNotification( "Option 3 tidak boleh kosong.", "warning" ); evt.cancel(); });';
		cols += 'option_4' + counter + '.on("required", function(evt) { option_4' + counter +'.showNotification( "Option 4 tidak boleh kosong.", "warning" ); evt.cancel(); });<' + '/' + 'script>';

		// cols += '<script>CKEDITOR.replace("subquestion'+ counter +'", {height: 75, toolbarCanCollapse: true}).on("required", function(evt) { alert( "Sub Pertanyaan tidak boleh kosong." ); evt.cancel(); });<' + '/' + 'script>';
		// cols += '<script>CKEDITOR.replace("option_1'+ counter +'", {height: 50, toolbarCanCollapse: true}).on("required", function(evt) { alert( "Option 1 tidak boleh kosong." ); evt.cancel(); });<' + '/' + 'script>';
		// cols += '<script>CKEDITOR.replace("option_2'+ counter +'", {height: 50, toolbarCanCollapse: true}).on("required", function(evt) { alert( "Option 2 tidak boleh kosong." ); evt.cancel(); });<' + '/' + 'script>';
		// cols += '<script>CKEDITOR.replace("option_3'+ counter +'", {height: 50, toolbarCanCollapse: true}).on("required", function(evt) { alert( "Option 3 tidak boleh kosong." ); evt.cancel(); });<' + '/' + 'script>';
		// cols += '<script>CKEDITOR.replace("option_4'+ counter +'", {height: 50, toolbarCanCollapse: true}).on("required", function(evt) { alert( "Option 4 tidak boleh kosong." ); evt.cancel(); });<' + '/' + 'script>';

		newRow.append(cols);
			$("#subquestions").append(newRow);
			counter++;
			increment++;
		});

		$("#subquestions").on("click", ".ibtnDel", function (event) {
			$(this).closest("p").remove();       
			counter -= 1
			increment -= 1
		});
    </script>
@endsection