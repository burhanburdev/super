@extends('layouts.main')

@section('css')
<style>
	fieldset.fieldset-border {
	    border: 1px groove #ddd !important;
	    padding: 0 1.4em 1.4em 1.4em !important;
	    margin: 0 0 1.5em 0 !important;
	    -webkit-box-shadow:  0px 0px 0px 0px #000;
	            box-shadow:  0px 0px 0px 0px #000;
	}

	legend.fieldset-border {
	    width:inherit; /* Or auto */
	    padding:0 10px; /* To give a bit of padding on the left and right */
	    border-bottom:none;
	}
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
    	<div class="card card-primary">
    		<div class="card-header">
    			<h5>Tambah Pertanyaan</h5>
    		</div>

    		<form method="POST" action="{{ route('store.question') }}" enctype="multipart/form-data">
    		@csrf
    		<input type="hidden" name="exercise_id" value="{{ $exerciseId }}">
    		<div class="card-body">
    			<div class="form-group">
    				<label>Kode Soal</label>
    				<input type="text" name="question_code" class="form-control">
    			</div>

    			<div class="form-group">
    				<label>Pertanyaan</label>
	    			<textarea id="question" class="form-control" name="question_text" rows="10" cols="50" required></textarea>
	    			<div id="mandatory">
	    				
	    			</div>
    			</div>

    			<div class="row">
    				<div class="col-md-6">
		    			<div class="form-group">
		    				<label>Gambar</label>
		    				<input style="border: none;" type="file" class="form-control" name="question_image" accept="image/png, image/jpg, image/jpeg">	
		    			</div>

		    			<div class="row">		    				
			    			<div class="col-md-6">
		    					<div class="form-group">
				    				<label>Level</label>
				    				<input type="number" min="0" class="form-control" name="level" required>	
				    			</div>		    				
			    			</div>

			    			<div class="col-md-6">
			    				<div class="form-group">
		    						<label>Apakah soal grup?</label>
		    						<br>
				                	<input type="checkbox" name="is_group" data-bootstrap-switch data-off-color="danger" data-on-color="success">
				                </div>
			    			</div>
		    			</div>
    				</div>

    				<div class="col-md-6">
    					
    				</div>
    			</div>

    			<h6><strong>Sub Pertanyaan</strong></h6>

                  <div id="subquestions">
                    <div class="form-group">
                      <a href="" class="btn btn-xs btn-success" id="addrow"> + Tambah Sub Pertanyaan</a>
                    </div>
                  </div>
    		</div>

    		<div class="card-footer">
    			<div class="form-group" style="float: right;">
                    <button class="btn btn-primary" type="submit">Simpan</button>
                    <a href="{{ route('show.exercise', $exerciseId) }}" class="btn btn-default">Kembali</a>
				</div>
    		</div>
    		</form>
		</div>
	</div>
</div>
@endsection

@section('javascript')
	<script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    <script>
    	$(function () {
			$("input[data-bootstrap-switch]").each(function(){
				$(this).bootstrapSwitch('state', $(this).prop('checked'));
			})
		});

        var ckeditor = CKEDITOR.replace('question');
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'notification, widget, dialog, mathjax';
        CKEDITOR.config.mathJaxLib = '{{ asset("assets/plugins/mathjax/MathJax.js") }}?config=TeX-AMS_HTML';

		// $('input[name=level]').on("keyup change", function(e) {
		// 	var level = $(this).val();

		// 	if (level == 3) {
		// 		var mandatory = '<script>ckeditor.on("required", function(evt) { ckeditor.showNotification( "Pertanyaan tidak boleh kosong.", "warning" ); evt.cancel(); });<' + '/' + 'script>';

		// 		$("#mandatory").html(mandatory);
		// 	} else {
		// 		$("#mandatory").remove();
		// 	}
		// })

        var counter = 0;
        var increment = 1;

		$("#addrow").on("click", function (e) {
		e.preventDefault();
		var newRow = $("<p>");
		var cols = "";

		cols += '<fieldset class="fieldset-border"><legend class="fieldset-border"><h5>Sub Pertanyaan ' + increment +'</h5></legend><div class="row"> <div class="col-md-8"> <textarea id="subquestion' + counter +'" class="form-control" name="text[]" required></textarea> </div> <div class="col-md-4"> <div class="row"> <div class="col-md-12"> <div class="form-group"> <div class="input-group-prepend"> <span class="input-group-text"> <input type="button" class="btn btn-xs btn-danger ibtnDel" value="Hapus" /> </span> </div> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="form-group"> <select class="form-control" name="answer[]" required> <option value="">Pilih Jawaban Benar</option> <option value="option_1">Option 1</option> <option value="option_2">Option 2</option> <option value="option_3">Option 3</option> <option value="option_4">Option 4</option> </select> </div> </div> </div> </div> </div>';

		cols += '<br>'

		cols += '<div class="row"> <div class="col-md-6"> <div class="form-group"> <label>Option 1</label> <textarea id="option_1' + counter + '" name="option_1[]" required class="form-control"></textarea> </div> </div> <div class="col-md-6"> <div class="form-group"> <label>Option 2</label> <textarea id="option_2' + counter + '" name="option_2[]" required class="form-control"></textarea> </div> </div> </div> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label>Option 3</label> <textarea id="option_3' + counter + '" name="option_3[]" required class="form-control"></textarea> </div> </div> <div class="col-md-6"> <div class="form-group"> <label>Option 4</label> <textarea id="option_4' + counter + '" name="option_4[]" required class="form-control"></textarea> </div> </div> </div></fieldset>';

		cols += '<script>var subquestion' + counter + ' = CKEDITOR.replace("subquestion'+ counter +'", {height: 75, toolbarCanCollapse: true});';
		cols += 'var option_1' + counter + ' = CKEDITOR.replace("option_1'+ counter +'", {height: 50, toolbarCanCollapse: true});';
		cols += 'var option_2' + counter + ' = CKEDITOR.replace("option_2'+ counter +'", {height: 50, toolbarCanCollapse: true});';
		cols += 'var option_3' + counter + ' = CKEDITOR.replace("option_3'+ counter +'", {height: 50, toolbarCanCollapse: true});';
		cols += 'var option_4' + counter + ' = CKEDITOR.replace("option_4'+ counter +'", {height: 50, toolbarCanCollapse: true});';

		cols += 'subquestion' + counter + '.on("required", function(evt) { subquestion' + counter + '.showNotification( "Sub Pertanyaan tidak boleh kosong.", "warning" ); evt.cancel(); });';
		cols += 'option_1' + counter + '.on("required", function(evt) { option_1' + counter +'.showNotification( "Option 1 tidak boleh kosong.", "warning" ); evt.cancel(); });';
		cols += 'option_2' + counter + '.on("required", function(evt) { option_2' + counter +'.showNotification( "Option 2 tidak boleh kosong.", "warning" ); evt.cancel(); });';
		cols += 'option_3' + counter + '.on("required", function(evt) { option_3' + counter +'.showNotification( "Option 3 tidak boleh kosong.", "warning" ); evt.cancel(); });';
		cols += 'option_4' + counter + '.on("required", function(evt) { option_4' + counter +'.showNotification( "Option 4 tidak boleh kosong.", "warning" ); evt.cancel(); });<' + '/' + 'script>';

		newRow.append(cols);
			$("#subquestions").append(newRow);
			counter++;
			increment++;
		});

		$("#subquestions").on("click", ".ibtnDel", function (event) {
			$(this).closest("p").remove();       
			counter -= 1
			increment -= 1
		});
    </script>
@endsection