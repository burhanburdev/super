@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link href="{{ asset('assets/plugins/bootstrap-datepicker/datepicker.css') }}" rel="stylesheet" type="text/css" />
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="modalMdTitle"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="modalError"></div>
              <div id="modalMdContent"></div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="modalEditTitle"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="modalError"></div>
              <div id="modalEditContent"></div>
          </div>
      </div>
  </div>
</div>

<div class="row">
	<div class="col-md-12">
      <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">{{ strtoupper($data->section->course->title) }} - {{ strtoupper($data->section->title) }} [{{ strtoupper($data->title) }}]</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
          	<a href="{{ route('create.question', ['exerciseId' => $id]) }}" class="btn btn-success"><i class="fas fa-university"></i> &nbsp;Tambah Soal</a>
            <div style="float: right;">           
              <a href="{{ route('courses') }}/{{ $data->section->course->id }}" class="btn btn-info"><i class="fas fa-undo"></i> Kembali</a>
            </div>
            <br><br>

            <table id="questions" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 5%" class="center">No</th>
                  <th class="center">Soal</th>
                  <th class="center">Level</th>
                  <th class="center">Grup</th>
                  <th class="center"></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>

  <script type="text/javascript">
  function createManageBtn() {
      return '<button id="manageBtn" type="button" onclick="myFunc()" class="btn btn-success btn-xs">Manage</button>';
  }

  function myFunc() {
      alert("Button was clicked!!!");
  }

  $(document).ready(function(){
  	$(".datepicker5").datepicker({
	      autoclose: true,
	      // todayHighlight: true
	    });
  	
    $('#questions').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('show.exercise', ['id' => $id]) !!}',
        // columnDefs: [
        //     {
        //       render: createManageBtn, 
        //       data: null, 
        //       targets: [0]
        //     }
        // ],
        columns: [
            { 
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                class: 'center',
                orderable: false,
                searchable: false
            },
            {
                data: 'text',
                name: 'text'
            },
            {
                data: 'level',
                name: 'level',
                class: 'center'
            },
            {
                data: 'group',
                name: 'group',
                class: 'center'
            },
            {
                data: 'action',
                name: 'action',
                class: 'center',
                orderable: false,
                searchable: false
            }
        ]
    });
  });
  </script>
@endsection