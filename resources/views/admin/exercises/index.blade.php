@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link href="{{ asset('assets/plugins/bootstrap-datepicker/datepicker.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('assets/plugins/ckeditor4/plugins/ckeditor_wiris/integration/WIRISplugins.js') }}?viewer=image"></script>
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="modalMdTitle"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="modalError"></div>
              <div id="modalMdContent"></div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="modalEditTitle"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="modalError"></div>
              <div id="modalEditContent"></div>
          </div>
      </div>
  </div>
</div>

<div class="row">
	<div class="col-md-12">
      <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">{{ strtoupper($subsection->section->course->title) }} - {{ strtoupper($subsection->section->title) }} [{{ strtoupper($subsection->title) }}]</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
          	<a href="" class="btn btn-success" data-target="#modal-add-data" data-toggle="modal"><i class="fas fa-university"></i> &nbsp;Tambah Latihan</a>
            <div style="float: right;">           
              <a href="{{ route('courses') }}/{{ $subsection->section->course->id }}" class="btn btn-info"><i class="fas fa-undo"></i> Kembali</a>
            </div>
            @include('admin.courses.exercises.add')
            <br><br>

            <table id="exercises" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 5%" class="center">No</th>
                  <th class="center">Judul Latihan</th>
                  <th class="center">Level</th>
                  <th class="center"></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
  <script src="{{ asset('assets/plugins/ckeditor4/ckeditor.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>


  <script type="text/javascript">
  {{--// var counter = 0;

  // $("#addrow").on("click", function () {
  //     var newRow = $("<p>");
  //     var cols = "";

  //     cols += '<div class="row"> <div class="col-md-8"> <textarea id="editor' + counter +'" class="form-control" name="question_text[]"></textarea> </div> <div class="col-md-4"> <div class="row"> <div class="input-group-prepend"> <span class="input-group-text"> <input type="button" class="btn btn-xs btn-danger ibtnDel" value="Hapus" /> </span> </div> </div> <div class="row"> <input type="radio" value="A"> A<input type="radio" value="A"> Kalium<input type="radio" value="A"> Natriu<minput type="radio" value="A"> D </div> </div> </div>';

  //     cols += '<script>CKEDITOR.replace("editor'+ counter +'");<' + '/' + 'script>';

  //     newRow.append(cols);
  //     $("#questions").append(newRow);
  //     counter++;
  // });

  // $("#questions").on("click", ".ibtnDel", function (event) {
  //     $(this).closest("p").remove();       
  //     counter -= 1
  // });
  --}}

  $(document).ready(function(){
  	$(".datepicker5").datepicker({
	      autoclose: true,
	      // todayHighlight: true
	    });
  	
    $('#exercises').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('exercises', ['subsectionId' => $subsectionId]) !!}',
        columns: [
            { 
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                class: 'center',
                orderable: false,
                searchable: false
            },
            {
                data: 'title',
                name: 'title'
            },
            {
                data: 'level',
                name: 'level',
                class: 'center'
            },
            {
                data: 'action',
                name: 'action',
                class: 'center',
                orderable: false,
                searchable: false
            }
        ]
    });
  });

  setInterval(function(){ 
    $('.modalMd').off('click').on('click', function () {
      $('#modalMd').modal({backdrop: 'static', keyboard: false}) 
      
        $('#modalMdContent').load($(this).attr('value'));
        $('#modalMdTitle').html($(this).attr('title'));

    });

    $('.modalEdit').off('click').on('click', function () {
      $('#modalEdit').modal({backdrop: 'static', keyboard: false}) 
      
        $('#modalEditContent').load($(this).attr('value'));
        $('#modalEditTitle').html($(this).attr('title'));
    });
  }, 500);
  </script>
@endsection