@extends('layouts.main')

@section('css')
<script src="{{ asset('assets/plugins/ckeditor4/plugins/ckeditor_wiris/integration/WIRISplugins.js') }}?viewer=image"></script>
@endsection

@section('content')
    <textarea id="editor" class="form-control" name="editor" rows="10" cols="50"></textarea>
@endsection

@section('javascript')
    <script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    <!-- <script src="{{ asset('assets/plugins/ckeditor4/ckeditor.js') }}"></script> -->
    <script>
        CKEDITOR.replace('editor');
        
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'widget, dialog, mathjax';
        CKEDITOR.config.mathJaxLib = '{{ asset("assets/plugins/mathjax/MathJax.js") }}?config=TeX-AMS_HTML';
    </script>
@endsection