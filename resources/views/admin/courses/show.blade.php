@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link href="{{ asset('assets/plugins/bootstrap-datepicker/datepicker.css') }}" rel="stylesheet" type="text/css" />
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="modalEditTitle"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="modalError"></div>
              <div id="modalEditContent"></div>
          </div>
      </div>
  </div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-primary">
				<div class="card-header">
					<h5>Mata Kuliah {{ $data->title }}</h5>
				</div>

				<div class="card-body">					
					<a href="" class="btn btn-success" data-target="#modal-add-data" data-toggle="modal"><i class="fas fa-university"></i> &nbsp;Tambah Bab</a>
					<div style="float: right;">						
						<a href="{{ route('courses') }}" class="btn btn-info"><i class="fas fa-undo"></i> Kembali</a>
					</div>
		            @include('admin.sections.add')
		            <br><br>

		            <table id="sections" class="table table-bordered table-striped">
		              <thead>
		                <tr>
		                  <th style="width: 5%" class="center">No</th>
		                  <th class="center">Bab</th>
		                  <th class="center">Latihan</th>
		                  <th class="center">Level</th>
		                  <th class="center"></th>
		                </tr>
		              </thead>
		              <tbody>
		              	@php $no = 1; @endphp
		              	@foreach($data->sections as $section)
		              		<tr>
		              			<td class="center">{{ $no }}</td>
		              			<td>{{ $section->title }}</td>
		              			<td>
		              				<ul>
		              				@foreach($section->exercises as $exe)
		              					<li><a href="{{ route('show.exercise', $exe->id) }}">{{ $exe->title }} (Level {{ $exe->level }})</a></li>
									@endforeach
									</ul>
		              			</td>
		              			<td class="center">{{ $section->level }}</td>
		              			<td class="center">
		              				<a href="#" value="{{ route('edit.section', $section->id) }}" class="btn btn-info btn-xs modalEdit" title="Ubah Data" data-toggle="modal" data-target="#modalEdit"><span class="fas fa-pencil-alt"></span></a>
		              				<form style="display: inline;" method="POST" action="{{ route('destroy.section', $section->id) }}" onsubmit="return confirm('Apakah Anda yakin akan menghapus data ini?')">
		                              @csrf
		                              @method('DELETE')
		                              <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
		                            </form>
		              			</td>
		              		</tr>
		              	@php $no++ @endphp
		              	@endforeach
		              </tbody>
		            </table>
				</div>

				<div class="card-footer">
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

  <script type="text/javascript">
		$(document).ready(function(){
			$('#sections').DataTable();
		});

		setInterval(function(){
			$('.modalEdit').off('click').on('click', function () {
				$('#modalEdit').modal({backdrop: 'static', keyboard: false}) 
			  
				$('#modalEditContent').load($(this).attr('value'));
				$('#modalEditTitle').html($(this).attr('title'));

			});
		}, 500);

		var counter = 0;

		$("#addrow").on("click", function () {
		    var newRow = $("<p>");
		    var cols = "";

		    cols += '<div class="row"><div class="col-md-5"> <input type="text" class="form-control" name="exercise_title[]" placeholder="Latihan" required> </div> <div class="col-md-2"> <input type="number" class="form-control" min="0" name="exercise_level[]" placeholder="Level" required> </div> <div class="col-md-3"> <input type="number" class="form-control" min="0" name="exercise_question[]" placeholder="Jumlah Pertanyaan" required> </div> <div class="col-md-2"> <div class="input-group-prepend"> <span class="input-group-text"> <input type="button" class="btn btn-xs btn-danger ibtnDel" value="Hapus" /> </span> </div> </div> </div>';

		    newRow.append(cols);
		    $("#exercises").append(newRow);
		    counter++;
		});

		$("#exercises").on("click", ".ibtnDel", function (event) {
		    $(this).closest("p").remove();       
		    counter -= 1
		});
  </script>
@endsection