@extends('layouts.main')

@section('content')
<link href="{{ asset('assets/plugins/bootstrap-datepicker/datepicker.css') }}" rel="stylesheet" type="text/css" />

<div class="container-fluid">
  <div class="row">
  	<div class="col-md-12">
  		<div class="card">
  			<form method="POST" action="{{ route('update.course', $data->id) }}">
  			@csrf
  			@method('PUT')
  				<div class="card-body">
  					<label>Mata Kuliah</label>
					<div class="form-group">
					<input type="text" name="title" class="form-control" value="{{ $data->title }}" required>
					</div>
  				</div>

  				<div class="card-footer">
  					<div class="form-group" style="float: right;">
		              <button class="btn btn-primary" type="submit">Simpan</button>
		              <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
		            </div>
  				</div>
  			</form>
  		</div>
  	</div>
  </div>
</div>

<script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
	$(function() {
		$("input[data-bootstrap-switch]").each(function(){
	      $(this).bootstrapSwitch('state', 
	      	$(this).prop('checked')
	      );
	    });

	    $(".datepicker5").datepicker({
	      autoclose: true,
	      // todayHighlight: true
	    });
	});
</script>
@endsection