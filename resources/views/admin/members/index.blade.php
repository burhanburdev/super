@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">
      <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">PENGAJUAN KELUAR KELOMPOK</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <form action="{{ route('leaves.approval') }}" method="POST">
              @csrf

              <input type="submit" class="btn btn-success" name="approval" value="APPROVE">
              <input type="submit" class="btn btn-danger" name="approval" value="REJECT">
              <br><br>

              <table id="table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="width: 5%" class="center">No</th>
                    <th class="center">Nomor Induk</th>
                    <th class="center">Nama</th>
                    <th class="center">Kelompok</th>
                    <th class="center">Tanggal Pengajuan</th>
                    <th style="width: 5%" class="center">
                      <input type="checkbox" id="checkAll">
                    </th>
                  </tr>
                </thead>
              </table>
            </form>
          </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

  <script>
    $(document).ready(function(){

      // Check/Uncheck ALl
      $('#checkAll').change(function() {
        if($(this).is(':checked')){
          $('input[name="checkBox[]"]').prop('checked',true);
        }else{
          $('input[name="checkBox[]"]').each(function(){
             $(this).prop('checked',false);
          });
        }
      });

      // Checkbox click
      $('input[name="checkBox[]"]').click(function() {
        var total_checkboxes = $('input[name="checkBox[]"]').length;
        var total_checkboxes_checked = $('input[name="checkBox[]"]:checked').length;

        if(total_checkboxes_checked == total_checkboxes){
           $('#checkAll').prop('checked',true);
        }else{
           $('#checkAll').prop('checked',false);
        }
      });
    });

    $('#table').DataTable({
        processing: true,
        serverSide: false,
        ajax: '{!! route('leaves') !!}',
        columns: [
            { 
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                class: 'center',
                orderable: false,
                searchable: false
            },
            {
                data: 'nim',
                name: 'nim',
            },
            {
                data: 'nama',
                name: 'nama'
            },
            {
                data: 'kelompok',
                name: 'kelompok',
            },
            {
                data: 'tanggal',
                name: 'tanggal',
                class: 'center'
            },
            {
                data: 'action',
                name: 'action',                
                class: 'center',
                orderable: false,
                searchable: false
            }
        ]
    });
  </script>
@endsection