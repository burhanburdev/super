@extends('layouts.main')

@section('content')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<style>
  .center {
    text-align: center;
  }

  .modal-xl {
    max-width: 1240px;
  }
</style>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
        	<div class="row">
        		<div class="col-md-6">
        			<div class="form-group row">
			            <label style="font-weight: 400" for="inputName" class="col-sm-3 col-form-label">Kode Grup</label>
			              <div class="col-sm-9">
			                <label style="border: 0; color: black;" class="form-control">
			                  {{ $data->code }}
			                </label>
			              </div>
			          </div>

			          <div class="form-group row">
			            <label style="font-weight: 400" for="inputName" class="col-sm-3 col-form-label">Nama Grup</label>
			              <div class="col-sm-9">
			                <label style="border: 0; color: black;" class="form-control">
			                  {{ $data->name }}
			                </label>
			              </div>
			          </div>
			          
			          <div class="form-group row">
			            <label style="font-weight: 400" for="inputName" class="col-sm-3 col-form-label">Total Poin</label>
			              <div class="col-sm-9">
			                <label style="border: 0; color: black;" class="form-control">
			                  {{ $data->total_point }}
			                </label>
			              </div>
			          </div>
        		</div>

        		<div class="col-md-6">
        			<div class="row">        				
	        			@foreach($members as $mem)
	        			<div class="col-md-4">
	        				<div class="center">
	        					<img class="profile-user-img img-fluid img-circle"
				                 src="
				                 @if ($mem->student->avatar && $mem->student->avatar != 'default.png')
				                   {{ asset('storage/photo') }}/{{ $mem->student->avatar }}
				                 @else
				                   {{ asset('assets/dist/img/default.png') }}
				                 @endif
				                 ">
				                 <br>
				                 <p><strong>{{ $mem->student->nama }}</strong></p>
	        				</div>
	        			</div>
	        			@endforeach
        			</div>
        		</div>
        	</div>

          <div class="row">
          	<div class="col-md-6">
          		<table class="table table-bordered table-striped">
		         	<thead>
		         		<tr>
		         			<th style="width: 5%" class="center">No</th>
							<th class="center">Turnamen</th>
							<th class="center">Status</th>
		         		</tr>
		         	</thead>
		         	<tbody>
		          		@php $no = 1; @endphp
		         		@foreach($tournaments as $tour)
		         			<tr>
			          			<td class="center">{{ $no }}</td>
		         				<td>{{ $tour->tournaments->title }}</td>
		         				<td>@if ($tour->tournaments->is_active) Aktif @else Non aktif @endif</td>
		         			</tr>
			          		@php $no++ @endphp
		         		@endforeach
		         	</tbody>
		          </table>
          	</div>

          	<div class="col-md-6">
          		<table class="table table-bordered table-striped">
		          	<thead>
		          		<tr>
		          			<th style="width: 5%" class="center">No</th>
							<th class="center">Nama Anggota</th>
							<th class="center">Poin</th>
							<th class="center">Jabatan</th>
							<th class="center"></th>
		          		</tr>
		          	</thead>
		          	<tbody>
		          		@php $no = 1; @endphp
		          		@foreach($members as $member)
		          		<tr>
		          			<td class="center">{{ $no }}</td>
		          			<td>{{ $member->student->nama }}</td>
		          			<td>{{ getTournamentMemberPoint($member->nim) }}</td>
		          			<td>@if ($member->is_leader) Ketua @else Anggota @endif</td>
		          			<td class="center">
		          				<form style="display: inline;" method="POST" action="{{ route('kick.member', $member->id) }}" onsubmit="return confirm('Apakah Anda yakin akan menghapus data ini?')">
			                      @csrf
			                      @method('DELETE')
			                      <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
			                    </form>
		          			</td>
		          		</tr>
		          		@php $no++ @endphp
		          		@endforeach
		          	</tbody>
		          </table>
          	</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script>
  	$('#members').DataTable();
  	$('#tournaments').DataTable();
  </script>
@endsection