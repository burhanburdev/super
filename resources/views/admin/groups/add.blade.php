        <div class="modal fade" id="modal-add-data">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Tambah Grup</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              </div>

              <form method="POST" action="{{ route('store.group') }}">
                @csrf

                <div class="modal-body">
                  <label>Kode Grup</label>
                  <div class="form-group">
                    <input type="text" class="form-control" name="code" id="code" required>
                    <a href="" id="random">Kode Acak</a>
                  </div>

                  <label>Nama Grup</label>
                  <div class="form-group">
                    <input type="text" class="form-control" name="name" required>
                  </div>

                  <label>Ketua Grup</label>                  
                  <div class="form-group">
                    <select name="nim" class="form-control select2" required>
                      <option value=""> -- Pilih Ketua Grup --</option>
                      @foreach($students as $student)
                        <option value="{{ $student->nim }}">{{ $student->nim.' - '.$student->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="modal-footer">
                  <div class="form-group">
                    <button class="btn btn-primary" type="submit">Simpan</button>
                    <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
                  </div>
                </div>

              </form>

            </div>
          </div>
        </div>