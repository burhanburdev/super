@extends('layouts.main')

@section('content')
<style>
  .modal-xl {
    max-width: 1240px;
  }
</style>

<div class="container-fluid">
  <div class="row">

    <div class="col-md-12">
      <div class="card">
        <form method="POST" action="{{ route('update.group', $data->id) }}" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <div class="card-body">

            <label>Kode Grup</label>
            <div class="form-group">
              <input type="text" name="code" class="form-control" value="{{ $data->code }}" required>
            </div>

            <label>Nama Grup</label>
            <div class="form-group">
              <input type="text" name="name" class="form-control" value="{{ $data->name }}" required>
            </div>
          </div>

          <div class="card-footer">
            <div class="form-group" style="float: right;">
              <button class="btn btn-primary" type="submit">Simpan</button>
              <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection