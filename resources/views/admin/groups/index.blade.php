@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link href="{{ asset('assets/plugins/bootstrap-datepicker/datepicker.css') }}" rel="stylesheet" type="text/css" />
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="modalMdTitle"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="modalError"></div>
              <div id="modalMdContent"></div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="modalEditTitle"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="modalError"></div>
              <div id="modalEditContent"></div>
          </div>
      </div>
  </div>
</div>

<div class="row">
	<div class="col-md-12">
      <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">KELOMPOK</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
          	<a href="" class="btn btn-success" data-target="#modal-add-data" data-toggle="modal"><i class="fas fa-university"></i> &nbsp;Tambah Grup</a>
            @include('admin.groups.add')
            <br><br>

            <table id="groups" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 5%" class="center">No</th>
                  <th class="center">Kode Grup</th>
                  <th class="center">Nama Grup</th>
                  <th class="center">Total Poin</th>
                  <th class="center">Turnamen</th>
                  <th class="center"></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>

  <script type="text/javascript">
  $(document).ready(function(){
  	//Initialize Select2 Elements
      $('.select2').select2({
        theme: 'bootstrap4'
      });

      $('#random').on('click', function(e) {
      	e.preventDefault();

      	$.ajax(
      		{
      			url: "{!! route('generate.string') !!}", 
      			success: function(result) {
				    $("#code").val(result);
				}
			}
		);
      });

    $('#groups').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('groups') !!}',
        columns: [
            { 
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                class: 'center',
                orderable: false,
                searchable: false
            },
            {
                data: 'code',
                name: 'code'
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'total_point[</br>]',
                name: 'total_point'
            },
            {
                data: 'turnamen[</br>]',
                name: 'turnamen'
            },
            {
                data: 'action',
                name: 'action',
                class: 'center',
                orderable: false,
                searchable: false
            }
        ]
    });
  });

  setInterval(function(){ 
    $('.modalMd').off('click').on('click', function () {
      $('#modalMd').modal({backdrop: 'static', keyboard: false}) 
      
        $('#modalMdContent').load($(this).attr('value'));
        $('#modalMdTitle').html($(this).attr('title'));

    });

    $('.modalEdit').off('click').on('click', function () {
      $('#modalEdit').modal({backdrop: 'static', keyboard: false}) 
      
        $('#modalEditContent').load($(this).attr('value'));
        $('#modalEditTitle').html($(this).attr('title'));

    });
  }, 500);

  $(function () {
      $("input[data-bootstrap-switch]").each(function(){
	      $(this).bootstrapSwitch('state', $(this).prop('checked'));
	    })
  });
  </script>
@endsection