@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')
{{-- <form method="GET" action="">
	<div class="card">
		<div class="card-body">
			<div class="row">
				
			</div>
		</div>

		<div class="card-footer">
	      <div style="float: right;">
	        <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span> &nbsp;Cari</button>
	      </div>
		</div>
	</div>
</form> --}}

<div class="row">
	<div class="col-md-12">
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title">
					Aktivitas Pengguna
				</h3>

				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
				</div>
			</div>

			<div class="card-body">
				<table id="table" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="width: 5%" class="center">No</th>
							<th class="center">NIM</th>
							<th class="center">Nama</th>
							<th class="center">Aktivitas</th>
							<th class="center">Hari</th>
							<th class="center">Jam</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('.select2').select2({
				theme: 'bootstrap4'
			});

			$('#table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{!! route('activities') !!}',
		        columns: [
		            { 
		                data: 'DT_RowIndex',
		                name: 'DT_RowIndex',
		                class: 'center',
		                orderable: false,
		                searchable: false
		            },
					{
						data: 'nim',
						name: 'nim'
					},
					{
						data: 'nama',
						name: 'nama'
					},
					{
						data: 'aktivitas',
						name: 'aktivitas'
					},
					{
						data: 'tanggal',
						name: 'tanggal',
		                class: 'center'
					},
					{
						data: 'time',
						name: 'time',
		                class: 'center'
					}
		        ]
		    });
		});
	</script>
@endsection