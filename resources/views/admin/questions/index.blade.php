@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
      <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Bank Soal</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
          	<a href="{{ route('create.questionbank') }}" class="btn btn-success"><i class="fas fa-university"></i> &nbsp;Tambah Soal</a>
            <br><br>

            <table id="questions" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 5%" class="center">No</th>
                  <th class="center">Kode Soal</th>
                  <th class="center">Level</th>
                  <th class="center">Grup</th>
                  <th class="center"></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>

<script>
	$(document).ready(function(){
	    $('#questions').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: '{!! route('questionbank') !!}',
	        // columnDefs: [
	        //     {
	        //       render: createManageBtn, 
	        //       data: null, 
	        //       targets: [0]
	        //     }
	        // ],
	        columns: [
	            { 
	                data: 'DT_RowIndex',
	                name: 'DT_RowIndex',
	                class: 'center',
	                orderable: false,
	                searchable: false
	            },
	            {
	                data: 'question_code',
	                name: 'question_code'
	            },
	            {
	                data: 'level',
	                name: 'level',
	                class: 'center'
	            },
	            {
	                data: 'group',
	                name: 'group',
	                class: 'center'
	            },
	            {
	                data: 'action',
	                name: 'action',
	                class: 'center',
	                orderable: false,
	                searchable: false
	            }
	        ]
	    });
	});
</script>
@endsection