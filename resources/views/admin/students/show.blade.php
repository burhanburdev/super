@extends('layouts.main')

@section('content')

<style>
  .modal-xl {
    max-width: 1240px;
  }
</style>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">

      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                 src="
                 @if ($data->avatar && $data->avatar != 'default.png')
                   {{ asset('storage/photo') }}/{{ $data->avatar }}
                 @else
                   {{ asset('assets/dist/img/default.png') }}
                 @endif
                 "
                 alt="User profile picture">
          </div>
          <br>
          <h3 class="profile-username text-center" style="font-size: 16px;"><strong>{{ $data->nama }}</strong></h3>

          <p class="text-muted text-center">{{ $data->nim }}</p>
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <div class="form-group row">
            <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">Username</label>
              <div class="col-sm-10">
                <label style="border: 0; color: black;" class="form-control">
                  {{ $data->username }}
                </label>
              </div>
          </div>
          <div class="form-group row">
            <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">Kode Kelas</label>
              <div class="col-sm-10">
                <label style="border: 0; color: black;" class="form-control">
                  {{ $data->class_code }}
                </label>
              </div>
          </div>
          <div class="form-group row">
            <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">Total Poin</label>
              <div class="col-sm-10">
                <label style="border: 0; color: black;" class="form-control">
                  {{ $data->total_xp }}
                </label>
              </div>
          </div>
          <div class="form-group row">
            <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">Total Crown</label>
              <div class="col-sm-10">
                <label style="border: 0; color: black;" class="form-control">
                  {{ $data->total_crown }}
                </label>
              </div>
          </div>
          <div class="form-group row">
            <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">Tujuan Harian</label>
              <div class="col-sm-10">
                <label style="border: 0; color: black;" class="form-control">
                  {{ $data->daily_goal }}
                </label>
              </div>
          </div>
          <div class="form-group row">
            <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">Jenis Peserta</label>
              <div class="col-sm-10">
                <label style="border: 0; color: black;" class="form-control">
                  @if ($data->is_sso) Internal @else Eksternal @endif
                </label>
              </div>
          </div>

          @if ($data->is_sso)
            <div class="form-group row">
              <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">Program Studi</label>
                <div class="col-sm-10">
                  <label style="border: 0; color: black;" class="form-control">
                    {{ @$data->jurusan->prodi }}
                  </label>
                </div>
            </div>
          @else
            <div class="form-group row">
              <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">Asal Sekolah</label>
                <div class="col-sm-10">
                  <label style="border: 0; color: black;" class="form-control">
                    {{ $data->asal_sekolah }}
                  </label>
                </div>
            </div>
            <div class="form-group row">
              <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">No. Telepon</label>
                <div class="col-sm-10">
                  <label style="border: 0; color: black;" class="form-control">
                    {{ $data->no_telp }}
                  </label>
                </div>
            </div>
            <div class="form-group row">
              <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">Prodi Minat 1</label>
                <div class="col-sm-10">
                  <label style="border: 0; color: black;" class="form-control">
                    {{ @$data->prodi1->prodi }}
                  </label>
                </div>
            </div>
            <div class="form-group row">
              <label style="font-weight: 400" for="inputName" class="col-sm-2 col-form-label">Prodi Minat 2</label>
                <div class="col-sm-10">
                  <label style="border: 0; color: black;" class="form-control">
                    {{ @$data->prodi2->prodi }}
                  </label>
                </div>
            </div>
          @endif

        </div>

      </div>

    </div>

  </div>

</div>
@endsection