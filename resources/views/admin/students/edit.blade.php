@extends('layouts.main')

@section('content')

<style>
  .modal-xl {
    max-width: 1240px;
  }
</style>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">

      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                 src="
                 @if ($data->avatar && $data->avatar != 'default.png')
                   {{ asset('storage/photo') }}/{{ $data->avatar }}
                 @else
                   {{ asset('assets/dist/img/default.png') }}
                 @endif
                 "
                 alt="User profile picture">
          </div>
          <br>
          <h3 class="profile-username text-center" style="font-size: 16px;"><strong>{{ $data->nama }}</strong></h3>
          <p class="text-muted text-center">{{ $data->email }}</p>
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card">
        <form method="POST" action="{{ route('update.student', $data->nim) }}" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <div class="card-body">

            @if ($data->is_sso)            
              <label>Nama</label>
              <div class="form-group">
                <input type="text" name="nama" class="form-control" value="{{ $data->nama }}" readonly>
              </div>

              <label>Email</label>
              <div class="form-group">
                <input type="email" name="email" class="form-control" value="{{ $data->email }}" readonly>
              </div>
            @else            
              <label>Nama</label>
              <div class="form-group">
                <input type="text" name="nama" class="form-control" value="{{ $data->nama }}" required>
              </div>

              <label>Email</label>
              <div class="form-group">
                <input type="email" name="email" class="form-control" value="{{ $data->email }}" required>
              </div>
            @endif

            <label>Kode Kelas</label>
            <div class="form-group">
              <input type="text" name="class_code" class="form-control" value="{{ $data->class_code }}" required>
            </div>

            <label>Total Poin</label>
            <div class="form-group">
              <input type="number" name="total_xp" class="form-control" value="{{ $data->total_xp }}" required>
            </div>

            <label>Total Crown</label>
            <div class="form-group">
              <input type="number" name="total_crown" class="form-control" value="{{ $data->total_crown }}" required>
            </div>

            <label>Tujuan Harian</label>
            <div class="form-group">
              <input type="number" name="daily_goal" class="form-control" value="{{ $data->daily_goal }}" required>
            </div>

            @if ($data->is_sso)
              <label>Program Studi 1</label>
              <div class="form-group">
                <select class="form-control select2" disabled>
                  @foreach($prodi as $prod)
                    <option @if ($prod->id == $data->program_studi) selected @endif value="{{ $prod->id }}">{{ $prod->prodi }}</option>
                  @endforeach                  
                </select>
              </div>
            @else
              <label>Asal Sekolah</label>
              <div class="form-group">
                <input type="text" name="asal_sekolah" class="form-control" value="{{ $data->asal_sekolah }}">
              </div>

              <label>No. Telepon</label>
              <div class="form-group">
                <input type="text" name="no_telp" class="form-control" value="{{ $data->no_telp }}">
              </div>

              <label>Program Studi 1</label>
              <div class="form-group">
                <select class="form-control select2" name="minat_1">
                  <option value=""> -- Pilih Program Studi -- </option>
                  @foreach($prodi as $prod)
                    <option @if ($prod->id == $data->minat_1) selected @endif value="{{ $prod->id }}">{{ $prod->prodi }}</option>
                  @endforeach                  
                </select>
              </div>

              <label>Program Studi 2</label>
              <div class="form-group">
                <select class="form-control select2" name="minat_2">
                  <option value=""> -- Pilih Program Studi -- </option>
                  @foreach($prodi as $id => $prod)
                    <option @if ($prod->id == $data->minat_2) selected @endif value="{{ $prod->id }}">{{ $prod->prodi }}</option>
                  @endforeach                  
                </select>
              </div>
            @endif

            <label>Avatar</label>
            <div class="form-group">
              <input type="file" name="avatar" accept="image/jpeg, image/png, image/jpg">
            </div>
          </div>

          <div class="card-footer">
            <div class="form-group" style="float: right;">
              <button class="btn btn-primary" type="submit">Simpan</button>
              <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
            </div>
          </div>
        </form>
      </div>

    </div>

  </div>

</div>

<script>  
      //Initialize Select2 Elements
      $('.select2').select2({
        theme: 'bootstrap4'
      });
</script>
@endsection