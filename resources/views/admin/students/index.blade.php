@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<style>
  .center {
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="modalMdTitle"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="modalError"></div>
              <div id="modalMdContent"></div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="modalEditTitle"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="modalError"></div>
              <div id="modalEditContent"></div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="modalPass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="modalPassTitle"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="modalError"></div>
              <div id="modalPassContent"></div>
          </div>
      </div>
  </div>
</div>

{{--<form method="GET" action="">
  <div class="card">      
    <div class="card-body">  
      <div class="row">     

        <div class="col-md-12">
          <div class="form-group">
            <label>Jenis Peserta</label>
            <select name="peserta" class="form-control select2">
              <option value="">Semua Jenis Peserta</option>
              <option <?= (isset($_GET['peserta']) && $_GET['peserta'] == 'internal') ? 'selected' : ''; ?> value="internal">Internal</option>
              <option <?= (isset($_GET['peserta']) && $_GET['peserta'] == 'eksternal') ? 'selected' : ''; ?> value="eksternal">Eksternal</option>
            </select>
          </div>
        </div> 
      </div>   
    </div>

    <div class="card-footer">
      <div style="float: right;">
        <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span> &nbsp;Cari</button>
      </div>
    </div>
  </div>
</form>--}}

<div class="row">
	<div class="col-md-12">
      <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">BIODATA PESERTA</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <table id="peserta" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 5%" class="center">No</th>
                  <th class="center">NIM</th>
                  <th class="center">Nama</th>
                  <th class="center">Email</th>
                  <th class="center">Jenis Peserta</th>
                  <th class="center"></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

  <script type="text/javascript">
  $(document).ready(function(){
    $('#peserta').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('students', ['peserta' => $pesertaAjax]) !!}',
        columns: [
            { 
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                class: 'center',
                orderable: false,
                searchable: false
            },
            {
                data: 'nim',
                name: 'nim'
            },
            {
                data: 'nama',
                name: 'nama'
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'jenis',
                name: 'jenis'
            },
            {
                data: 'action',
                name: 'action',
                class: 'center',
                orderable: false,
                searchable: false
            }
        ]
    });
  });

  setInterval(function(){ 

    $('.modalMd').off('click').on('click', function () {
      $('#modalMd').modal({backdrop: 'static', keyboard: false}) 
      
        $('#modalMdContent').load($(this).attr('value'));
        $('#modalMdTitle').html($(this).attr('title'));

    });

    $('.modalEdit').off('click').on('click', function () {
      $('#modalEdit').modal({backdrop: 'static', keyboard: false}) 
      
        $('#modalEditContent').load($(this).attr('value'));
        $('#modalEditTitle').html($(this).attr('title'));

    });
    
    $('.modalPass').off('click').on('click', function () {
      $('#modalPass').modal({backdrop: 'static', keyboard: false}) 
      
        $('#modalPassContent').load($(this).attr('value'));
        $('#modalPassTitle').html($(this).attr('title'));

    });
  }, 500);
  </script>
@endsection