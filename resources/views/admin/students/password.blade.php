@extends('layouts.main')

@section('content')

<style>
  .modal-xl {
    max-width: 1240px;
  }
</style>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">

      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                 src="
                 @if ($data->avatar && $data->avatar != 'default.png')
                   {{ asset('storage/photo') }}/{{ $data->avatar }}
                 @else
                   {{ asset('assets/dist/img/default.png') }}
                 @endif
                 "
                 alt="User profile picture">
          </div>
          <br>
          <h3 class="profile-username text-center" style="font-size: 16px;"><strong>{{ $data->nama }}</strong></h3>
          <p class="text-muted text-center">{{ $data->email }}</p>
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card">
        <form method="POST" action="{{ route('update.password', $data->nim) }}">
          @csrf
          @method('PUT')
          <div class="card-body">

            <label>Password</label>
            <div class="form-group">
              <input type="password" name="password" class="form-control" required>
            </div>

            <label>Konfirmasi Password</label>
            <div class="form-group">
              <input type="password" name="password_confirmation" class="form-control" required>
            </div>
          </div>

          <div class="card-footer">
            <div class="form-group" style="float: right;">
              <button class="btn btn-primary" type="submit">Simpan</button>
              <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
            </div>
          </div>
        </form>
      </div>

    </div>

  </div>

</div>
@endsection