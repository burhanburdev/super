        <div class="modal fade" id="modal-add-data">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Tambah Bab</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              </div>

              <form method="POST" action="{{ route('store.section') }}">
                @csrf

                <div class="modal-body">
                    <input type="hidden" class="form-control" name="course_id" value="{{ $id }}" required>

                  <label>Judul Bab</label>
                  <div class="form-group">
                    <input type="text" class="form-control" name="title" required>
                  </div>

                  <label>Level</label>
                  <div class="form-group">
                    <input type="number" class="form-control" min="0" name="level" required>
                  </div>

                  <div id="exercises">
                    <div class="form-group">
                      <a href="#" class="btn btn-xs btn-success" id="addrow"> + Tambah Latihan</a>
                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <div class="form-group">
                    <button class="btn btn-primary" type="submit">Simpan</button>
                    <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
                  </div>
                </div>

              </form>

            </div>
          </div>
        </div>