@extends('layouts.main')

@section('content')
<div class="container-fluid">
  <div class="row">
  	<div class="col-md-12">
  		<div class="card">
  			<form method="POST" action="{{ route('update.section', $data->id) }}">
  			@csrf
  			@method('PUT')
  				<div class="card-body">
  					<label>Judul Bab</label>
					<div class="form-group">
					<input type="text" name="title" class="form-control" value="{{ $data->title }}" required>
					</div>

  					<label>Level</label>
					<div class="form-group">
					<input type="number" name="level" class="form-control" min="0" value="{{ $data->level }}" required>
					</div>

					<div id="edit-exercises">
						<div class="form-group">
							<a href="#" class="btn btn-xs btn-success" id="addrow"> + Tambah Latihan</a>
						</div>
						@foreach($data->exercises as $exe)
						<p>
							<div class="row">
							  <div class="col-md-1">
							    <label>Latihan</label>
							  </div>
							  <div class="col-md-4">
							    <input type="hidden" class="form-control" name="u_exercise_id[]" value="{{ $exe->id }}" required>
							    <input type="text" class="form-control" name="u_exercise_title[]" value="{{ $exe->title }}" required>
							  </div>
							  <div class="col-md-1">
							  	<label>Level</label>
							  </div>
							  <div class="col-md-2">
							    <input type="number" class="form-control" min="0" name="u_exercise_level[]" value="{{ $exe->level }}" required>
							  </div>
							  <div class="col-md-1">
							  	<label>Jumlah Pertanyaan</label>
							  </div>
							  <div class="col-md-2">
							    <input type="number" class="form-control" min="0" name="u_exercise_question[]" value="{{ $exe->question }}" required>
							  </div>
							  <div class="col-md-1">
							    <div class="input-group-prepend">
							      <span class="input-group-text" style="color: transparent; background-color: transparent;border: none;">
							      	<a href="{{ route('destroy.exercise', $exe->id) }}" class="btn btn-xs btn-danger" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="fa fa-trash"></i></a>
							      </span>
							    </div>
							  </div>
							</div>
						</p>
						@endforeach
					</div>
  				</div>

  				<div class="card-footer">
  					<div class="form-group" style="float: right;">
		              <button class="btn btn-primary" type="submit">Simpan</button>
		              <button class="btn btn-default" type="button" data-dismiss="modal">Kembali</button>
		            </div>
  				</div>
  			</form>
  		</div>
  	</div>
  </div>
</div>

<script>
	var counter = 0;

	$("#addrow").on("click", function () {
	    var newRow = $("<p>");
	    var cols = "";

	    cols += '<div class="row"><div class="col-md-1"><label>Latihan</label></div> <div class="col-md-4"> <input type="text" class="form-control" name="exercise_title[]" required> </div> <div class="col-md-1"> <label> Level </label> </div> <div class="col-md-2"> <input type="number" class="form-control" min="0" name="exercise_level[]" required> </div> <div class="col-md-1"> <label> Jumlah Pertanyaan </label> </div> <div class="col-md-2"> <input type="number" class="form-control" min="0" name="exercise_question[]" required> </div> <div class="col-md-1"> <div class="input-group-prepend"> <span class="input-group-text" style="background-color: transparent; border: none;"> <a href="javascript:void(0)" class="btn btn-xs btn-danger ibtnDel"><i class="fa fa-trash"></i></a> </span> </div> </div> </div>';

	    newRow.append(cols);
	    $("#edit-exercises").append(newRow);
	    counter++;
	});

	$("#edit-exercises").on("click", ".ibtnDel", function (event) {
	    $(this).closest("p").remove();       
	    counter -= 1
	});
</script>
@endsection