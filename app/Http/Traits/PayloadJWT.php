<?php

namespace App\Http\Traits;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

trait PayloadJWT
{
	public function nim()
	{
		$returnValue = null;		
		$token = JWTAuth::getToken();

		if ($token) {			
	        $payloads = JWTAuth::getPayload($token)->toArray();

	        $returnValue = $payloads['sub'];
		}

        return $returnValue;
	}

	public function ranking()
	{
		$returnValue = null;		
		$token = JWTAuth::getToken();

		if ($token) {			
	        $payloads = JWTAuth::getPayload($token)->toArray();

	        $returnValue = $payloads['ranking_position'];
		}

        return $returnValue;
	}
}