<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\DailyXp;
use App\Models\DailyXpAchieve;
use App\Models\ExerciseFinished;
use App\Models\UserLogs;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller
{
    public function index()
    {
        $student = Student::all();
        return response([
            'success' => true,
            'message' => 'List student',
            'data' => $student
        ], 200);
    }

    public function show()
    {
        $nim = $this->nim;
        
        $student = Student::where('nim', $nim)->first();
        $leaderboard = Student::orderByRaw('total_xp DESC, total_crown DESC')->get()->toArray();
        $rankPosition =  array_search($nim, array_column($leaderboard, 'nim'));
        $student['rank_position'] = $rankPosition + 1;
        $now = date('Y-m-d');

        if ($now) {
            $dailyResult = DailyXp::where('nim', $nim)->first();

            if ($dailyResult) {
                if ($dailyResult->date == $now) {
                    $student['daily_xp'] = $dailyResult->xp;
                } else {
                    DailyXp::find($dailyResult->id)->update([
                        'xp' => 0,
                        'date' => $now
                    ]);
                    $student['daily_xp'] = 0;
                }
            } else {
                $student['daily_xp'] = 0;
            }
        } else {
            $student['daily_xp'] = 0;
        }

        return response([
            'success' => true,
            'message' => 'Detail Student',
            'data' => $student
        ], 200);
    }

    public function updateDailyXp(Request $request)
    {
        $nim = $this->nim;

        $dateRequest = $request->date;
        $xpAddition = $request->xp;
        $actionMessage = "";

        if ($dateRequest) {
            $dailyResult = DailyXp::where('nim', $nim)->first();

            if ($dailyResult) {
                if ($dailyResult->date == $dateRequest) {
                    $prevXp = DailyXp::where('nim', $nim)->first()->xp;
                    $daily_xp = DailyXp::find($dailyResult->id)->update([
                        'xp' => $prevXp + $xpAddition,
                        'date' => $dateRequest
                    ]);

                    $actionMessage = "Daily xp updated";

                    if ($daily_xp) {
                        return response([
                            'success' => true,
                            'message' => $actionMessage,
                            'data' => $daily_xp
                        ], 200);
                    } else {
                        return response([
                            'success' => false,
                            'message' => 'Daily Action Failed'
                        ], 200);
                    }
                } else {
                    $daily_xp = DailyXp::find($dailyResult->id)->update([
                        'xp' => $xpAddition,
                        'date' => $dateRequest
                    ]);

                    $actionMessage = "Daily xp added";

                    if ($daily_xp) {
                        return response([
                            'success' => true,
                            'message' => $actionMessage,
                            'data' => $daily_xp
                        ], 200);
                    } else {
                        return response([
                            'success' => false,
                            'message' => 'Daily Action Failed'
                        ], 200);
                    }
                }
            } else {
                $daily_xp = DailyXp::create([
                    'nim' => $nim,
                    'xp' => $xpAddition,
                    'date' => $dateRequest
                ]);

                $actionMessage = "Daily xp created";

                if ($daily_xp) {
                    return response([
                        'success' => true,
                        'message' => $actionMessage,
                        'data' => true
                    ], 200);
                } else {
                    return response([
                        'success' => false,
                        'message' => 'Daily Action Failed'
                    ], 200);
                }
            }
        }
    }

    public function store(Request $request)
    {
        $resultFiles = $request->file('avatar');
        $path = Storage::put(
            'public/photo',
            $request->file('avatar')
        );
        $pathName = basename($path);
        $resultFiles->move(public_path('/storage/photo'), $pathName);

        $student = Student::create([
            'nim' => $request->nim,
            'nama' => $request->nama,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'class_code' => $request->class_code,
            'total_xp' => 0,
            'daily_goal' => 0,
            'total_crown' => 0,
            'avatar' => $pathName,
        ]);

        if ($student) {
            return response([
                'success' => true,
                'message' => "Sign up Success",
                'data' => $student
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Sign up Failed'
            ], 200);
        }
    }

    public function update(Request $request)
    {
        $student = Student::find($this->nim)->update([
            'nim' => $this->nim,
            'nama' => $request->nama,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'kode_prodi' => $request->kode_prodi,
        ]);

        if ($student) {
            return response([
                'success' => true,
                'message' => 'Item berhasil diupdate',
                'data' => $student
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal diupdate'
            ], 200);
        }
    }

    public function destroy($nim)
    {
        $student = Student::where('nim', $nim)->delete();

        if ($student) {
            return response([
                'success' => true,
                'message' => 'Item berhasil didelete',
                'data' => $student
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal didelete'
            ], 200);
        }
    }

    // public function login(Request $request)
    // {
    //     $student = Student::where('email', $request->email)->first();
    //     $leaderboard = Student::orderByRaw('total_xp DESC, total_crown DESC')->get()->toArray();
    //     $rankPosition =  array_search($student->nim, array_column($leaderboard, 'nim'));
    //     $student['rank_position'] = $rankPosition + 1;

    //     if (Hash::check($request->password, $student->password)) {
    //         return response([
    //             'success' => true,
    //             'message' => 'Login success',
    //             'data' => $student
    //         ], 200);
    //     } else {
    //         return response([
    //             'success' => false,
    //             'message' => 'Login failed',
    //         ], 200);
    //     }
    // }

    public function leaderboard()
    {
        $leaderboard = Student::orderByRaw('total_xp DESC, total_crown DESC')->get();

        return response([
            'success' => true,
            'message' => 'Leaderboard',
            'data' => $leaderboard
        ], 200);
    }

    public function updateDaily(Request $request)
    {
        $daily = Student::where('nim', $this->nim)->update([
            'daily_goal' => $request->daily,
        ]);

        return response([
            'success' => true,
            'message' => 'Berhasil update daily',
            'data' => $daily
        ], 200);
    }

    public function updateXp(Request $request)
    {
        $student = Student::where('nim', $this->nim)->first();

        $totalXp = $student["total_xp"] + $request->xp;

        $xp = Student::where('nim', $this->nim)->update([
            'total_xp' => $totalXp,
        ]);

        if ($xp) {
            return response([
                'success' => true,
                'message' => 'Total XP Updated',
                'data' => true
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Total XP failed to Update',
                'data' => false
            ], 200);
        }
    }

    public function changePhoto(Request $request)
    {
        $student = Student::where('nim', $this->nim)->first();

        $resultfiles = $request->file('file');
        $path = Storage::put(
            'public/photo',
            $request->file('file')
        );
        $resultfiles->move(public_path('/storage/photo'), basename($path));

        if (file_exists(storage_path('app') . '/' . $student->foto)) {
            unlink(storage_path('app') . '/' . $student->foto);
        }

        Student::where('nim', $this->nim)->update([
            'foto' => basename($path),
        ]);

        return response([
            'success' => true,
            'message' => 'Update foto',
            'data' => $path
        ], 200);
    }

    public function updateUsername(Request $request)
    {
        $student = Student::find($this->nim)->update([
            'username' => $request->username,
        ]);

        if ($student) {
            return response([
                'success' => true,
                'message' => 'Item berhasil diupdate',
                'data' => $student
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal diupdate'
            ], 200);
        }
    }

    public function updateCrown(Request $request)
    {
        $student = Student::where('nim', $this->nim)->first();
        $crown = $request->crown + $student->total_crown;

        $student = Student::find($this->nim)->update([
            'total_crown' => $crown,
        ]);

        if ($student) {
            return response([
                'success' => true,
                'message' => 'Crown added diupdate',
                'data' => true
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Crown failed to added'
            ], 200);
        }
    }

    public function getExerciseStatus()
    {
        $exerciseFinished = ExerciseFinished::where('nim', $this->nim)->get();

        return response([
            'success' => true,
            'message' => 'Exercise Status',
            'data' => $exerciseFinished
        ], 200);
    }

    public function storeLoginLog(Request $request)
    {
        $loginLogs = UserLogs::create([
            "nim" => $this->nim,
            "date" => $request->date,
            "time" => $request->time,
        ]);

        if ($loginLogs) {
            return response([
                'success' => true,
                'message' => 'login logs stored',
                'data' => true
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'login logs failed to store',
                'data' => false
            ], 200);
        }
    }

    public function updatedDailyXpAchieve(Request $request)
    {
        $dailyXpAchieve = DailyXpAchieve::where('nim', $this->nim)->first();

        if ($dailyXpAchieve) {
            $dateRequest = $request->date_updated;

            if ($dateRequest != $dailyXpAchieve->date_updated) {
                $prevCountAchieved = $dailyXpAchieve->count;
                $dailyXpAchieveUpdated = DailyXpAchieve::where('nim', $this->nim)->update([
                    'count' => $prevCountAchieved + 1,
                    'date_updated' => $request->date_updated
                ]);

                if ($dailyXpAchieveUpdated) {
                    return response([
                        'success' => true,
                        'message' => 'daily goals achieved added',
                        'data' => true
                    ], 200);
                } else {
                    return response([
                        'success' => false,
                        'message' => 'daily goals achieved failed to add',
                        'data' => false
                    ], 500);
                }
            } else {
                return response([
                    'success' => true,
                    'message' => 'daily goals achieved have been added before',
                    'data' => false
                ], 200);
            }
        } else {
            $insertDailyXpAchieve = DailyXpAchieve::create([
                "nim" => $this->nim,
                "count" => 1,
                "date_updated" => $request->date_updated
            ]);
            
            if ($insertDailyXpAchieve) {
                return response([
                    'success' => true,
                    'message' => 'daily goals achieved inserted',
                    'data' => true
                ], 200);
            } else {
                return response([
                    'success' => false,
                    'message' => 'daily goals achieved failed to insert',
                    'data' => false
                ], 500);
            }
        }
    }

    public function resetPasswordOrAvatar(Request $request)
    {

        $student = Student::where('nim', $this->nim)->first();

        $oldPassword = $request->old_password;
        $newPassword = $request->new_password;
        $resultFiles = $request->file('avatar');

        if ($oldPassword != null && $newPassword != null) {
            if (Hash::check($oldPassword, $student->password)) {
                if ($resultFiles != null) {
                    $path = Storage::put(
                        'public/photo',
                        $request->file('avatar')
                    );
                    $pathName = basename($path);
                    $resultFiles->move(public_path('/storage/photo'), $pathName);

                    $updatedStudent = Student::find($this->nim)->update([
                        'password' => Hash::make($newPassword),
                        'avatar' => $pathName,
                    ]);

                    return response([
                        'success' => true,
                        'message' => 'Password and Avatar Updated',
                        'data' => $updatedStudent
                    ], 200);
                } else {
                    $updatedStudent = Student::find($this->nim)->update([
                        'nim' => $this->nim,
                        'password' => Hash::make($newPassword),
                    ]);

                    return response([
                        'success' => true,
                        'message' => 'Password Updated',
                        'data' => $updatedStudent
                    ], 200);
                }
            } else {
                // password salah
                return response([
                    'success' => true,
                    'message' => 'Incorrect old password',
                    'data' => false
                ], 500);
            }
        } else {
            if ($resultFiles != null) {
                $path = Storage::put(
                    'public/photo',
                    $request->file('avatar')
                );
                $pathName = basename($path);
                $resultFiles->move(public_path('/storage/photo'), $pathName);

                $updatedStudent = Student::find($this->nim)->update([
                    'avatar' => $pathName,
                ]);

                return response([
                    'success' => true,
                    'message' => 'Password and Avatar Updated',
                    'data' => $updatedStudent
                ], 200);
            } else {
                return response([
                    'success' => true,
                    'message' => 'Avatar failed to updated',
                    'data' => false
                ], 500);
            }
        }
    }
}
