<?php

namespace App\Http\Controllers;

use App\Models\Subquestion;
use App\Models\ExerciseAnswerLogs;

use Illuminate\Http\Request;

class SubquestionController extends Controller
{
    public function index()
    {
        $Subquestion = Subquestion::all();
        return response([
            'success' => true,
            'message' => 'List Subquestion',
            'data' => $Subquestion
        ], 200);
    }

    public function show($id)
    {
        $Subquestion = Subquestion::where('course_id', $id)->get();
        return response([
            'success' => true,
            'message' => 'Detail Subquestion',
            'data' => $Subquestion
        ], 200);
    }

    public function store(Request $request)
    {
        $Subquestion = Subquestion::create([
            'topic_title' => $request->topic_title,
            'topic_category' => $request->topic_category,
            'mata_kuliah' => $request->mata_kuliah,
        ]);

        if ($Subquestion) {
            return response([
                'success' => true,
                'message' => 'Item berhasil disimpan',
                'data' => $Subquestion
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal disimpan'
            ], 200);
        }
    }

    public function update(Request $request)
    {
        $Subquestion = Subquestion::find($request->id_topic)->update([
            'topic_title' => $request->topic_title,
            'topic_category' => $request->topic_category,
            'mata_kuliah' => $request->mata_kuliah,
        ]);

        if ($Subquestion) {
            return response([
                'success' => true,
                'message' => 'Item berhasil diupdate',
                'data' => $Subquestion
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal diupdate'
            ], 200);
        }
    }

    public function destroy($id)
    {
        $Subquestion = Subquestion::where('id_topic', $id)->delete();

        if ($Subquestion) {
            return response([
                'success' => true,
                'message' => 'Item berhasil didelete',
                'data' => $Subquestion
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal didelete'
            ], 200);
        }
    }

    public function storeLog(Request $request)
    {
        $log = ExerciseAnswerLogs::create([
            "nim" => $this->nim,
            "subquestion_id" => $request->subquestion_id,
            "answer" => $request->answer
        ]);
        if ($log) {
            return response([
                'success' => true,
                'message' => 'Logs Created',
                'data' => $log
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Logs Failed to Create',
                'data' => $log
            ], 500);
        }
    }
}
