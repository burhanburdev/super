<?php

namespace App\Http\Controllers;

use App\Models\Tournaments;
use App\Models\TournamentGroup;
use App\Models\TournamentPeriod;
use App\Models\TournamentAnswerLogs;
use App\Models\MemberGroup;
use App\Models\MemberPoint;

use DB;
use Exception;

use Illuminate\Http\Request;

class TournamentController extends Controller
{
    public function index()
    {
    	$data = Tournaments::all();

        return response([
            'success' => true,
            'message' => 'List tournaments',
            'data' => $data
        ], self::SUCCESS);
    }

    public function active()
    {
    	$data = Tournaments::where('is_active', true)->withCount('tournamentGroup as total_group')->get();

        return response([
            'success' => true,
            'message' => 'Active tournament',
            'data' => $data
        ], self::SUCCESS);
    }

    public function updateTourneyPoint(Request $request)
    {
        $point = $request->point;
        $tournament_id = $request->tournament_id;
        $nim = $this->nim;

        DB::beginTransaction();

        try {
            // update poin member di dalam turnamen
            $member = MemberPoint::where(array('nim' => $nim, 'tournament_id' => $tournament_id))->first();
            $member_point = @$member->point;

            $member->update([
                'point' => $member_point + $point
            ]);

            // update total poin grup di dalam turnamen
            $member_group = MemberGroup::where(array('nim' => $nim))->first();

            $groups = DB::table('member_group as mg')
            ->select('mp.nim', 'mp.point')
            ->join('groups as g', 'g.id', 'mg.group_id')
            ->join('tournament_group as tg', 'tg.group_id', 'g.id')
            ->join('tournaments as t', 't.id', 'tg.tournament_id')
            ->join('member_point as mp', 'mp.tournament_id', 't.id')
            ->where(array('t.id' => $tournament_id, 'g.id' => @$member_group->group_id))
            ->groupBy('mp.nim', 'mp.point')
            ->get();

            $total_xp = 0;

            foreach ($groups as $group) {
                $xp = $group->point;

                $total_xp = $total_xp + $xp;

            }

            $tour_group = TournamentGroup::where(array('group_id' => @$member_group->group_id, 'tournament_id' => $tournament_id))->first();
            $tour_group->update(['total_point' => $total_xp]);

            $app = app();
            $data = $app->make('stdClass');
            $data->tournament_id = (string) $tournament_id;
            $data->group_id = (string) $member_group->group_id;
            $data->total_point = (string) $total_xp;
            $data->your_point = (string) $point;

            DB::commit();            

            return response([
                'success' => true,
                'message' => 'Update tourney point',
                'data' => $data
            ], self::SUCCESS);
        } catch (Exception $ex) {
            DB::rollBack();
            
            return response([
                'success' => false,
                'message' => $ex->getMessage(),
            ], self::SERVER_ERROR);
        }
    }

    public function checkTournamentGroup($id)
    {
        $nim = $this->nim;

        if (!Tournaments::where(array('id' => $id, 'is_active' => true))->exists()) {
            return response([
                'success' => false,
                'message' => 'Tournament you were looking for could not be found',
            ], self::SUCCESS);
        }
        $check = MemberGroup::select('nim')->where(array('nim' => $nim))->exists();

        if (!$check) {
            return response([
                'success' => false,
                'message' => 'You have not joined the group. Please create a group or join another group',
            ], self::SUCCESS);
        }

        $check = DB::table('tournament_group as tg')
        ->join('groups as g', 'g.id', 'tg.group_id')
        ->join('member_group as mg', 'mg.group_id', 'g.id')
        ->where(array('tg.tournament_id' => $id, 'mg.nim' => $nim))
        ->exists();

        if (!$check) {
            return response([
                'success' => false,
                'message' => 'Your group has joined the tournament',
            ], self::SUCCESS);
        }

        return response([
            'success' => true,
            'message' => 'You can join the tournament',
        ], self::SUCCESS);
    }

    public function show($id)
    {
        $data = Tournaments::where('id', $id)->first();
        $grups = DB::table('groups as g')
        ->select('g.code', 'g.name as group', 'tg.total_point')
        ->join('tournament_group as tg', 'tg.group_id', 'g.id')
        ->where(array('tg.tournament_id' => $id))
        ->orderBy('tg.total_point', 'DESC')
        ->get();
        
        $data['start_date'] = tanggal(@$data->start_date);
        $data['end_date'] = tanggal(@$data->end_date);

        $data['groups'] = $grups;

        return response([
            'success' => true,
            'message' => 'Detail tournament',
            'data' => $data
        ], self::SUCCESS);
    }

    public function store(Request $request)
    {
    	$data = Tournaments::create([
            'title' => $request->title,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'is_active' => $request->is_active,
        ]);

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil disimpan',
                'data' => $data
            ], self::SUCCESS);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal disimpan'
            ], self::SUCCESS);
        }
    }

    public function update(Request $request, $id)
    {
    	$data = Tournaments::find($id)->update([
            'title' => $request->title,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'is_active' => $request->is_active,
        ]);

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil diupdate',
                'data' => $data
            ], self::SUCCESS);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal diupdate'
            ], self::SUCCESS);
        }
    }

    public function destroy($id)
    {
    	$data = Tournaments::where('id', $id)->delete();

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil didelete',
                'data' => $data
            ], self::SUCCESS);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal didelete'
            ], self::SUCCESS);
        }
    }

    public function storeLog(Request $request)
    {
    	$log = TournamentAnswerLogs::create([
            "tournament_id" => $request->tournament_id,
            "nim" => $this->nim,
            "subquestion_id" => $request->subquestion_id,
            "answer" => $request->answer,
            // "point" => $request->point
        ]);
        
        if ($log) {
            return response([
                'success' => true,
                'message' => 'Logs Created',
                'data' => $log
            ], self::SUCCESS);
        } else {
            return response([
                'success' => false,
                'message' => 'Logs Failed to Create',
                'data' => $log
            ], self::SERVER_ERROR);
        }
    }

    public function indexPeriod()
    {
    	$data = TournamentPeriod::all();

        return response([
            'success' => true,
            'message' => 'List period tournaments',
            'data' => $data
        ], self::SUCCESS);
    }

    public function showPeriod($id)
    {
    	$data = TournamentPeriod::where('id', $id)->get();

        return response([
            'success' => true,
            'message' => 'Detail period tournament',
            'data' => $data
        ], self::SUCCESS);
    }

    public function storePeriod(Request $request)
    {
        DB::beginTransaction();

        try {
            $activeTournament = Tournaments::where(array('is_active' => true))->first();            

            $data = TournamentPeriod::create([
                'tournament_id' => $activeTournament->id,
                'start' => $request->start,
                'end' => $request->end,
            ]);

            DB::commit();

            return response([
                'success' => true,
                'message' => 'Item berhasil disimpan',
                'data' => $data
            ], self::SUCCESS);          
        } catch (Exception $ex) {
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], self::SERVER_ERROR);
        } catch(\Illuminate\Database\QueryException $ex) { 
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], self::SERVER_ERROR);
        }
    }

    public function updatePeriod(Request $request, $id)
    {
        DB::beginTransaction();

        try {            
            $activeTournament = Tournaments::where(array('is_active' => true))->first();

            $data = TournamentPeriod::find($id)->update([
                'tournament_id' => $activeTournament->id,
                'start' => $request->start,
                'end' => $request->end,
            ]);

            DB::commit();

            return response([
                'success' => true,
                'message' => 'Item berhasil diupdate',
                'data' => $data
            ], self::SUCCESS);
        } catch (Exception $ex) {
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], self::SERVER_ERROR);
        } catch(\Illuminate\Database\QueryException $ex) { 
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], self::SERVER_ERROR);
        }
    }

    public function destroyPeriod($id)
    {
    	$data = TournamentPeriod::where('id', $id)->delete();

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil didelete',
                'data' => $data
            ], self::SUCCESS);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal didelete'
            ], self::SUCCESS);
        }
    }

    public function indexGroup()
    {
    	$data = TournamentGroup::all();

        return response([
            'success' => true,
            'message' => 'List Group tournaments',
            'data' => $data
        ], self::SUCCESS);
    }

    public function showGroup($id)
    {
    	$data = TournamentGroup::where('id', $id)->get();

        return response([
            'success' => true,
            'message' => 'Detail Group tournament',
            'data' => $data
        ], self::SUCCESS);
    }

    public function storeGroup(Request $request)
    {
        $total_point = 0;
        $nim = $this->nim;

        DB::beginTransaction();

        try {
            $group = MemberGroup::select('group_id as id')->where(array('nim' => $nim))->first();

            if (!$group) {
                return response([
                    'success' => false,
                    'message' => 'Unable to join tournament because you do not have a group yet'
                ], self::UNAUTHORIZED);
            }

            // check apakah group sudah mengikuti turnamen
            $check = TournamentGroup::where(array('group_id' => $group->id, 'tournament_id' => $request->tournament_id))->first();

            if ($check) {
                return response([
                    'success' => false,
                    'message' => 'Unable to enter tournament because you have joined tournament'
                ], self::UNAUTHORIZED);
            }

            // hitung total point member grup
            $members = MemberGroup::where(array('group_id' => $group->id))->get();

            foreach ($members as $member) {
                $point = 0;
                $memberPoint = MemberPoint::select('point')->where(array('nim' => $member->nim, 'tournament_id' => $request->tournament_id))->first();

                if ($memberPoint) {
                    $point = $memberPoint->point;
                } else {
                    MemberPoint::create([
                        'tournament_id' => $request->tournament_id,
                        'nim' => $member->nim,
                        'point' => 0
                    ]);
                }

                $total_point = $total_point + $point;
            }

            $data = TournamentGroup::create([
                'tournament_id' => (int) $request->tournament_id,
                'group_id' => $group->id,
                'total_point' => $total_point,
                'place' => null
            ]);

            DB::commit();

            return response([
                'success' => true,
                'message' => 'Your group has successfully joined the tournament',
                'data' => $data
            ], self::SUCCESS);
        } catch (Exception $ex) {
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], self::SERVER_ERROR);
        } catch(\Illuminate\Database\QueryException $ex) { 
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], self::SERVER_ERROR);
        }
    }

    public function updateGroup(Request $request, $id)
    {
    	$data = TournamentGroup::find($id)->update([
            'tournament_id' => $request->title,
            'start' => $request->start,
            'end' => $request->end,
        ]);

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil diupdate',
                'data' => $data
            ], self::SUCCESS);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal diupdate'
            ], self::SUCCESS);
        }
    }

    public function destroyGroup($id)
    {
    	$data = TournamentGroup::where('id', $id)->delete();

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil didelete',
                'data' => $data
            ], self::SUCCESS);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal didelete'
            ], self::SUCCESS);
        }
    }
}
