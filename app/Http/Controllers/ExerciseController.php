<?php

namespace App\Http\Controllers;

use App\Models\Exercise;
use App\Models\ExerciseFinished;
use App\Models\ExerciseAnswerLogs;

use Illuminate\Http\Request;

class ExerciseController extends Controller
{
    public function index()
    {
        $Exercise = Exercise::all()->load('questions')->toArray();
        $exercises = [];

        foreach ($Exercise as $value) {
            $temp = $value['questions'];
            $exercise = $value;
            if (count($temp) > $value->question) {
                $exercise['questions'] = array_slice($temp, $value->question);
            }
            array_push($exercises, $exercise);
        }
        return response([
            'success' => true,
            'message' => 'List Exercise',
            'data' => $exercises
        ], 200);
    }

    public function show($id)
    {
        $Exercise = Exercise::all();
        
        return response([
            'success' => true,
            'message' => 'Detail Exercise',
            'data' => $Exercise
        ], 200);
    }

    public function count()
    {
        $exerciseCount = count((array) Exercise::all()->toArray());

        return response([
            'success' => true,
            'message' => 'Detail Exercise',
            'data' => $exerciseCount
        ], 200);
    }

    public function showBySectionId($id)
    {
        $Section = Exercise::with('questions.subquestions')->where('section_id', $id)->get()->toArray();
        $exercises = [];

        foreach ($Section as $value) {
            $temp = $value['questions'];
            $exercise = $value;
            if (count($temp) > $value->question) {
                $exercise['questions'] = array_slice($temp, $value->question);
            }
            array_push($exercises, $exercise);
        }
        return response([
            'success' => true,
            'message' => 'Detail Exercise',
            'data' => $exercises
        ], 200);
    }

    public function store(Request $request)
    {
        $Exercise = Exercise::create([
            'title' => $request->title,
            'level' => $request->level,
            'section_id' => $request->section_id,
        ]);

        if ($Exercise) {
            return response([
                'success' => true,
                'message' => 'Item berhasil disimpan',
                'data' => $Exercise
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal disimpan'
            ], 200);
        }
    }

    public function status(Request $request)
    {
        $exercise = ExerciseFinished::create([
            'nim' => $this->nim,
            'exercise_id' => $request->exercise_id,
        ]);

        if ($exercise) {
            return response([
                'success' => true,
                'message' => 'Item berhasil disimpan',
                'data' => true
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal disimpan'
            ], 200);
        }
    }

    public function update(Request $request)
    {
        $Exercise = Exercise::find($request->id)->update([
            'title' => $request->title,
            'level' => $request->level,
            'section_id' => $request->section_id,
        ]);

        if ($Exercise) {
            return response([
                'success' => true,
                'message' => 'Item berhasil diupdate',
                'data' => $Exercise
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal diupdate'
            ], 200);
        }
    }

    public function destroy($id)
    {
        $Exercise = Exercise::where('id', $id)->delete();

        if ($Exercise) {
            return response([
                'success' => true,
                'message' => 'Item berhasil didelete',
                'data' => $Exercise
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal didelete'
            ], 200);
        }
    }

    public function storeLog(Request $request)
    {
        $Exercise = ExerciseAnswerLogs::create([
            'nim' => $this->nim,
            'subquestion_id' => $request->subquestion_id,
            'answer' => $request->answer,
        ]);

        if ($Exercise) {
            return response([
                'success' => true,
                'message' => 'Answer has been saved',
                'data' => $Exercise
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal disimpan'
            ], 200);
        }
    }
}
