<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index()
    {
        $course = Course::all()->load('sections');
        return response([
            'success' => true,
            'message' => 'List course',
            'data' => $course
        ], 200);
    }

    public function show($id)
    {
        $course = Course::where('id', $id)->first()->load('sections');
        return response([
            'success' => true,
            'message' => 'Detail course',
            'data' => $course
        ], 200);
    }

    public function store(Request $request)
    {
        $course = Course::create([
            'title' => $request->title,
        ]);

        if ($course) {
            return response([
                'success' => true,
                'message' => 'Course Saved Successfully',
                'data' => $course
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Course Failed to Save'
            ], 200);
        }
    }

    public function update(Request $request)
    {
        $course = Course::find($request->id)->update([
            'title' => $request->title,
        ]);

        if ($course) {
            return response([
                'success' => true,
                'message' => 'Course updated',
                'data' => $course
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Course failed to delete'
            ], 200);
        }
    }

    public function destroy($id)
    {
        $course = Course::where('id', $id)->delete();

        if ($course) {
            return response([
                'success' => true,
                'message' => 'Course deleted',
                'data' => $course
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Course failed to delete'
            ], 200);
        }
    }
}
