<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Groups;
use App\Models\MemberGroup;
use App\Models\MemberPoint;
use App\Models\LeaveLogs;
use App\Models\Tournaments;
use App\Models\TournamentGroup;

use DB;
use Exception;

use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function joinGroup(Request $request)
    {
        DB::beginTransaction();

        try {            
            $data = Groups::where('code', $request->code)->first();

            if ($data) {
                $hasGroup = false;
                $check = MemberGroup::select('nim')->where(array('nim' => $this->nim))->first();

                if ($check) {
                    return response([
                        'success' => false,
                        'message' => 'Unable to join because you have joined a group',
                    ], self::SUCCESS);
                }

                if ($check) {
                    return response([
                        'success' => false,
                        'message' => 'Unable to join because you have joined a group'
                    ], self::SUCCESS);
                } else {
                    $countMember = DB::table('member_group as mg')
                                ->select('mg.id')
                                ->where(array('mg.group_id' => $data->id))
                                ->count();

                    if ($countMember > 3) {
                        return response([
                            'success' => false,
                            'message' => 'Failed to join the group because the group has 3 members'
                        ], self::SUCCESS);
                    }

                    MemberGroup::create([
                        'nim' => $this->nim,
                        'group_id' => $data->id,
                        'is_leader' => false
                    ]);

                    $activeTournament = DB::table('tournaments as t')->select('t.id')
                    ->join('tournament_group as tg', 'tg.tournament_id', 't.id')
                    ->where(array('t.is_active' => true, 'tg.group_id' => $data->id))
                    ->get();

                    foreach ($activeTournament as $tourney) {
                        $existMember = MemberPoint::where(array('nim' => $this->nim, 'tournament_id' => $tourney->id))->first();

                        if ($existMember) {
                            $group = TournamentGroup::where(array('group_id' => $data->id, 'tournament_id' => $tourney->id))->first();

                            TournamentGroup::where(array('group_id' => $data->id, 'tournament_id' => $tourney->id))->update([
                                'total_point' => $group->total_point + $existMember->point
                            ]);
                        } else {
                            MemberPoint::create([
                                'tournament_id' => $tourney->id,
                                'nim' => $this->nim,
                                'point' => 0
                            ]);
                        }
                    }

                    DB::commit();

                    return response([
                        'success' => true,
                        'message' => 'You have successfully joined the group',
                        'data' => $data
                    ], self::SUCCESS);
                }

            } else {
                return response([
                    'success' => false,
                    'message' => 'The group you are looking for was not found'
                ], self::SUCCESS);
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], self::SERVER_ERROR);
        } catch(\Illuminate\Database\QueryException $ex) { 
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], self::SERVER_ERROR);
        }
    }

    public function changeLeader($id)
    {
        DB::beginTransaction();

        try {
            $leader = DB::table('member_group as mg')
            ->select('mg.*')
            ->where(array('mg.nim' => $this->nim))
            ->first();

            if ($leader->is_leader != true) {
                throw new Exception("Failed to change the leader because you are not the group leader", 1);                
            }

            MemberGroup::find($leader->id)->update([
                'is_leader' => false,
            ]);

            $data = MemberGroup::find($id)->update([
                'is_leader' => true,
            ]);

            DB::commit();
                return response([
                    'success' => true,
                    'message' => 'The group leader has been successfully changed',
                    'data' => $data
                ], self::SUCCESS);
        } catch (Exception $ex) {
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], self::SERVER_ERROR);
        } catch(\Illuminate\Database\QueryException $ex) { 
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], self::SERVER_ERROR);
        }
    }

    public function checkGroup()
    {
        $response = [];
        $code = self::SERVER_ERROR;
        $nim = $this->nim;

        try {
            $data = DB::table('student as s')
            ->select('s.nim', 'g.id as group_id', 'g.code', 'g.name as group_name')
            ->join('member_group as mg', 'mg.nim', '=', 's.nim')
            ->join('groups as g', 'g.id', '=', 'mg.group_id')
            ->where('s.nim', $nim)
            ->first();

            if ($data) {
                $message = 'You have joined a group';
            } else {
                $message = 'You have not joined a group yet';
            }

            $response = ['success' => true, 'message' => $message, 'data' => $data];
            $code = self::SUCCESS;
        } catch (Exception $ex) {
            $response = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
        }

        return response($response, $code);
    }

    public function index()
    {
        $data = DB::table('groups as g')
        ->join('tournament_group as tg', 'tg.group_id', '=', 'g.id')
        ->join('tournaments as tn', 'tn.id' ,'=', 'tg.tournament_id')
        ->select('g.*', 'tn.title')
        ->where(array('tn.is_active' => true))
        ->orderBy('g.name', 'asc')
        ->orderBy('tn.title', 'asc')
        ->get();

        return response([
            'success' => true,
            'message' => 'List of groups in active tournaments',
            'data' => $data
        ], 200);
    }

    public function show($id)
    {
        $code = self::SERVER_ERROR;
        $member = [];

        try {            
            $data = Groups::find($id);

            if (!$data) {
                return response([
                    'success' => false,
                    'message' => 'The group you are looking for was not found'
                ], self::SUCCESS);
            }

            $memberGroup = MemberGroup::where('group_id', $data->id)->get();

            $i = 0;
            foreach ($memberGroup as $value) {
                $member[$i]['id'] = $value->id;
                $member[$i]['username'] = $value->student->username;
                $member[$i]['nim'] = $value->nim;
                $member[$i]['group_id'] = $value->group_id;
                $member[$i]['is_leader'] = $value->is_leader;
                $member[$i]['is_leave'] = false;

                $isLeave = LeaveLogs::where(array('member_group_id' => $value->id, 'status' => 'WAITING'))->first();
                if ($isLeave) {
                    $member[$i]['is_leave'] = true;
                }

                $i++;
            }

            $data['member_group'] = $member;

            $response = [
                'success' => true, 
                'message' => 'Detail group', 
                'data' => $data
            ];

            $code = self::SUCCESS;
        } catch (Exception $ex) {
            $response = [
                'success' => false, 
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ];            
        }

        return response($response, $code);
    }

    public function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        if (Groups::where('code', $randomString)->exists()) {
            $this->generateRandomString(6);
        }

        return $randomString;
    }

    public function showTournament($id)
    {
        $code = self::SERVER_ERROR;
        $member = [];

        try {
            $data = DB::table('tournaments as t')
            ->join('tournament_group as tg', 't.id', 'tg.tournament_id')
            ->join('groups as g', 'g.id', 'tg.group_id')
            ->select('t.id', 't.title', 't.is_active', 'tg.total_point', 'tg.place')
            ->where(array('tg.group_id' => $id))
            ->orderBy('t.is_active', 'desc')
            ->orderBy('t.title', 'asc')
            ->get();

            $response = [
                'success' => true, 
                'message' => 'Tourney group', 
                'data' => $data
            ];

            $code = self::SUCCESS;
        } catch (Exception $ex) {
            $response = [
                'success' => false, 
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ];            
        }

        return response($response, $code);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            // generate kode grup
            $code = $this->generateRandomString(6);

            // check member grup existing
            $check = DB::table('member_group as mg')->select('mg.id')->where(array('mg.nim' => $this->nim))->first();

            if (!$check) {
                $existGroup = Groups::where('code', $code)->first();

                if ($existGroup) {
                    throw new Exception("The group code you entered has been registered", 1);                    
                }

                $data = Groups::create([
                    'code' => $code,
                    'name' => $request->name,
                ]);

                MemberGroup::create([
                    'nim' => $this->nim,
                    'group_id' => $data->id,
                    'is_leader' => false
                ]);

                DB::commit();

                return response([
                    'success' => true,
                    'message' => 'Successfully created a group',
                    'data' => $data
                ], 200);
            } else {
                $message = 'Unable to create a group because you have joined the group';

                return response([
                    'success' => false,
                    'message' => $message,
                ], 200);
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], 500);
        } catch(\Illuminate\Database\QueryException $ex) { 
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            $code = $request->code;

            $check = Groups::where('code', $code)->first();

            if ($check) {
                throw new Exception("Failed to change group code due to code ".$code." registered", 1);                
            }

            $data = Groups::find($id)->update([
                'code' => $code,
                'name' => $request->name,
            ]);

            DB::commit();

            return response([
                'success' => true,
                'message' => 'The group has been successfully updated',
                'data' => $data
            ], 200);            
        } catch (Exception $ex) {
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], 500);
        } catch(\Illuminate\Database\QueryException $ex) { 
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], 500);
        }
    }

    public function destroy($id)
    {
    	$data = Groups::where('id', $id)->delete();

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Group deleted successfully',
                'data' => $data
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Group failed to delete'
            ], 200);
        }
    }

    public function indexMember()
    {
    	$data = MemberGroup::all();

        return response([
            'success' => true,
            'message' => 'List member groups',
            'data' => $data
        ], 200);
    }

    public function showMember($id)
    {
    	$data = MemberGroup::where('id', $id)->get();

        return response([
            'success' => true,
            'message' => 'Detail member group',
            'data' => $data
        ], 200);
    }

    public function storeMember(Request $request)
    {
    	$data = MemberGroup::create([
            'nim' => $request->nim,
            'group_id' => $request->group_id,
            'is_leader' => $request->is_leader,
        ]);

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil disimpan',
                'data' => $data
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal disimpan'
            ], 200);
        }
    }

    public function updateMember(Request $request, $id)
    {
    	$data = MemberGroup::find($id)->update([
            'nim' => $request->nim,
            'group_id' => $request->group_id,
            'is_leader' => $request->is_leader,
        ]);

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil diupdate',
                'data' => $data
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal diupdate'
            ], 200);
        }
    }

    public function destroyMember($id)
    {
        $losePoint = 0;
        $totalGroupPoint = 0;

        DB::beginTransaction();

        try {    
            $data = MemberGroup::where('id', $id)->first();

            if (!$data) {
                throw new Exception("Member data not found", 1);                
            }

            if ($data->is_leader) {
                throw new Exception("The leader cannot leave the group", 1); 
            }

            $group_id = $data->group_id;

            $activeTournament = Tournaments::where(array('is_active' => true))->get();

            foreach ($activeTournament as $tourney) {
                $tourGroup = TournamentGroup::where(array('group_id' => $data->group_id, 'tournament_id' => $tourney->id))->first();

                if ($tourGroup) {
                    $totalGroupPoint = $tourGroup->total_point;
                }

                $memberPoint = MemberPoint::where(array('nim' => $data->nim, 'tournament_id' => $tourney->id))->first();

                if ($memberPoint) {
                    $losePoint = $memberPoint->point;
                }

                TournamentGroup::where(array('group_id' => $data->group_id, 'tournament_id' => $tourney->id))->update([
                    'total_point' => $totalGroupPoint - $losePoint,
                ]);
            }

            $delete = $data->delete();

            if ($delete) {
                DB::commit();

                return response([
                    'success' => true,
                    'message' => 'Group member successfully exited',
                    'data' => $data
                ], 200);
            } else {
                return response([
                    'success' => false,
                    'message' => 'Item failed to delete'
                ], 200);
            }
        } catch (Exception $ex) {
            DB::rollBack();

            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], 500);  
        } catch(\Illuminate\Database\QueryException $ex) { 
            DB::rollBack();
            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], 500);
        }
    }

    public function indexPoint()
    {
        $data = MemberPoint::all();

        return response([
            'success' => true,
            'message' => 'List member Points',
            'data' => $data
        ], 200);
    }

    public function showPoint($id)
    {
        $data = MemberPoint::where('id', $id)->get();

        return response([
            'success' => true,
            'message' => 'Detail member Point',
            'data' => $data
        ], 200);
    }

    public function storePoint(Request $request)
    {
        $data = MemberPoint::create([
            'tournament_id' => $request->tournament_id,
            'nim' => $request->nim,
            'point' => $request->point,
        ]);

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil disimpan',
                'data' => $data
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal disimpan'
            ], 200);
        }
    }

    public function updatePoint(Request $request, $id)
    {
        $data = MemberPoint::find($id)->update([
            'tournament_id' => $request->tournament_id,
            'nim' => $request->nim,
            'point' => $request->point,
        ]);

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil diupdate',
                'data' => $data
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal diupdate'
            ], 200);
        }
    }

    public function destroyPoint($id)
    {
        $data = MemberPoint::where('id', $id)->delete();

        if ($data) {
            return response([
                'success' => true,
                'message' => 'Item berhasil didelete',
                'data' => $data
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal didelete'
            ], 200);
        }
    }

    public function leaveGroup(Request $request)
    {
        DB::beginTransaction();

        try {
            $member = MemberGroup::find($request->member_group_id);

            $check = LeaveLogs::where(array('member_group_id' => $request->member_group_id))->exists();

            if ($check) {
                return response([
                    'success' => false,
                    'message' => 'Your request is already processed by admin'
                ], 200);
            }

            if (!$member) {
                return response([
                    'success' => false,
                    'message' => 'Member not found'
                ], 200);
            }

            if ($member->is_leader) {
                return response([
                    'success' => false,
                    'message' => 'The group exit request cannot be processed because you are the group leader'
                ], 200);
            }

            $data = LeaveLogs::create([
                'member_group_id' => $request->member_group_id,
                'nim' => $member->nim,
                'group_id' => $member->group_id,
                'status' => 'WAITING',
                'request_date' => date('Y-m-d H:i:s'),
                'response_date' => date('Y-m-d H:i:s'),
            ]);

            DB::commit();

            return response([
                'success' => true,
                'message' => 'Your request to leave the group has been successfully submitted. Please wait for admin approval',
                'data' => $data
            ], 200);
        } catch (Exception $ex) {
            DB::rollBack();

            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], 500);  
        } catch(\Illuminate\Database\QueryException $ex) { 
            DB::rollBack();

            return response([
                'success' => false,
                'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()
            ], 500);
        }
    }
}
