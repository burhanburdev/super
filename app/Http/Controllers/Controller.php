<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use URL;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $token, $payloads, $nim, $rank_position;

	const SUCCESS = 200;
	const SERVER_ERROR = 500;
	const UNAUTHORIZED = 401;
	const METHOD_NOT_ALLOWED = 405;
	const NOT_FOUND = 404;
	const BAD_REQUEST = 400;
	const REQUEST_TIMEOUT = 408;

	public function __construct()
    {
    	try {
	        $this->token = JWTAuth::getToken(); 

	        if ($this->token) {
		        $this->payloads = JWTAuth::getPayload($this->token)->toArray();
		        $this->nim = $this->payloads['sub'];
		        $this->rank_position = $this->payloads['rank_position'];
	        }
    	} catch (JWTException $e) {
    		
    	}
    }
}
