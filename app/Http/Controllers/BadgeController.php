<?php

namespace App\Http\Controllers;

use App\Models\Badge;
use App\Models\BadgeOwned;
use App\Models\DailyXpAchieve;
use App\Models\UserLogs;
use App\Models\Student;
use Illuminate\Http\Request;

class BadgeController extends Controller
{
    public function index()
    {
    }

    public function show($id)
    {
    }

    public function store(Request $request)
    {
    }

    public function update(Request $request)
    {
    }

    public function destroy($id)
    {
    }

    public function getUserBadgeOwned()
    {

        $badgesOwned = BadgeOwned::where('nim', $this->nim)->get();
        $badgesOwnedDetail = [];

        if ($badgesOwned) {
            foreach ($badgesOwned as $badgeOwned) {
                $badgeDetail = Badge::where('id', $badgeOwned->badge_id)->first();
                array_push($badgesOwnedDetail, $badgeDetail);
            }
            
            return response([
                'success' => true,
                'message' => 'Badge Owned List',
                'data' => $badgesOwnedDetail
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'user not found',
                'data' => $badgesOwnedDetail
            ], 404);
        }
    }

    public function checkStudentBadge(Request $request)
    {
        $userDailyXpAchieve = 0;
        $userDailyLoginLogs = 0;

        $student = Student::where('nim', $this->nim)->first();

        if ($student) {
            $tempUserDailyXpAchieve = DailyXpAchieve::where('nim', $this->nim)->first();

            if ($tempUserDailyXpAchieve) {
                $userDailyXpAchieve = $tempUserDailyXpAchieve->count;
            }

            $tempUserDailyLoginLogs = UserLogs::where('nim', $this->nim)->distinct()->get(['date'])->count();

            if ($tempUserDailyLoginLogs) {
                $userDailyLoginLogs = $tempUserDailyLoginLogs;
            }

            $badges = Badge::where('total_xp', '<=', $student->total_xp)
                ->where('total_crown', '<=', $student->total_crown)
                ->where('daily_goals_achieve', '<=', $userDailyXpAchieve)
                ->where('login_count', '<=', $userDailyLoginLogs)
                ->get();

            $newBadges = [];

            if ($badges) {
                $badgeOwned = BadgeOwned::where('nim', $this->nim)->get()->toArray();

                if ($badgeOwned) {
                    foreach ($badges as $badge) {
                        $foundBadge = array_search($badge->id, array_column($badgeOwned, 'badge_id'));

                        if ($foundBadge === false) {
                            $newBadgeOwned = BadgeOwned::create([
                                'nim' => $this->nim,
                                'badge_id' => $badge->id
                            ]);

                            if ($newBadgeOwned) {
                                $badgeAddedIndex = array_search($badge->id, array_column($badges->toArray(), 'id'));
                                array_push($newBadges, $badges[$badgeAddedIndex]);
                            }
                        }
                    }

                    return response([
                        'success' => true,
                        'message' => 'new badge earned',
                        'data' => $newBadges
                    ], 200);
                } else {
                    foreach ($badges as $badge) {
                        $newBadgeOwned = BadgeOwned::create([
                            'nim' => $this->nim,
                            'badge_id' => $badge->id
                        ]);

                        if ($newBadgeOwned) {
                            $badgeAddedIndex = array_search($badge->id, array_column($badges->toArray(), 'id'));
                            array_push($newBadges, $badges[$badgeAddedIndex]);
                        }
                    }

                    return response([
                        'success' => true,
                        'message' => 'new badge earned',
                        'data' => $newBadges
                    ], 200);
                }
            } else {
                return response([
                    'success' => true,
                    'message' => 'No badge earned',
                    'data' => []
                ], 200);
            }
        } else {
            return response([
                'success' => false,
                'message' => 'User not found',
                'data' => false
            ], 400);
        }
    }
}
