<?php

namespace App\Http\Controllers;

use App\Models\UserLogs;
use Illuminate\Http\Request;

class UserLogsController extends Controller
{
    public function index()
    {
        $logs = UserLogs::all();
        return response([
            'success' => true,
            'message' => 'User Logs List',
            'data' => $logs
        ], 200);
    }

    public function show($id)
    {
        $logs = UserLogs::where('id', $id)->first();
        if ($logs) {
            return response([
                'success' => true,
                'message' => 'User Logs List',
                'data' => $logs
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'User Not Found',
                'data' => []
            ], 500);
        }
    }

    public function store(Request $request)
    {
        $log = UserLogs::create([
            "nim" => $this->nim,
            "date" => $request->date,
            "time" => $request->time,
            "action" => $request->action
        ]);
        
        if ($log) {
            return response([
                'success' => true,
                'message' => 'Logs Created',
                'data' => $log
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Logs Failed to Create',
                'data' => $log
            ], 500);
        }
    }

    public function showByNim()
    {
        $logs = UserLogs::where('nim', $this->nim)->get();

        if ($logs) {
            return response([
                'success' => true,
                'message' => 'User Logs List',
                'data' => $logs
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'User Not Found',
                'data' => []
            ], 500);
        }
    }

    public function update(Request $request)
    {
    }

    public function destroy($id)
    {
    }
}
