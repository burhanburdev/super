<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Encryption\DecryptException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;

USE App\Helpers\AuthHelper;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\Models\Student;
use App\Mail\ResetPassword;

use Crypt;
use DB;
use URL;
use Exception;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $code = self::SERVER_ERROR;
        $response = array();        
    	$credentials = $request->only('username', 'password');

        $client = new Client();

        try {
            $endpoint = URL::current();
            $student = Student::where('username', $credentials['username'])->first();

            if ($student) {
                if ($student->is_sso) {
                    // if (env('APP_ENV') == 'live') {
                        $url = 'https://sso.universitaspertamina.ac.id/api/login';
                    // } else {
                    //     $url = 'http://localhost/pertamina/public/api/login';
                    // }

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($credentials));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $output = curl_exec($ch);            
                    curl_close($ch);

                    $res = json_decode($output);

                    if ($res->status) {
                        $leaderboard = Student::orderByRaw('total_xp DESC, total_crown DESC')->get()->toArray();
                        $rankPosition =  array_search($student->nim, array_column($leaderboard, 'nim'));

                        $customClaims = array(
                            'username' => $credentials['username'],
                            'rank_position' => $rankPosition + 1
                        );     

                        $token = Auth::guard('api')->claims($customClaims)->login($student);

                        $code = self::SUCCESS;
                        $response = array(
                            'success' => true,
                            'message' => 'Login success',
                            'data' => $token
                        );
                    } else {
                        $code = self::UNAUTHORIZED;
                        $response = array(
                            'success' => false,
                            'message' => 'Username or password is incorrect'
                        );
                    }
                } else {
                    $is_valid = Auth::guard('api')->attempt($credentials);

                    // check validatation in JWTGuard
                    if (!$is_valid) {
                        $code = self::UNAUTHORIZED;
                        $response = array(
                            'success' => false,
                            'message' => 'Username or password is incorrect'
                        );
                    } else {
                        $leaderboard = Student::orderByRaw('total_xp DESC, total_crown DESC')->get()->toArray();
                        $rankPosition =  array_search($student->nim, array_column($leaderboard, 'nim'));

                        $customClaims = array(
                            'username' => $credentials['username'],
                            'rank_position' => $rankPosition + 1
                        );     

                        $token = Auth::guard('api')->claims($customClaims)->attempt($credentials);

                        $code = self::SUCCESS;
                        $response = array(
                            'success' => true,
                            'message' => 'Login success',
                            'data' => $token
                        );
                    }
                }
            } else {
                $code = self::UNAUTHORIZED;
                $response = array(
                    'success' => false,
                    'message' => 'User you were looking for was not found'
                );
            }
        } catch (Exception $e) {
            $response = array(
                'success' => false,
                'message' => $e->getMessage(),    
                'file' => $e->getFile(),
                'line' => $e->getLine(),           
                'url' => $endpoint
            );
        }

        return response()->json($response, $code);
    }

    public function refreshToken(Request $request)
    {
        $endpoint = URL::current();

        try {
            if (!JWTAuth::getToken()) {
                $refreshed = JWTAuth::refresh($request->get('token'));
            } else {                
                $refreshed = JWTAuth::refresh(JWTAuth::getToken());
            }

            $code = self::SUCCESS;
            $response = array(
                'success' => true,
                'message' => 'Refresh token success',
                'data' => $refreshed
            );
        } catch (JWTException $e) {
            $code = self::SERVER_ERROR;
            $response = array(
                'success' => false,
                'message' => $e->getMessage(),    
                'file' => $e->getFile(),
                'line' => $e->getLine(),           
                'url' => $endpoint
            );
        }

        return response()->json($response, $code);
    }

    public function logout(Request $request)
    {
        $endpoint = URL::current();

        $token = $request->header('Authorization');
        
        try {
            JWTAuth::parseToken()->invalidate($token);

            $code = self::SUCCESS;
            $response = [
                'success' => true,
                'message' => 'You have logged out'
            ];
        } catch (JWTException $e) {
        	$code = self::SERVER_ERROR;

            $response = array(
                'success' => false,
                'message' => $err->getMessage(),    
                'file' => $err->getFile(),
                'line' => $err->getLine(),             
                'url' => $endpoint
            );
        }

        return response()->json($response, $code);
    }

    public function ssoActivate(Request $request)
    {
        $endpoint = URL::current();
        $response = array();        
        $credentials = $request->only('username', 'password');

        $client = new Client();

        try {
            if (env('APP_ENV') == 'live') {
                $url = 'https://sso.universitaspertamina.ac.id/api/login';
            } else {
                $url = 'http://localhost/pertamina/public/api/login';
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($credentials));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);            
            curl_close($ch);

            $res = json_decode($output);

            if (!$res) {
                $code = self::UNAUTHORIZED;
                $response = array(
                    'success' => false,
                    'message' => 'Incorrect username or password',            
                    'url' => $endpoint
                );
            } else {
                if ($res->status) {
                    $data = $res->data;

                    if (Student::where('username', $data->username)->first()) {
                        throw new Exception("Your SSO account has already been activated", 1);
                    }

                    $student = Student::create([
                        'nim' => $data->code,
                        'nama' => $data->name,
                        'username' => $data->username,
                        'email' => $data->email,
                        'password' => Hash::make('chemfyi'),
                        'class_code' => substr($data->code, 1, 3),
                        'total_xp' => 0,
                        'daily_goal' => 0,
                        'total_crown' => 0,
                        'daily_goal' => 100,
                        'is_sso' => 1,
                        'avatar' => 'default.png',
                        // 'avatar' => $pathName,
                    ]);

                    $code = self::SUCCESS;
                    $response = array(
                        'success' => true,
                        'message' => 'Activation success',
                        'data' => $student
                    );
                } else {
                    $code = self::UNAUTHORIZED;
                    $response = array(
                        'success' => false,
                        'message' => 'Incorrect username or password',            
                        'url' => $endpoint
                    );
                }
            }
        } catch (JWTException $e) {
            $code = self::SERVER_ERROR;
            $response = array(
                'success' => false,
                'message' => $e->getMessage(),    
                'file' => $e->getFile(),
                'line' => $e->getLine(),           
                'url' => $endpoint
            );
        } catch (Exception $ex) {
            $code = self::SERVER_ERROR;
            $response = array(
                'success' => false,
                'message' => $ex->getMessage(),
            );
        }

        return response()->json($response, $code);
    }
}
