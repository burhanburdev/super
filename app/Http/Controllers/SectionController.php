<?php

namespace App\Http\Controllers;

use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function index()
    {
        $Section = Section::all();
        return response([
            'success' => true,
            'message' => 'List Section',
            'data' => $Section
        ], 200);
    }

    public function show($id)
    {
        $Section = Section::where('id', $id)->get();
        return response([
            'success' => true,
            'message' => 'Detail Section',
            'data' => $Section
        ], 200);
    }

    public function showByCourseId($id)
    {
        $Section = Section::where('course_id', $id)->get()->load('exercises')->load('exercises.questions');
        
        return response([
            'success' => true,
            'message' => 'Detail Section',
            'data' => $Section
        ], 200);
    }

    public function store(Request $request)
    {
        $Section = Section::create([
            'title' => $request->title,
            'course_id' => $request->course_id,
        ]);

        if ($Section) {
            return response([
                'success' => true,
                'message' => 'Section Saved',
                'data' => $Section
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Section failed to save'
            ], 200);
        }
    }

    public function update(Request $request)
    {
        $Section = Section::find($request->id)->update([
            'title' => $request->title,
            'course_id' => $request->course_id,
        ]);

        if ($Section) {
            return response([
                'success' => true,
                'message' => 'Section updated',
                'data' => $Section
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Section failed to updated'
            ], 200);
        }
    }

    public function destroy($id)
    {
        $Section = Section::where('id', $id)->delete();

        if ($Section) {
            return response([
                'success' => true,
                'message' => 'Section deleted',
                'data' => $Section
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Section failed to delete'
            ], 200);
        }
    }
}
