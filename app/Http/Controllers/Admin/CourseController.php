<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;

use App\Services\CourseService;

use App\Models\Section;
use App\Models\Subsection;

use Alert;
use URL;
use Exception;

class CourseController extends Controller
{
	protected $courseService;

	public function __construct(CourseService $courseService)
	{
		$this->courseService = $courseService;
	}

	public function index()
	{
		$data = $this->courseService->getAllCourse();

		return view('admin.courses.index', get_defined_vars());
	}

	public function edit($id)
	{
		$data = $this->courseService->getCourseById($id);

		return view('admin.courses.edit', get_defined_vars())->renderSections()['content'];	
	}

	public function show($id)
	{
		$data = $this->courseService->getCourseById($id);

		return view('admin.courses.show', get_defined_vars());	
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'title' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->courseService->saveCourse($request);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil ditambah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'title' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->courseService->updateCourse($request, $id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil diubah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function destroy($id)
	{
		$this->courseService->deleteCourse($id);

		Alert::success('Success', 'Data berhasil dihapus');

		return redirect()->route('courses');
	}
}