<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;

use App\Services\StudentService;

use App\Models\Prodi;

use App\Helpers\Consumer;

use Alert;
use URL;
use Exception;

class StudentController extends Controller
{
	protected $studentService;

	public function __construct(StudentService $studentService)
	{
		$this->url = URL::current();
		$this->studentService = $studentService;
	}

	public function index(Request $request)
	{
		$peserta = null;
        $pesertaAjax = null;

        if ($request->get('peserta') == 'eksternal') {
            $peserta = '00';
            $pesertaAjax = 'eksternal';
        }

        if ($request->get('peserta') == 'internal') {
            $peserta = '1';
            $pesertaAjax = 'internal';
        }

        $data = $this->studentService->getAllStudents();

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('jenis', function($data) {
		        	if ($data->is_sso) {
                        $paid = 'Internal';
                    } else {
                        $paid = 'Eksternal';
                    }
                    
                    return $paid;
		        })
		        ->addColumn('action', function ($data) {
		            return '
		            	<a href="#" value="'.e(route('show.student', $data->nim)).'" class="btn btn-primary btn-xs modalMd" title="Detail Peserta" data-toggle="modal" data-target="#modalMd"><span class="fas fa-search"></span></a>
                    	<a href="#" value="'.e(route('edit.student', $data->nim)).'" class="btn btn-info btn-xs modalEdit" title="Ubah Data" data-toggle="modal" data-target="#modalEdit"><span class="fas fa-pencil-alt"></span></a>
                    	<a href="#" value="'.e(route('edit.password', $data->nim)).'" class="btn btn-success btn-xs modalPass" title="Ubah Password" data-toggle="modal" data-target="#modalPass"><span class="fas fa-unlock"></span></a>
		            ';
		        })
		        ->toJson();
		}

		return view('admin.students.index', get_defined_vars());
	}

	public function show($nim)
	{
		$data = $this->studentService->getStudentByNim($nim);

		return view('admin.students.show', get_defined_vars())->renderSections()['content'];
	}

	public function edit($nim)
	{
		$data = $this->studentService->getStudentByNim($nim);
		$prodi = Prodi::all();

		return view('admin.students.edit', get_defined_vars())->renderSections()['content'];		
	}

	public function editPassword($nim)
	{
		$data = $this->studentService->getStudentByNim($nim);

		return view('admin.students.password', get_defined_vars())->renderSections()['content'];		
	}

	public function store(Request $request)
	{
		Alert::success('Success', 'Data berhasil ditambah');

		return redirect()->back();
	}

	public function update(Request $request, $nim)
	{
		$validator = Validator::make($request->all(), [
			'nama' => 'required',
			'email' => 'required|email',
			'class_code' => 'required',
			'total_xp' => 'required', 
			'total_crown' => 'required',
			'avatar' => 'image|mimes:jpeg,jpg,png|max:1024',
			'daily_goal' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }
		
		$data = $this->studentService->updateStudent($request, $nim);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil diubah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function updatePassword(Request $request, $nim)
	{
		$validator = Validator::make($request->all(), [
			'password' => 'required|confirmed|min:6',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }
		
		$data = $this->studentService->updatePassword($request, $nim);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil diubah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function destroy($nim)
	{
		$this->studentService->deleteStudent($nim);

		Alert::success('Success', 'Data berhasil dihapus');

		return redirect()->route('students');
	}
}