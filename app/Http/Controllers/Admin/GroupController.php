<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use App\Services\GroupService;

use App\Models\Student;
use App\Models\TournamentGroup;

use Alert;
use URL;
use Exception;

class GroupController extends Controller
{
	protected $groupService;

	public function __construct(GroupService $groupService)
	{
		$this->url = URL::current();
		$this->groupService = $groupService;
	}

	public function index(Request $request)
	{
		$data = $this->groupService->getAllGroups();

		if ($data['success']) {
			$data = $data['data'];
		}

		$students = Student::getAllStudents();

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('turnamen', function ($data) {
		        	$returnValue = [];

		        	foreach ($data->tournamentGroup as $value) {
		        		$returnValue[] = $value->tournaments->title;
		        	}

		        	return $returnValue;
		        })
		        ->addColumn('total_point', function ($data) {
		        	$returnValue = [];

		        	foreach ($data->tournamentGroup as $value) {
		        		$returnValue[] = $value->total_point;
		        	}
		        	
		        	return $returnValue;
		        })
		        ->addColumn('action', function ($data) {
		            return '
		            	<a href="#" value="'.e(route('show.group', $data->id)).'" class="btn btn-primary btn-xs modalMd" title="Detail Grup" data-toggle="modal" data-target="#modalMd"><span class="fas fa-search"></span></a>
                    	<a href="#" value="'.e(route('edit.group', $data->id)).'" class="btn btn-info btn-xs modalEdit" title="Ubah Data" data-toggle="modal" data-target="#modalEdit"><span class="fas fa-pencil-alt"></span></a>
                    	<form style="display: inline;" method="POST" action="'.e(route('destroy.group', $data->id)).'">
                    		<input type="hidden" name="_token" value="'.csrf_token().'">
                    		<input type="hidden" name="_method" value="DELETE">
                    		<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('."'Total poin yang dimiliki oleh grup akan hilang apabila Anda menghapus grup. Apakah anda yakin akan menghapus data ini?'".')">
                    			<span class="fa fa-trash"></span>
                    		</button>
                    	</form>
		            ';
		        })
		        ->toJson();
		}

		return view('admin.groups.index', get_defined_vars());
	}

	public function show($id)
	{
		$data = $this->groupService->getGroupInActiveTournamentById($id);

		if ($data['success']) {
			$data = $data['data'];
		}

		$members = $this->groupService->getMemberGroup($id);

		if ($members['success']) {
			$members = $members['data'];
		}

		$tournaments = TournamentGroup::where('group_id', $id)->get();

		return view('admin.groups.show', get_defined_vars())->renderSections()['content'];	
	}

	public function edit($id)
	{
		$data = $this->groupService->getGroupById($id);

		if ($data['success']) {
			$data = $data['data'];
		}

		return view('admin.groups.edit', get_defined_vars())->renderSections()['content'];	
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'code' => 'required',
			'name' => 'required',
			'nim' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->groupService->saveGroup($request);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil ditambah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'code' => 'required',
			'name' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->groupService->updateGroup($request, $id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil diubah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function delete($id)
	{
		$data = $this->groupService->deleteGroup($id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil dihapus');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->route('groups');
	}

	public function addMember(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'code' => 'required',
			'name' => 'required',
			'nim' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

		$data = $this->groupService->addMember($request);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil ditambah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->route('groups');
	}

	public function kickMember($id)
	{
		$data = $this->groupService->kickMember($id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil dihapus');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->route('groups');
	}
}