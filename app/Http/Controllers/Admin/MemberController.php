<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

use Yajra\DataTables\Facades\DataTables;

use App\Http\Controllers\Controller;

use App\Models\Groups;
use App\Models\MemberGroup;
use App\Models\LeaveLogs;
use App\Models\MemberPoint;
use App\Models\Tournaments;
use App\Models\TournamentGroup;

use DB;
use Alert;
use Exception;

class MemberController extends Controller
{
	public function index(Request $request)
	{
		$data = LeaveLogs::where('status', 'WAITING')->orderBy('request_date', 'DESC')->get();

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('nim', function ($data) {
		        	return @$data->member->nim;
		        })
		        ->addColumn('nama', function ($data) {
		        	return @$data->member->student->nama;
		        })
		        ->addColumn('kelompok', function ($data) {
		        	return @$data->member->groups->name;
		        })
		        ->addColumn('tanggal', function ($data) {
		        	return @tanggal(date('Y-m-d', strtotime($data->request_date)));
		        })
		        ->addColumn('action', function($data) {
		        	return '<input type="checkbox" name="checkBox[]" value="'.e($data->id).'">';
		        })
		        ->toJson();
		}

		return view('admin.members.index', get_defined_vars());
	}

	public function approval(Request $request)
	{
        $losePoint = 0;
        $totalGroupPoint = 0;

		DB::beginTransaction();

		try {
			$members = $request->checkBox;
			$status = $request->approval;

			foreach ($members as $value) {
				$leave = LeaveLogs::find($value);

				$data = MemberGroup::find($leave->member_group_id);
				if (!$data) {
	                throw new Exception("Member data not found", 1);                
	            }

				$leave->update([
					'status' => $status,
				]);

	            $group_id = $data->group_id;

	            $activeTournament = Tournaments::where(array('is_active' => true))->get();
	            foreach ($activeTournament as $tourney) {
	                $tourGroup = TournamentGroup::where(array('group_id' => $data->group_id, 'tournament_id' => $tourney->id))->first();

	                if ($tourGroup) {
	                    $totalGroupPoint = $tourGroup->total_point;
	                }

	                $memberPoint = MemberPoint::where(array('nim' => $data->nim, 'tournament_id' => $tourney->id))->first();

	                if ($memberPoint) {
	                    $losePoint = $memberPoint->point;
	                }

	                TournamentGroup::where(array('group_id' => $data->group_id, 'tournament_id' => $tourney->id))->update([
	                    'total_point' => $totalGroupPoint - $losePoint,
	                ]);
	            }

	            $delete = $data->delete();
			}

			DB::commit();
			Alert::success('Success', 'Approval berhasil dilakukan');
		} catch (Exception $ex) {
			DB::rollBack();
			Alert::error('Error', $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine());
		}

		return redirect()->back();
	}
}