<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

use Yajra\DataTables\Facades\DataTables;

use App\Http\Controllers\Controller;

use App\Models\UserLogs;
use App\Models\ExerciseAnswerLogs;
use App\Models\TournamentAnswerLogs;

use Alert;
use Exception;

class ActivityController extends Controller
{
	public function index(Request $request)
	{
		$data = UserLogs::orderBy('date', 'DESC')
		->orderBy('time', 'DESC')
		->get();

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('nama', function($data) {
		        	return @$data->student->nama;
		        })
		        ->addColumn('tanggal', function($data) {
		        	return tanggal($data->date);
		        })
		        ->addColumn('aktivitas', function($data) {
                    return $data->action;
                })
		        ->toJson();
		}

		return view('admin.activities.index', get_defined_vars());
	}

	public function exercise(Request $request)
	{
		$data = ExerciseAnswerLogs::orderBy('created_at', 'DESC')->get();

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('tanggal', function($data) {
		        	return tanggal(strtotime('Y-m-d', $data->created_at));
		        })
		        ->addColumn('jam', function($data) {
		        	return strtotime('H:i:s', $data->created_at);
		        })
		        ->toJson();
		}

		return view('admin.activities.exercise', get_defined_vars());
	}

	public function tournament(Request $request)
	{
		$data = TournamentAnswerLogs::orderBy('created_at', 'DESC')->get();

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('turnamen', function($data) {
		        	return $data->tournamentPeriod->tournament->title;
		        })
		        ->addColumn('nim', function($data) {
		        	return $data->memberGroup->nim;
		        })
		        ->addColumn('tanggal', function($data) {
		        	return tanggal(strtotime('Y-m-d', $data->created_at));
		        })
		        ->addColumn('jam', function($data) {
		        	return strtotime('H:i:s', $data->created_at);
		        })
		        ->toJson();
		}

		return view('admin.activities.tournament', get_defined_vars());
	}
}