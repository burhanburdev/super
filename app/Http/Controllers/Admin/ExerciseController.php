<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Yajra\DataTables\Facades\DataTables;
use App\Services\ExerciseService;

use App\Http\Controllers\Controller;

use App\Models\Subsection;
use App\Models\Exercise;

use App\Models\Question;

use DB;
use Alert;
use Exception;

class ExerciseController extends Controller
{
	protected $exerciseService;

	public function __construct(ExerciseService $exerciseService)
	{
		$this->exerciseService = $exerciseService;
	}

	public function showBySubsectionId(Request $request, $subsectionId)
	{
		$subsection = Subsection::find($subsectionId);
		$data = $this->exerciseService->getExerciseBySubsection($subsectionId);

		if ($data['success']) {
			$data = $data['data'];
		}

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('action', function ($data) {
		            return '
                    	<a href="'.e(route('show.exercise', $data->id)).'" class="btn btn-primary btn-xs" title="Lihat Soal Latihan"><span class="fas fa-search"></span></a>
                    	<a href="#" value="'.e(route('edit.exercise', $data->id)).'" class="btn btn-info btn-xs modalEdit" title="Ubah Data" data-toggle="modal" data-target="#modalEdit"><span class="fas fa-pencil-alt"></span></a>
                    	<form style="display: inline;" method="POST" action="'.e(route('destroy.exercise', $data->id)).'">
                    		<input type="hidden" name="_token" value="'.csrf_token().'">
                    		<input type="hidden" name="_method" value="DELETE">
                    		<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('."'Apakah anda yakin akan menghapus data ini?'".')">
                    			<span class="fa fa-trash"></span>
                    		</button>
                    	</form>
		            ';
		        })
		        ->toJson();
		}

		return view('admin.courses.exercises.index', get_defined_vars());
	}

	public function show(Request $request, $id)
	{
		$data = Exercise::find($id);
		$questions = $this->exerciseService->getQuestionByExerciseId($id);

		if ($questions['success']) {
			$questions = $questions['data'];
		}

		if ($request->ajax()) {
			return Datatables::of($questions)
		        ->addIndexColumn()
		        ->addColumn('text', function($questions) {
		        	$question_text = substr($questions->question_text, 0, 50);

		        	if (strlen($question_text) > 50) {
		        		$question_text = $question_text.'...';
		        	}

		        	return $question_text;
		        })
		        ->addColumn('group', function($questions) {
		        	if (!$questions->is_group) {
		        		$group = 'Personal';
		        	} else {
		        		$group = 'Turnamen';
		        	}

		        	return $group;
		        })
		        ->addColumn('action', function ($questions) {
		            return '
                    	<form style="display: inline;" method="POST" action="'.e(route('duplicate.question', $questions->id)).'">
                    		<input type="hidden" name="_token" value="'.csrf_token().'">
                    		<button type="submit" class="btn btn-primary btn-xs" onclick="return confirm('."'Apakah anda yakin akan menduplikasi data ini?'".')">
                    			<span class="far fa-copy"></span>
                    		</button>
                    	</form>
                    	<a href="'.e(route('edit.question', $questions->id)).'" class="btn btn-info btn-xs" title="Ubah Soal Latihan"><span class="fas fa-pencil-alt"></span></a>
                    	<form style="display: inline;" method="POST" action="'.e(route('destroy.question', $questions->id)).'">
                    		<input type="hidden" name="_token" value="'.csrf_token().'">
                    		<input type="hidden" name="_method" value="DELETE">
                    		<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('."'Apakah anda yakin akan menghapus data ini?'".')">
                    			<span class="fa fa-trash"></span>
                    		</button>
                    	</form>
		            ';
		        })
		        ->toJson();
		}

		return view('admin.exercises.show', get_defined_vars());
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'subsection_id' => 'required',
			'title' => 'required',
			'level' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->exerciseService->saveExercise($request);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil ditambah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'subsection_id' => 'required',
			'title' => 'required',
			'level' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->exerciseService->updateExercise($request, $id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil diubah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function destroy($id)
	{
		$data = $this->exerciseService->deleteExercise($id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil dihapus');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function createQuestion(Request $request, $exerciseId)
	{
		return view('admin.exercises.questions.add', get_defined_vars());
	}

	public function storeQuestion(Request $request)
	{
		$validator = Validator::make($request->all(), [
			// 'question_text' => 'required',
			'question_image' => 'image|mimes:jpeg,png,jpg|max:1024',
			'exercise_id' => 'required',
			'level' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->exerciseService->saveQuestion($request);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil ditambah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function editQuestion($id)
	{
		$data = $this->exerciseService->getQuestionById($id);

		if ($data['success']) {
			$data = $data['data'];
		}

		$countSub = count((array) $data->subquestions);

		return view('admin.exercises.questions.edit', get_defined_vars());
	}

	public function updateQuestion(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			// 'question_text' => 'required',
			'question_image' => 'image|mimes:jpeg,png,jpg|max:1024',
			'exercise_id' => 'required',
			'level' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->exerciseService->updateQuestion($request, $id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil diubah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function destroyQuestion($id)
	{
		$data = $this->exerciseService->deleteSubquestionByQuestiodId($id);
		$data = $this->exerciseService->deleteQuestion($id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil dihapus');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function destroySubQuestion($id)
	{
		$data = $this->exerciseService->deleteSubquestion($id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil dihapus');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function duplicateQuestion($id)
	{
		DB::beginTransaction();

		try {
	    	$question_id = null;

			$question = $this->exerciseService->getQuestionById($id);

			if ($question['success']) {
				$question = $question['data'];

	            $data = new Question;
				$data->question_text = $question->question_text;
				$data->question_image = $question->question_image;
				$data->exercise_id = $question->exercise_id;
				$data->level = $question->level;
				$data->is_group = $question->is_group;
				$data->is_active = $question->is_active;
				$data->save();

				$question_id = $data->id;
			}

			$subquestions = $this->exerciseService->getSubquestionByQuestionId($id);

			if ($subquestions['success']) {
				$subquestions = $subquestions['data'];

				foreach ($subquestions as $sub) {
					$sub->question_id = $question_id;

					$this->exerciseService->saveSubquestion($sub);
				}
			}

			DB::commit();
			Alert::success('Success', 'Data berhasil diduplikasi');
		} catch (Exception $ex) {
			DB::rollBack();
			
			Alert::error('Error', $ex->getMessage());
		}

		return redirect()->back();
	}
}