<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

use App\Models\Question;
use App\Models\ExcludeRiddle;

use Alert;
use Exception;

class QuestionController extends Controller
{
	public function index(Request $request)
	{
		$data = Question::all();

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('text', function($data) {
		        	$question_text = substr($data->question_text, 0, 50);

		        	if (strlen($question_text) > 50) {
		        		$question_text = $question_text.'...';
		        	}

		        	return $question_text;
		        })
		        ->addColumn('group', function($data) {
		        	if (!$data->is_group) {
		        		$group = 'Personal';
		        	} else {
		        		$group = 'Turnamen';
		        	}

		        	return $group;
		        })
		        ->addColumn('action', function ($data) {
		            return '
		            	<form style="display: inline;" method="POST" action="'.e(route('duplicate.questionbank', $data->id)).'">
                    		<input type="hidden" name="_token" value="'.csrf_token().'">
                    		<button type="submit" class="btn btn-primary btn-xs" onclick="return confirm('."'Apakah anda yakin akan menduplikasi data ini?'".')">
                    			<span class="far fa-copy"></span>
                    		</button>
                    	</form>
                    	<a href="'.e(route('edit.questionbank', $data->id)).'" class="btn btn-info btn-xs" title="Ubah Data"><span class="fas fa-pencil-alt"></span></a>
                    	<form style="display: inline;" method="POST" action="'.e(route('delete.questionbank', $data->id)).'">
                    		<input type="hidden" name="_token" value="'.csrf_token().'">
                    		<input type="hidden" name="_method" value="DELETE">
                    		<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('."'Apakah anda yakin akan menghapus data ini?'".')">
                    			<span class="fa fa-trash"></span>
                    		</button>
                    	</form>
		            ';
		        })
		        ->toJson();
		}

		return view('admin.questions.index', get_defined_vars());
	}

	public function createQuestion()
	{

	}

	public function editQuestion($id)
	{

	}

	public function duplicateQuestion(Request $request, $id)
	{

	}

	public function storeQuestion(Request $request)
	{

	}

	public function updateQuestion(Request $request, $id)
	{

	}

	public function deleteQuestion($id)
	{

	}

	// tournament question
	public function list($id)
	{
		return view('admin.tournaments.questions.index', get_defined_vars());
	}

	public function active(Request $request, $id)
	{
		$data = Question::where(array('is_active' => 1, 'is_group' => 0))
		->whereNotIn('id', function($query) use ($id) {
			$query->select('question_id')->from('exclude_riddle')->where(array('tournament_id' => $id))->get();
		})
		->get();

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('active_status', function($data) {
		        	if ($data->is_active) { 
		        		$status = 'Aktif'; 
		        	} else { 
		        		$status = 'Nonaktif';
		        	}

		        	return $status;
		        })
		        ->addColumn('subbab', function($data) {
		        	return $data->exercise->title;
		        })
		        ->addColumn('question', function($data) {
		        	return substr(strip_tags($data->question_text), 0, 50).' ...';
		        })
		        ->addColumn('action', function($data) {
		        	return '<input type="checkbox" name="exclude[]" value="'.e($data->id).'">';
		        })
		        ->toJson();
		}
	}

	public function deactive(Request $request, $id)
	{
		$data = ExcludeRiddle::where(array('tournament_id' => $id))->get();

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('active_status', function($data) {
		        	if ($data->question->is_active) { 
		        		$status = 'Aktif'; 
		        	} else { 
		        		$status = 'Nonaktif';
		        	}
		        	
		        	return $status;
		        })
		        ->addColumn('subbab', function($data) {

		        	return $data->question->exercise->title;
		        })
		        ->addColumn('level', function($data) {

		        	return $data->question->level;
		        })
		        ->addColumn('question', function($data) {
		        	return substr(strip_tags($data->question->question_text), 0, 50).' ...';
		        })
		        ->addColumn('action', function($data) {
		        	return '<input type="checkbox" name="include[]" value="'.e($data->id).'">';
		        })
		        ->toJson();
		}
	}

	public function include(Request $request)
	{
		try {
			$include = $request->include;

			if (!$include) {
				throw new Exception("Silahkan pilih soal terlebih dahulu", 1);
			}

			foreach ($include as $value) {
				$data = ExcludeRiddle::find($value);

				$data->delete();
			}

			Alert::success('Success', 'Soal berhasil diaktifkan');
		} catch (Exception $ex) {
			Alert::error('Error', $ex->getMessage());
		}

		return redirect()->back();
	}

	public function exclude(Request $request)
	{
		try {			
			$exclude = $request->exclude;

			if (!$exclude) {
				throw new Exception("Silahkan pilih soal terlebih dahulu", 1);
			}

			foreach ($exclude as $value) {
				$data = new ExcludeRiddle;

				$data->tournament_id = $request->tournament_id;
				$data->question_id = $value;

				$data->save();
			}

			Alert::success('Success', 'Soal berhasil dinonaktifkan');
		} catch (Exception $ex) {
			Alert::error('Error', $ex->getMessage());
		}

		return redirect()->back();
	}
}