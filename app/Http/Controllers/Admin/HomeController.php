<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Course;
use App\Models\Tournaments;
use App\Models\Student;
use App\Models\Groups;

use Alert;
use Exception;

class HomeController extends Controller
{
	public function index()
	{
		$course = Course::count();
		$tournament = Tournaments::count();
		$student = Student::count();
		$group = Groups::count();

		return view('admin.home', get_defined_vars());
	}
}