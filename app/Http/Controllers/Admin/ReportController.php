<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Alert;
use Exception;

class ReportController extends Controller
{
	public function index()
	{
		return view('admin.reports.index', get_defined_vars());
	}
}