<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use App\Services\TournamentService;

use App\Models\Groups;

use Alert;
use URL;
use Exception;

class TournamentController extends Controller
{
	protected $tournamentService;

	public function __construct(TournamentService $tournamentService)
	{
		$this->url = URL::current();
		$this->tournamentService = $tournamentService;
	}

	public function index(Request $request)
	{
		$data = $this->tournamentService->getAllTournaments();

		if ($request->ajax()) {
			return Datatables::of($data)
		        ->addIndexColumn()
		        ->addColumn('mulai', function($data) { 
		        	return tanggal($data->start_date);
		        })
		        ->addColumn('selesai', function($data) { 
		        	return tanggal($data->end_date);
		        })
		        ->addColumn('status', function($data) {
		        	if ($data->is_active) {
                        $status = 'Aktif';
                    } else {
                        $status = 'Non aktif';
                    }
                    
                    return $status;
		        })
		        ->addColumn('action', function ($data) {
		            return '
		            	<a href="'.e(route('tournament.riddles', $data->id)).'" class="btn btn-success btn-xs" title="Soal Turnamen"><span class="fas fa-book"></span></a>
		            	<a href="'.e(route('show.tournament', $data->id)).'" class="btn btn-primary btn-xs" title="Detail Turnamen"><span class="fas fa-search"></span></a>
                    	<a href="#" value="'.e(route('edit.tournament', $data->id)).'" class="btn btn-info btn-xs modalEdit" title="Ubah Data" data-toggle="modal" data-target="#modalEdit"><span class="fas fa-pencil-alt"></span></a>
                    	<form style="display: inline;" method="POST" action="'.e(route('destroy.tournament', $data->id)).'">
                    		<input type="hidden" name="_token" value="'.csrf_token().'">
                    		<input type="hidden" name="_method" value="DELETE">
                    		<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('."'Apakah anda yakin akan menghapus data ini?'".')">
                    			<span class="fa fa-trash"></span>
                    		</button>
                    	</form>
		            ';
		        })
		        ->toJson();
		}

		return view('admin.tournaments.index', get_defined_vars());
	}

	public function show(Request $request, $id)
	{
		$data = $this->tournamentService->getTournamentById($id);
		$dropdownGroups = Groups::all();

		$groups = $data->tournamentGroup;

		if ($request->ajax()) {
			return Datatables::of($groups)
		        ->addIndexColumn()
		        ->addColumn('group_code', function($groups) {
		        	return $groups->groups->code;
		        })
		        ->addColumn('group_name', function($groups) {
		        	return $groups->groups->name;
		        })
		        ->addColumn('members', function($groups) {
		        	$returnValue = [];

		        	foreach ($groups->groups->memberGroup as $value) {
		        		$returnValue[] = $value->student->nim.' - '.$value->student->nama;
		        	}

		        	return $returnValue;
		        })
		        ->addColumn('action', function ($groups) {
		            return '
                    	<form style="display: inline;" method="POST" action="'.e(route('delete.group.tournament', $groups->id)).'">
                    		<input type="hidden" name="_token" value="'.csrf_token().'">
                    		<input type="hidden" name="_method" value="DELETE">
                    		<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('."'Apakah anda yakin akan menghapus data ini?'".')">
                    			<span class="fa fa-trash"></span>
                    		</button>
                    	</form>
		            ';
		        })
		        ->toJson();
		}

		return view('admin.tournaments.show', get_defined_vars());	
	}

	public function edit($id)
	{
		$data = $this->tournamentService->getTournamentById($id);

		return view('admin.tournaments.edit', get_defined_vars())->renderSections()['content'];
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'start_date' => 'required',
			'end_date' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->tournamentService->saveTournament($request);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil ditambah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function storeGroup(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'tournament_id' => 'required',
			'group' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->tournamentService->addGroupTournament($request);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil ditambah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function deleteGroup($id)
	{
		$this->tournamentService->deleteTournamentGroup($id);

		Alert::success('Success', 'Data berhasil dihapus');

		return redirect()->back();
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'start_date' => 'required',
			'end_date' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->tournamentService->updateTournament($request, $id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil diubah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function updateStatusActive($id)
	{
		$this->tournamentService->updateStatusActive($id);

		Alert::success('Success', 'Status berhasil diubah');

		return redirect()->route('tournaments');
	}

	public function delete($id)
	{
		$this->tournamentService->deleteTournament($id);

		Alert::success('Success', 'Data berhasil dihapus');

		return redirect()->route('tournaments');
	}
}