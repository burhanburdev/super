<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;

use App\Services\SectionService;

use App\Models\Section;
use App\Models\Subsection;

use Alert;
use URL;
use Exception;

class SectionController extends Controller
{
	protected $sectionService;

	public function __construct(SectionService $sectionService)
	{
		$this->sectionService = $sectionService;
	}

	public function edit($id)
	{
		$data = $this->sectionService->editSection($id);

		return view('admin.sections.edit', get_defined_vars())->renderSections()['content'];	
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'level' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->sectionService->updateSection($request, $id);

        if ($data['success']) {			
			Alert::success('Success', 'Data berhasil diubah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'course_id' => 'required',
			'title' => 'required',
			'level' => 'required',
		]);

		if ($validator->fails()) {
        	Alert::error('Error', $validator->errors()->first());

			return redirect()->back();
        }

        $data = $this->sectionService->saveSection($request);

        if ($data['success']) {			
			Alert::success('Success', 'Data berhasil ditambah');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}

	public function destroy($id)
	{
		$data = $this->sectionService->deleteSection($id);

		if ($data['success']) {			
			Alert::success('Success', 'Data berhasil dihapus');
		} else {
			Alert::error('Error', $data['message']);
		}

		return redirect()->back();
	}
}