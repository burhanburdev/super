<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Http\Traits\PayloadJWT;
use App\Http\Controllers\Controller;
use App\Services\StudentService;

use App\Models\Student;

use App\Helpers\Consumer;

use URL;
use Exception;

class StudentController extends Controller
{
	use PayloadJWT;

	protected $studentService;

	public function __construct(StudentService $studentService)
	{
		$this->url = URL::current();
		$this->studentService = $studentService;
	}

    public function index()
    {
		$response = initResponse($this->url);
		$code = self::SERVER_ERROR;

		try {			
	    	$data = $this->studentService->getAllStudents();

	    	if ($data) {
	    		$response = [
	    			'success' => true,
	    			'message' => 'get all students',
	    			'data' => $data,
	    			'url' => $this->url
	    		];

	    		$code = self::SUCCESS;
	    	}
		} catch (Exception $ex) {
	    	$response = errorResponse($ex, $this->url);
		}

    	return response()->json($response, $code);
    }
	
	public function show()
    {
    	$nim = $this->nim();
		$response = initResponse($this->url);
		$code = self::SERVER_ERROR;

		try {
			$data = $this->studentService->getStudentByNim($nim);

			if ($data) {
	    		$response = [
	    			'success' => true,
	    			'message' => 'get detail student',
	    			'data' => $data,
	    			'url' => $this->url
	    		];

	    		$code = self::SUCCESS;
	    	}
		} catch (Exception $ex) {
	    	$response = errorResponse($ex, $this->url);
		}

        // $student = Student::where('nim', $nim)->first();
        // $leaderboard = Student::orderByRaw('total_xp DESC, total_crown DESC')->get()->toArray();
        // $rankPosition =  array_search($nim, array_column($leaderboard, 'nim'));
        // $student['rank_position'] = $rankPosition + 1;
        // $now = date('Y-m-d');

        // if ($now) {
        //     $dailyResult = DailyXp::where('nim', $nim)->first();

        //     if ($dailyResult) {
        //         if ($dailyResult->date == $now) {
        //             $student['daily_xp'] = $dailyResult->xp;
        //         } else {
        //             DailyXp::find($dailyResult->id)->update([
        //                 'xp' => 0,
        //                 'date' => $now
        //             ]);
        //             $student['daily_xp'] = 0;
        //         }
        //     } else {
        //         $student['daily_xp'] = 0;
        //     }
        // } else {
        //     $student['daily_xp'] = 0;
        // }

    	return response()->json($response, $code);
    }
}