<?php

namespace App\Http\Controllers;

use App\Models\Subsection;
use Illuminate\Http\Request;

class SubsectionController extends Controller
{
    public function index()
    {
        $Subsection = Subsection::with('exercises.questions')->get();
        return response([
            'success' => true,
            'message' => 'List Subsection',
            'data' => $Subsection
        ], 200);
    }

    public function show($id)
    {
        $Subsection = Subsection::where('id', $id)->get();
        return response([
            'success' => true,
            'message' => 'Subsection Detail',
            'data' => $Subsection
        ], 200);
    }

    public function showBySectionId($id)
    {
        $Section = Subsection::with('exercises.questions')->where('section_id', $id)->get();
        
        return response([
            'success' => true,
            'message' => 'Detail Section',
            'data' => $Section
        ], 200);
    }

    public function store(Request $request)
    {
        $Subsection = Subsection::create([
            'title' => $request->title,
            'section_id' => $request->section_id,
        ]);

        if ($Subsection) {
            return response([
                'success' => true,
                'message' => 'Subsection saved',
                'data' => $Subsection
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Subsection failed to save'
            ], 200);
        }
    }

    public function update(Request $request)
    {
        $Subsection = Subsection::find($request->id_topic)->update([
            'title' => $request->title,
            'section_id' => $request->section_id,
        ]);

        if ($Subsection) {
            return response([
                'success' => true,
                'message' => 'Subsection updated',
                'data' => $Subsection
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Subsection failed to update'
            ], 200);
        }
    }

    public function destroy($id)
    {
        $Subsection = Subsection::where('id', $id)->delete();

        if ($Subsection) {
            return response([
                'success' => true,
                'message' => 'Subsection deleted',
                'data' => $Subsection
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Subsection failed to updated'
            ], 200);
        }
    }
}
