<?php

namespace App\Http\Controllers;

use App\Models\Tournaments;
use App\Models\Question;
use App\Models\Exercise;
use Illuminate\Http\Request;
use DB;

class QuestionController extends Controller
{
    public function index()
    {
        $Question = Question::all();
        return response([
            'success' => true,
            'message' => 'List Question',
            'data' => $Question
        ], 200);
    }

    public function show($id)
    {
        $Question = Question::where('id', $id)->get();
        return response([
            'success' => true,
            'message' => 'Detail Question',
            'data' => $Question
        ], 200);
    }

    public function showByExerciseId($id)
    {
        $questions = Question::with('subquestions')->where('exercise_id', $id)
        ->where(function($query) { 
            $query->whereNull('is_group')->orWhere('is_group', false); 
        })
        ->get()
        ->toArray();
        
        shuffle($questions);

        $exercise = Exercise::find($id);
        $result = array_slice($questions, 0, @$exercise->question);
        
        return response([
            'success' => true,
            'message' => 'List Question',
            'data' => $result
        ], 200);
    }

    public function showByTournamentId($id)
    {
        $questions = Question::with('subquestions')->where(function($q) {
            $q->where('is_group', true)->orWhere('level', 3);
        })
        ->whereNotIn('id', function($query) use ($id) {
            $query->select('question_id')->from('exclude_riddle')->where('tournament_id', $id);
        })
        ->get()
        ->toArray();

        $tourney = Tournaments::select('question')->find($id);
        
        shuffle($questions);

        $result = array_slice($questions, 0, @$tourney->question);
        
        return response([
            'success' => true,
            'message' => 'List tourney question',
            'data' => $result
        ], 200);
    }

    public function store(Request $request)
    {
        $Question = Question::create([
            'topic_title' => $request->topic_title,
            'topic_category' => $request->topic_category,
            'mata_kuliah' => $request->mata_kuliah,
        ]);

        if ($Question) {
            return response([
                'success' => true,
                'message' => 'Item berhasil disimpan',
                'data' => $Question
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal disimpan'
            ], 200);
        }
    }

    public function update(Request $request)
    {
        $Question = Question::find($request->id_topic)->update([
            'topic_title' => $request->topic_title,
            'topic_category' => $request->topic_category,
            'mata_kuliah' => $request->mata_kuliah,
        ]);

        if ($Question) {
            return response([
                'success' => true,
                'message' => 'Item berhasil diupdate',
                'data' => $Question
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal diupdate'
            ], 200);
        }
    }

    public function destroy($id)
    {
        $Question = Question::where('id_topic', $id)->delete();

        if ($Question) {
            return response([
                'success' => true,
                'message' => 'Item berhasil didelete',
                'data' => $Question
            ], 200);
        } else {
            return response([
                'success' => false,
                'message' => 'Item gagal didelete'
            ], 200);
        }
    }
}
