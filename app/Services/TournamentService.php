<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\Models\Tournaments;
use App\Models\TournamentGroup;

use File;
use Exception;

class TournamentService 
{
	public function getAllTournaments()
	{
		return Tournaments::getAllTournaments();
	}

	public function getTournamentById($id)
	{
		return Tournaments::getTournamentById($id);
	}

	public function saveTournament($request)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			if ($request->is_active == 'on') {
				$request->is_active = 1;
			} else {
				$request->is_active = 0;
			}

			$data = Tournaments::saveTournament($request);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function updateTournament($request, $id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			if ($request->is_active == 'on') {
				$request->is_active = 1;
			} else {
				$request->is_active = 0;
			}

			$data = Tournaments::updateTournament($request, $id);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function deleteTournament($id)
	{
		return Tournaments::deleteTournament($id);
	}

	public function deleteTournamentGroup($id)
	{
		return TournamentGroup::find($id)->delete();
	}

	public function updateStatusActive($id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$data = Tournaments::updateStatusActive($id);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function addGroupTournament($request)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			foreach ($request->group as $value) {
				$data = new TournamentGroup;
				$data->tournament_id = $request->tournament_id;
				$data->group_id = $value;
				$data->total_point = 0;
				$data->place = null;

				$data->save();
			}

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}
}