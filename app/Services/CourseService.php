<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\Models\Course;
use App\Models\Section;
use App\Models\Subsection;

use File;
use Exception;

class CourseService 
{
	public function getAllCourse()
	{
		return Course::getAllCourse();
	}

	public function getCourseById($id)
	{
		return Course::getCourseById($id);
	}

	public function saveCourse($request)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$data = Course::saveCourse($request);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function updateCourse($request, $id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$data = Course::updateCourse($request, $id);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function deleteCourse($id)
	{
		return Course::deleteCourse($id);
	}
}