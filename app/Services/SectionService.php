<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\Models\Section;
use App\Models\Exercise;

use File;
use Exception;

class SectionService 
{
	public function editSection($id)
	{		
		return Section::find($id);
	}

	public function saveSection($request) 
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$data = Section::create([
				'course_id' => $request->course_id,
				'title' => $request->title,
				'level' => $request->level
			]);

			$exercise = $request->exercise_title;
			$level = $request->exercise_level;
			$question = $request->exercise_question;

			// store exercise
			$i = 0;
			for ($i=0; $i < count((array) $exercise) ; $i++) {
				Exercise::create([
					'section_id' => $data->id,
					'title' => $exercise[$i],
					'level' => $level[$i],
					'question' => $question[$i],
				]);
			}

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function updateSection($request, $id) 
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$data = Section::find($id);

			$data->title = $request->title;
			$data->level = $request->level;
			$data->save();

			$u_id = $request->u_exercise_id;
			$u_exercise = $request->u_exercise_title;
			$u_level = $request->u_exercise_level;
			$u_question = $request->u_exercise_question;

			// update exercise
			$j = 0;
			for ($j=0; $j < count((array) $u_id) ; $j++) { 
				$exe = Exercise::find($u_id[$j]);
				$exe->title = $u_exercise[$j];
				$exe->level = $u_level[$j];
				$exe->question = $u_question[$j];
				$exe->save();
			}

			$exercise = $request->exercise_title;
			$level = $request->exercise_level;
			$question = $request->exercise_question;

			// store exercise
			$i = 0;
			for ($i=0; $i < count((array) $exercise) ; $i++) {
				Exercise::create([
					'section_id' => $id,
					'title' => $exercise[$i],
					'level' => $level[$i],
					'question' => $question[$i],
				]);
			}

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function deleteSection($id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			Exercise::where('section_id', $id)->delete();
			$data = Section::find($id)->delete();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}
}