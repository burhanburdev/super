<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\Models\Student;
use App\Models\UserLogs;

use File;
use Exception;

class StudentService 
{
	public function getAllStudents()
	{
		return Student::getAllStudents();
	}

	public function getStudentByNim($nim)
	{
		return Student::getStudentByNim($nim);
	}

	public function saveStudent($request)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$resultFiles = $request->file('avatar');

			if ($resultFiles) {
				$path = Storage::put(
		            'public/photo',
		            $request->file('avatar')
		        );

		        $pathName = basename($path);
		        $resultFiles->move(public_path('/storage/photo'), $pathName);

		        $request->avatar = $pathName;
			}

	        $request->is_sso = false;

			$data = Student::saveStudent($request);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();
			
			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function updateStudent($request, $nim)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$resultFiles = $request->file('avatar');

			if ($resultFiles) {
				$path = Storage::put(
		            'public/photo',
		            $request->file('avatar')
		        );

		        $pathName = basename($path);
		        $resultFiles->move(public_path('/storage/photo'), $pathName);

		        Student::updatePhoto($pathName, $nim);
			}

	        $request->is_sso = false;

			$data = Student::updateStudent($request, $nim);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function updatePassword($request, $nim)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$data = Student::updatePassword($request, $nim);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function deleteStudent($id)
	{
		return Student::deleteStudent($id);
	}
}