<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\Models\Groups;
use App\Models\MemberGroup;
use App\Models\MemberPoint;
use App\Models\Tournaments;
use App\Models\TournamentGroup;
use App\Models\LeaveLogs;

use File;
use Exception;

class GroupService 
{
	public function getAllGroups()
	{
		$returnValue = [];

		try {
          //   $data = DB::table('groups as g')
		        // ->join('tournament_group as tg', 'tg.group_id', '=', 'g.id')
		        // ->join('tournaments as tn', 'tn.id' ,'=', 'tg.tournament_id')
		        // ->select('g.*', 'tg.total_point')
		        // ->where(array('tn.is_active' => true))
		        // ->get();

			$data = Groups::all();

			$returnValue = ['success' => true, 'data' => $data];
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage()];			
		}

		return $returnValue;
	}

	public function getGroupById($id)
	{
		$returnValue = [];

		try {
            $data = Groups::getGroupById($id);

			$returnValue = ['success' => true, 'data' => $data];
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage()];			
		}

		return $returnValue;
	}

	public function getGroupInActiveTournamentById($id)
	{
		$returnValue = ['success' => false, 'message' => ''];

		try {
            $data = DB::table('groups as g')
		        ->join('tournament_group as tg', 'tg.group_id', '=', 'g.id')
		        ->join('tournaments as tn', 'tn.id' ,'=', 'tg.tournament_id')
		        ->select('g.*', 'tg.total_point')
		        ->where(array('tn.is_active' => true, 'g.id' => $id))
		        ->first();

			$returnValue = ['success' => true, 'data' => $data];
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage()];			
		}

		return $returnValue;
	}

	public function getMemberGroup($id)
	{
		$returnValue = [];

		try {
            $data = MemberGroup::where('group_id', $id)->get();

			$returnValue = ['success' => true, 'data' => $data];
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage()];			
		}

		return $returnValue;
	}

	public function saveGroup($request)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {			
            $activeTournament = Tournaments::where(array('is_active' => true))->first();

            if (!$activeTournament) {
            	throw new Exception("Tidak dapat membuat grup karena tidak ada turnamen yang aktif saat ini", 1);
            }

            // check member grup existing
            $check = DB::table('member_group as mg')
                            ->join('tournament_group as tg', 'tg.group_id', '=', 'mg.group_id')
                            ->where(array('tg.tournament_id' => $activeTournament->id, 'mg.nim' => $request->nim))
                            ->first();

            if ($check) {
            	throw new Exception("Tidak dapat membuat grup karena Anda telah bergabung dengan grup pada turnamen yang aktif saat ini", 1);
            }

            // generate kode grup
            $code = generateRandomString($request->code);
            $request->code = $code;

			$data = Groups::saveGroup($request);

			MemberGroup::create([
                'nim' => $request->nim,
                'group_id' => $data->id,
                'is_leader' => true
            ]);

			TournamentGroup::create([
                'tournament_id' => $activeTournament->id,
                'group_id' => $data->id,
                'total_point' => 0,
                'place' => null
            ]);

            $existMember = MemberPoint::where(array('nim' => $request->nim, 'tournament_id' => $activeTournament->id))->first();

            if ($existMember) {
                $group = TournamentGroup::where(array('group_id' => $data->id, 'tournament_id' => $activeTournament->id))->first();

                TournamentGroup::where(array('group_id' => $data->id, 'tournament_id' => $activeTournament->id))->update([
                    'total_point' => $group->total_point + $existMember->point
                ]);
            } else {
                MemberPoint::create([
                    'tournament_id' => $activeTournament->id,
                    'nim' => $request->nim,
                    'point' => 0
                ]);
            }

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function updateGroup($request, $id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$check = Groups::where('code', $request->code)->first();

			if ($check && $check->id != $id) {
				throw new Exception("Kode grup telah terdaftar", 1);				
			}

			$data = Groups::updateGroup($request, $id);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];
		}

		return $returnValue;
	}

	public function joinGroup()
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
            $data = Groups::where('code', $request->code)->first();
            $activeTournament = Tournaments::where(array('is_active' => true))->first();

            if (!$data && !$activeTournament) {
            	throw new Exception("Grup yang Anda cari tidak ditemukan pada turnamen yang aktif saat ini", 1);
            }

            $hasGroup = false;
            $tourGroup = TournamentGroup::where(array('tournament_id' => $activeTournament->id))->get();

            foreach ($tourGroup as $value) {
                $check = MemberGroup::where(array('nim' => $request->nim, 'group_id' => $value->group_id))->first();

                if ($check) {
                    $hasGroup = true;
                    break;
                }
            }

            if ($hasGroup) {
            	throw new Exception("Tidak dapat bergabung karena Anda telah bergabung dengan grup pada turnamen yang aktif saat ini", 1);
            }

            $countMember = DB::table('member_group as mg')
                                ->join('tournament_group as tg', 'tg.group_id', '=', 'mg.group_id')
                                ->join('tournaments as tn', 'tn.id', '=', 'tg.tournament_id')
                                ->select('mg.*')
                                ->where(array('tn.id' => $activeTournament->id, 'mg.group_id' => $data->id))
                                ->count();

            if ($countMember > 3) {
                throw new Exception("Gagal bergabung dengan grup karena jumlah anggota grup telah berjumlah 3 orang", 1);
            }

            MemberGroup::create([
                'nim' => $request->nim,
                'group_id' => $data->id,
                'is_leader' => false
            ]);

            $existMember = MemberPoint::where(array('nim' => $request->nim, 'tournament_id' => $activeTournament->id))->first();

            if ($existMember) {
                $group = TournamentGroup::where(array('group_id' => $data->id, 'tournament_id' => $activeTournament->id))->first();

                TournamentGroup::where(array('group_id' => $data->id, 'tournament_id' => $activeTournament->id))->update([
                    'total_point' => $group->total_point + $existMember->point
                ]);
            } else {
                MemberPoint::create([
                    'tournament_id' => $activeTournament->id,
                    'nim' => $request->nim,
                    'point' => 0
                ]);
            }

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];			
		}

		return $returnValue;
	}

	public function changeLeader($id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$leader = DB::table('member_group as mg')
            ->join('tournament_group as tg', 'tg.group_id', '=', 'mg.group_id')
            ->join('tournaments as tn', 'tn.id', '=', 'tg.tournament_id')
            ->select('mg.*')
            ->where(array('mg.nim' => $request->nim, 'tn.is_active' => true))
            ->first();

            if (!$leader->is_leader) {
                throw new Exception("Gagal merubah ketua karena Anda bukan ketua grup", 1);                
            }

            MemberGroup::find($leader->id)->update([
                'is_leader' => false,
            ]);

            $data = MemberGroup::find($id)->update([
                'is_leader' => true,
            ]);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];			
		}

		return $returnValue;
	}

	public function checkGroup()
	{
		$returnValue = [];

		try {
            $activeTournament = Tournaments::where(array('is_active' => true))->first();
            $data = DB::table('student as s')
            ->select('s.nim', 'g.id as group_id', 'g.code', 'g.name as group_name')
            ->join('member_group as mg', 'mg.nim', '=', 's.nim')
            ->join('groups as g', 'g.id', '=', 'mg.group_id')
            ->join('tournament_group as tg', 'tg.group_id', '=', 'tg.id')
            ->join('tournaments as t', 't.id', '=', 'tg.tournament_id')
            ->where('t.id', $activeTournament->id)
            ->where('s.nim', $request->nim)
            ->first();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage()];			
		}

		return $returnValue;
	}

	public function leaveGroup($request)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			$member = MemberGroup::find($request->member_group_id);

            if (!$member) {
                throw new Exception("Anggota tidak ditemukan", 1);
            }

            if ($member->is_leader) {
                throw new Exception("Permintaan keluar grup tidak dapat diproses karena Anda adalah ketua grup", 1);
            }

            $data = LeaveLogs::create([
                'member_group_id' => $request->member_group_id,
                'is_approved' => false,
                'request_date' => date('Y-m-d H:i:s'),
                'response_date' => date('Y-m-d H:i:s'),
            ]);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];			
		}

		return $returnValue;
	}

	public function kickMember($id)
	{
		$returnValue = [];
        $losePoint = 0;
        $totalGroupPoint = 0;

		DB::beginTransaction();

		try {
			$data = MemberGroup::where('id', $id)->first();
            $activeTournament = Tournaments::where(array('is_active' => true))->first();

			if (!$data) {
                throw new Exception("Data anggota tidak ditemukan", 1);                
            }

            // if ($data->is_leader) {
            //     throw new Exception("Ketua tidak dapat meninggalkan grup", 1); 
            // }

            $memberPoint = MemberPoint::where(array('nim' => $data->nim, 'tournament_id' => $activeTournament->id))->first();
            $tourGroup = TournamentGroup::where(array('group_id' => $data->group_id, 'tournament_id' => $activeTournament->id))->first();

            if ($memberPoint) {
                $losePoint = $memberPoint->point;
            }

            if ($tourGroup) {
                $totalGroupPoint = $tourGroup->total_point;
            }

            TournamentGroup::where(array('group_id' => $data->group_id, 'tournament_id' => $activeTournament->id))->update([
                'total_point' => $totalGroupPoint - $losePoint,
            ]);

            $delete = MemberGroup::where('id', $id)->delete();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];			
		}

		return $returnValue;
	}

	public function deleteGroup($id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {
			TournamentGroup::where('group_id', $id)
			->whereHas('tournaments', function($query) {
				$query->where('is_active', true);
			})
			->delete();

			$data = Groups::deleteGroup($id);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage()];			
		}

		return $returnValue;
	}
}