<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\Models\Exercise;
use App\Models\Question;
use App\Models\Subquestion;

use File;
use Exception;

class ExerciseService 
{
	public function getExerciseBySubsection($id)
	{
		$returnValue = [];

		try {
			$data = Exercise::where('subsection_id', $id)->get();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function getExerciseById($id)
	{		
		$returnValue = [];

		try {
			$data = Exercise::find($id);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function saveExercise($request)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {			
            $data = Exercise::create([
            	'title' => $request->title,
            	'level' => $request->level,
            	'subsection_id' => $request->subsection_id,
            ]);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function updateExercise($request, $id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {			
            $data = Exercise::find($id);
            $data->title = $request->title;
            $data->level = $request->level;
            $data->save();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function deleteExercise($id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {			
            $data = Exercise::find($id);
            $data->delete();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function getQuestionByExerciseId($id)
	{
		$returnValue = [];

		try {
			$data = Question::where('exercise_id', $id)->get();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function getQuestionById($id)
	{
		$returnValue = [];

		try {
			$data = Question::find($id);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function saveQuestion($request)
	{
		$returnValue = [];
		$path = 'storage/questions';

		DB::beginTransaction();

		try {
			if ($request->is_group == 'on') {
				$request->is_group = 1;
			} else {
				$request->is_group = 0;
			}

			// if ($request->is_active == 'on') {
				$request->is_active = 1;
			// } else {
			// 	$request->is_active = 0;
			// }

			// add question
            $data = new Question;
           	$data->question_text = $request->question_text;
           	$data->exercise_id = $request->exercise_id;
            $data->level = $request->level;
            $data->is_group = $request->is_group;
            $data->is_active = $request->is_active;

			if ($request->file('question_image')) {
                $image = $request->file('question_image');
                $file_image = $image->getClientOriginalName();
                $image->move($path, $file_image);
                $data->question_image = $file_image;
            }

            $data->save();

            // add subquestions
            $question_id = $data->id;
            $subquestions = $request->text;
            $answer = $request->answer;
            $option_1 = $request->option_1;
            $option_2 = $request->option_2;
            $option_3 = $request->option_3;
            $option_4 = $request->option_4;

            for ($i=0; $i < count((array) $subquestions); $i++) {
	            $obj = app();
            	$subquestion = $obj->make('stdClass');

	            $subquestion->text = $subquestions[$i];
	            $subquestion->answer = $answer[$i];
	            $subquestion->option_1 = $option_1[$i];
	            $subquestion->option_2 = $option_2[$i];
	            $subquestion->option_3 = $option_3[$i];
	            $subquestion->option_4 = $option_4[$i];
	            $subquestion->question_id = $question_id;

            	$sub = $this->saveSubquestion($subquestion);

            	if (!$sub['success']) {
            		throw new Exception($sub['message'], 1);            		
            	}
            }

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();

		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function updateQuestion($request, $id)
	{
		$returnValue = [];			
		$path = 'storage/questions';

		DB::beginTransaction();

		try {
			if ($request->is_group == 'on') {
				$request->is_group = 1;
			} else {
				$request->is_group = 0;
			}

			if ($request->is_active == 'on') {
				$request->is_active = 1;
			} else {
				$request->is_active = 0;
			}

			// update question
			$data = Question::find($id);
           	$data->question_text = $request->question_text;
           	$data->exercise_id = $request->exercise_id;
            $data->level = $request->level;
            $data->is_group = $request->is_group;
            $data->is_active = $request->is_active;

			if ($request->file('question_image')) {
                $image = $request->file('question_image');
                $file_image = $image->getClientOriginalName();
                $image->move($path, $file_image);
                $data->question_image = $file_image;
            }

            $data->save();

            $question_id = $id;

            // add subquestions
            $subquestions = $request->text;
            $answer = $request->answer;
            $option_1 = $request->option_1;
            $option_2 = $request->option_2;
            $option_3 = $request->option_3;
            $option_4 = $request->option_4;

            for ($i=0; $i < count((array) $subquestions); $i++) {
	            $obj = app();
            	$subquestion = $obj->make('stdClass');

	            $subquestion->text = $subquestions[$i];
	            $subquestion->answer = $answer[$i];
	            $subquestion->option_1 = $option_1[$i];
	            $subquestion->option_2 = $option_2[$i];
	            $subquestion->option_3 = $option_3[$i];
	            $subquestion->option_4 = $option_4[$i];
	            $subquestion->question_id = $question_id;

            	$sub = $this->saveSubquestion($subquestion);

            	if (!$sub['success']) {
            		throw new Exception($sub['message'], 1);            		
            	}
            }

            // update subquestions
            $u_id = $request->u_id;
            $u_subquestions = $request->u_text;
            $u_answer = $request->u_answer;
            $u_option_1 = $request->u_option_1;
            $u_option_2 = $request->u_option_2;
            $u_option_3 = $request->u_option_3;
            $u_option_4 = $request->u_option_4;

            for ($i=0; $i < count((array) $u_id); $i++) {
	            $u_obj = app();
            	$u_subquestion = $u_obj->make('stdClass');

	            $u_subquestion->text = $u_subquestions[$i];
	            $u_subquestion->answer = $u_answer[$i];
	            $u_subquestion->option_1 = $u_option_1[$i];
	            $u_subquestion->option_2 = $u_option_2[$i];
	            $u_subquestion->option_3 = $u_option_3[$i];
	            $u_subquestion->option_4 = $u_option_4[$i];
	            $u_subquestion->question_id = $question_id;

            	$sub = $this->updateSubquestion($u_subquestion, $u_id[$i]);

            	if (!$sub['success']) {
            		throw new Exception($sub['message'], 1);            		
            	}
            }

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function deleteQuestion($id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {			
            $data = Question::find($id);
            $data->delete();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function deleteSubquestion($id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {			
            $data = Subquestion::find($id);
            $data->delete();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function deleteSubquestionByQuestiodId($questionId)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {			
            $data = Subquestion::where('question_id', $questionId);
            $data->delete();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function getSubquestionByQuestionId($id)
	{
		$returnValue = [];

		try {
			$data = Subquestion::where('question_id', $id)->get();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function getSubquestionById($id)
	{
		$returnValue = [];

		try {
			$data = Subquestion::find($id);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}
		} catch (Exception $ex) {
			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function saveSubquestion($request)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {			
            $data = Subquestion::create([
            	'text' => $request->text,
            	'option_1' => $request->option_1,
            	'option_2' => $request->option_2,
            	'option_3' => $request->option_3,
            	'option_4' => $request->option_4,
            	'answer' => $request->answer,
            	'question_id' => $request->question_id
            ]);

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}
		
		return $returnValue;
	}

	public function updateSubquestion($request, $id)
	{
		$returnValue = [];

		DB::beginTransaction();

		try {			
            $data = Subquestion::find($id);
            $data->text = $request->text;
            $data->option_1 = $request->option_1;
            $data->option_2 = $request->option_2;
            $data->option_3 = $request->option_3;
            $data->option_4 = $request->option_4;
            $data->answer = $request->answer;
            $data->question_id = $request->question_id;
            $data->save();

			if ($data) {
				$returnValue = ['success' => true, 'data' => $data];
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();

			$returnValue = ['success' => false, 'message' => $ex->getMessage().' in file '.$ex->getFile().' at line '.$ex->getLine()];
		}

		return $returnValue;
	}

	public function storeLog(Request $request)
    {
        $Exercise = AnswerLogs::create([
            'nim' => $this->nim,
            'subquestion_id' => $request->subquestion_id,
            'answer' => $request->answer,
        ]);
    }
}