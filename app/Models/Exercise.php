<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    use HasFactory;
    protected $table = 'exercise';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'level',
        'section_id',
        'question'
    ];

    public function questions()
    {
        return $this->hasMany(Question::class, 'exercise_id');
    }

    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id');
    }
}
