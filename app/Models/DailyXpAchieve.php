<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyXpAchieve extends Model
{
    use HasFactory;
    protected $table = 'daily_xp_achieve';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'nim',
        'count',
        'date_updated'
    ];
}
