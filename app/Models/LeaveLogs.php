<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveLogs extends Model
{
    use HasFactory;
    protected $table = 'leave_logs';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'member_group_id',
        'nim',
        'group_id',
        'status',
        'request_date',
        'response_date',
    ];

    public function member()
    {
        return $this->belongsTo(MemberGroup::class, 'member_group_id');
    }
}
