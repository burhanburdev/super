<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLogs extends Model
{
    use HasFactory;
    protected $table = 'user_logs';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'nim',
        'date',
        'time',
        'action'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'nim');
    }
}
