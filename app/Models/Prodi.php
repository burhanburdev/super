<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    use HasFactory;

    protected $table = 'prodi';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'prodi',
    ];

    public function minat1()
    {
    	return $this->hasMany(Student::class, 'minat_1');
    }

    public function minat2()
    {
    	return $this->hasMany(Student::class, 'minat_2');
    }
}
