<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tournaments extends Model
{
    use HasFactory;    

    protected $table = 'tournaments';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'start_date',
        'end_date',
        'is_active',
        'question',
    ];

    public function tournamentPeriod()
    {
        return $this->hasMany(TournamentPeriod::class, 'tournament_id');
    }

    public function tournamentGroup()
    {
        return $this->hasMany(TournamentGroup::class, 'tournament_id');
    }

    public function memberPoint()
    {
        return $this->hasMany(MemberPoint::class, 'tournament_id');
    }

    public function excludeRiddle()
    {
        return $this->hasMany(ExcludeRiddle::class, 'tournament_id');
    }

    public static function getAllTournaments()
    {
        return self::orderBy('start_date', 'DESC')->get();
    }

    public static function getTournamentById($id)
    {
        return self::find($id);
    }

    public static function saveTournament($request)
    {
        $data = new Tournaments;        

        $data->title = $request->title;
        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;
        $data->is_active = $request->is_active;
        $data->question = $request->question;

        $data->save();

        return $data;
    }

    public static function updateTournament($request, $id)
    {
        $data = Tournaments::find($id);

        $data->title = $request->title;
        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;
        $data->is_active = $request->is_active;
        $data->question = $request->question;

        $data->save();

        return $data;
    }

    public static function updateStatusActive($id)
    {
        $data = Tournaments::find($id);

        if ($data->is_active) {
            $data->is_active = false;
        } else {
            $data->is_active = true;
        }

        $data->save();

        return $data;
    }

    public static function deleteTournament($id)
    {
        $data = Tournaments::find($id);

        $data->delete();

        return $data;
    }
}
