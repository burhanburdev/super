<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prestasi extends Model
{
    use HasFactory;

    protected $table = 'prestasi';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'nim',
        'prestasi',
    ];

    public function student()
    {
    	return $this->belongsTo(Student::class, 'nim');
    }
}
