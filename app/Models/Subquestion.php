<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subquestion extends Model
{
    use HasFactory;
    protected $table = 'subquestion';
    protected $primaryKey = 'id';
    // public $timestamps = false;

    protected $fillable = [
        'text',
        'option_1',
        'option_2',
        'option_3',
        'option_4',
        'answer',
        'question_id'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }
}
