<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $table = 'question';
    protected $primaryKey = 'id';
    // public $timestamps = false;

    protected $fillable = [
        'question_code',
        'question_text',
        'question_image',
        'exercise_id',
        'level',
        'is_group',
        'is_active'
    ];

    public function subquestions()
    {
        return $this->hasMany(Subquestion::class, 'question_id');
    }

    public function exercise()
    {
        return $this->belongsTo(Exercise::class, 'exercise_id');
    }

    public function excludeRiddle()
    {
        return $this->hasMany(ExcludeRiddle::class, 'question_id');
    }
}
