<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

use Tymon\JWTAuth\Contracts\JWTSubject;

class Student extends Authenticatable implements JWTSubject
{
    use HasFactory;

    protected $table = 'student';
    protected $primaryKey = 'nim';
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'nim',
        'nama',
        'name',
        'username',
        'email',
        'password',
        'class_code',
        'total_xp',
        'daily_goal',
        'total_crown',
        'avatar',
        'is_sso',
        'program_studi',
        'asal_sekolah',
        'no_telp',
        'minat_1',
        'minat_2',
    ];

    protected $hidden = [
        'password', 'salt'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function memberGroup()
    {
        return $this->hasMany(MemberGroup::class, 'nim');
    }

    public function prestasi()
    {
        return $this->hasMany(Prestasi::class, 'nim');
    }

    public function jurusan()
    {
        return $this->belongsTo(Prodi::class, 'program_studi');
    }

    public function prodi1()
    {
        return $this->belongsTo(Prodi::class, 'minat_1');
    }

    public function prodi2()
    {
        return $this->belongsTo(Prodi::class, 'minat_2');
    }

    public static function getAllStudents()
    {
        return self::orderBy('nim', 'ASC')->get();
    }

    public static function getStudentByNim($nim)
    {
        return self::where('nim', $nim)->first();
    }

    public static function getStudentByUsername($username)
    {
        return self::where('username', $username)->first();
    }

    public static function saveStudent($request)
    {
        $data = new Student;

        $data->nama = $request->nama;
        $data->nim = $request->nim;
        $data->username = $this->generateUsername($request->nama);
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->class_code = $request->class_code;
        $data->total_xp = $request->total_xp;
        $data->total_crown = $request->total_crown;
        $data->avatar = $request->avatar;
        $data->daily_goal = $request->daily_goal;
        $data->is_sso = $request->is_sso;
        $data->program_studi = $request->program_studi;
        $data->asal_sekolah = $request->asal_sekolah;
        $data->no_telp = $request->no_telp;
        $data->minat_1 = $request->minat_1;
        $data->minat_2 = $request->minat_2;

        $data->save();

        return $data;
    }

    public static function updateStudent($request, $nim)
    {
        $data = Student::find($nim);

        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->class_code = $request->class_code;
        $data->total_xp = $request->total_xp;
        $data->total_crown = $request->total_crown;
        $data->daily_goal = $request->daily_goal;
        $data->is_sso = $request->is_sso;
        $data->program_studi = $request->program_studi;
        $data->asal_sekolah = $request->asal_sekolah;
        $data->no_telp = $request->no_telp;
        $data->minat_1 = $request->minat_1;
        $data->minat_2 = $request->minat_2;

        $data->save();

        return $data;
    }

    public static function updatePassword($request, $nim)
    {
        $data = Student::find($nim);
        $data->password = Hash::make($request->password);

        $data->save();

        return $data;
    }

    public static function updatePhoto($avatar, $nim)
    {
        $data = Student::find($nim);
        $data->avatar = $avatar;

        $data->save();

        return $data;
    }

    public static function deleteStudent($nim)
    {
        $data = Student::find($nim);

        $data->delete();

        return $data;
    }

    public static function generateUsername($nama)
    {
        $temp = explode(' ', $nama);

        $username = $name[0] . '.' . $temp[count($temp) - 1];

        $username = strtolower($username);
        $username = preg_replace('/[^A-Za-z0-9\.]/', '', $username);

        $checkUsername = Student::getStudentByUsername($username);

        if ($checkUsername) {
            $username_with_inc = '';
            $increment = 0;

            while ($checkUsername) {
                $increment++;
                $username_with_inc = $username . str_pad($increment, 2, "0", STR_PAD_LEFT);
                $checkUsername = Student::getStudentByUsername($username_with_inc);
            }

            $username = $username_with_inc;
        }

        return $username;
    }
}
