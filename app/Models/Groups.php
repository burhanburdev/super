<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    use HasFactory;
    protected $table = 'groups';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'code',
        'name',
    ];

    public function memberGroup()
    {
        return $this->hasMany(MemberGroup::class, 'group_id');
    }

    public function tournamentGroup()
    {
        return $this->hasMany(TournamentGroup::class, 'group_id');
    }

    public static function getAllGroups()
    {
        return self::orderBy('id', 'ASC')->get();
    }

    public static function getGroupById($id)
    {
        return self::find($id);
    }

    public static function saveGroup($request)
    {
        $data = new Groups;

        $data->code = $request->code;
        $data->name = $request->name;

        $data->save();

        return $data;
    }

    public static function updateGroup($request, $id)
    {
        $data = Groups::find($id);

        $data->code = $request->code;
        $data->name = $request->name;

        $data->save();

        return $data;
    }

    public static function deleteGroup($id)
    {
        $data = Groups::find($id);

        $data->delete();

        return $data;
    }
}
