<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExerciseAnswerLogs extends Model
{
    use HasFactory;
    
    protected $table = 'exercise_answer_logs';
    protected $primaryKey = 'id';
    // public $timestamps = false;

    protected $fillable = [
        'nim',
        'subquestion_id',
        'answer',
        'point',
    ];

    public function subquestion()
    {
    	return $this->belongsTo(Subquestion::class, 'subquestion_id');
    }
}
