<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TournamentAnswerLogs extends Model
{
    use HasFactory;

    protected $table = 'tournament_answer_logs';
    protected $primaryKey = 'id';
    // public $timestamps = false;

    protected $fillable = [
        'tournament_id',
        'nim',
        'subquestion_id',
        'answer',
        'point',
    ];

    public function tournaments()
    {
        return $this->belongsTo(Tournaments::class, 'tournament_id');
    }

    public function memberGroup()
    {
        return $this->belongsTo(MemberGroup::class, 'member_group_id');
    }

    public function subquestion()
    {
        return $this->belongsTo(Subquestion::class, 'subquestion_id');
    }
}
