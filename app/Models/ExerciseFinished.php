<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExerciseFinished extends Model
{
    use HasFactory;
    protected $table = 'exercise_finished';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'exercise_id',
        'nim',
    ];
}
