<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TournamentPeriod extends Model
{
    use HasFactory;

    protected $table = 'tournament_period';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'tournament_id',
        'start',
        'end',
    ];

    public function tournaments()
    {
        return $this->belongsTo(Tournaments::class, 'tournament_id');
    }

    public function tournamentAnswerLogs()
    {
        return $this->hasMany(TournamentAnswerLogs::class, 'tournament_period_id');
    }
}
