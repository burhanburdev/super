<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberPoint extends Model
{
    use HasFactory;
    
    protected $table = 'member_point';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'tournament_id',
        'nim',
        'point',
    ];

    public function tournaments()
    {
        return $this->belongsTo(Tournaments::class, 'tournament_id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'nim');
    }
}
