<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExcludeRiddle extends Model
{
    use HasFactory;

    protected $table = 'exclude_riddle';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'tournament_id',
        'question_id',
    ];

    public function tournaments()
    {
        return $this->belongsTo(Tournaments::class, 'tournament_id');
    }

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }
}
