<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $table = 'course';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'title'
    ];

    public function sections()
    {
        return $this->hasMany(Section::class, 'course_id');
    }

    public static function getAllCourse()
    {
        return self::orderBy('id', 'ASC')->get();
    }

    public static function getCourseById($id)
    {
        return self::find($id);
    }

    public static function saveCourse($request)
    {
        $data = new Course;

        $data->title = $request->title;

        $data->save();

        return $data;
    }

    public static function updateCourse($request, $id)
    {
        $data = Course::find($id);

        $data->title = $request->title;

        $data->save();

        return $data;
    }

    public static function deleteCourse($id)
    {
        $data = Course::find($id);

        $data->delete();

        return $data;
    }
}
