<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TournamentGroup extends Model
{
    use HasFactory;

    protected $table = 'tournament_group';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'tournament_id',
        'group_id',
        'total_point',
        'place',
    ];

    public function tournaments()
    {
        return $this->belongsTo(Tournaments::class, 'tournament_id');
    }

    public function groups()
    {
        return $this->belongsTo(Groups::class, 'group_id');
    }
}
