<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberGroup extends Model
{
    use HasFactory;
    
    protected $table = 'member_group';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'nim',
        'group_id',
        'is_leader',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'nim');
    }

    public function groups()
    {
        return $this->belongsTo(Groups::class, 'group_id');
    }

    public function tournamentAnswerLogs()
    {
        return $this->hasMany(TournamentAnswerLogs::class, 'member_group_id');
    }

    public function leaveLogs()
    {
        return $this->hasMany(LeaveLogs::class, 'member_group_id');
    }

    public static function saveMemberGroup($nim, $group_id, $is_leader)
    {
        $data = new MemberGroup;

        $data->nim = $nim;
        $data->group_id = $group_id;
        $data->is_leader = $is_leader;

        $data->save();

        return $data;
    }

    public static function updateMemberGroup($request, $id)
    {
        $data = MemberGroup::find($id);

        $data->nim = $request->nim;
        $data->group_id = $request->group_id;
        $data->is_leader = $request->is_leader;

        $data->save();

        return $data;
    }

    public static function deleteMemberGroup($id)
    {
        $data = MemberGroup::find($id);

        $data->delete();

        return $data;
    }
}
