<?php

namespace App\Helpers;

class Endpoint
{
	private static $instance = null;

	public static function getInstance()
	{
		if(self::$instance == null)
		{
			self::$instance = new Endpoint();
		}

		return self::$instance;
	}

	public function getSsoUrl($appEnv) 
	{
		if ($appEnv == 'live') {
            $url = 'https://sso.universitaspertamina.ac.id/api/';
		} else {
			$url = 'http://localhost/pertamina/api/';
		}

		return $url;
	}

	public function ssoLogin($appEnv)
	{
		return $this->getSsoUrl($appEnv).'login';
	}

}