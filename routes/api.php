<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BadgeController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\JawabanController;
use App\Http\Controllers\ProdiController;
use App\Http\Controllers\SoalController;
use App\Http\Controllers\StatusCourseController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\SubsectionController;
use App\Http\Controllers\ExerciseController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\SubquestionController;
use App\Http\Controllers\TournamentController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\UserLogsController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/phpinfo', function(){
    phpinfo();
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('student/login', [AuthController::class, 'login']); // kepake
Route::post('accounts/register', [StudentController::class, 'store']); // kepake

// aktivasi akun via sso
Route::post('accounts/activate/sso', [AuthController::class, 'ssoActivate']);

Route::group(['middleware' => 'auth.jwt'], function () {
	Route::prefix('v2')->group(function() {		
		Route::get('students', [App\Http\Controllers\Api\V2\StudentController::class, 'index'])->name('students'); // 
		Route::get('students/show', [App\Http\Controllers\Api\V2\StudentController::class, 'show'])->name('show.student'); // 
	});

	Route::get('student', [StudentController::class, 'index']); // 
	Route::get('student/show', [StudentController::class, 'show']); // kepake
	Route::get('student/exercise', [StudentController::class, 'getExerciseStatus']); // kepake
	Route::get('leaderboard', [StudentController::class, 'leaderboard']); // kepake

	Route::post('student/daily_xp', [StudentController::class, 'updateDailyXp']); // kepake
	Route::post('student/update', [StudentController::class, 'update']);
	Route::post('student/update/daily', [StudentController::class, 'updateDaily']);
	Route::post('student/update/total_xp', [StudentController::class, 'updateXp']); // kepake
	Route::post('student/update/photo', [StudentController::class, 'changePhoto']);
	Route::post('student/update/username', [StudentController::class, 'updateUsername']);
	Route::post('student/update/crown', [StudentController::class, 'updateCrown']); // kepake
	Route::post('student/login_logs', [StudentController::class, 'storeLoginLog']); // kepake
	Route::post('student/daily_xp_achieved', [StudentController::class, 'updatedDailyXpAchieve']); // kepake
	Route::post('student/update/password_or_avatar', [StudentController::class, 'resetPasswordOrAvatar']); // kepake

	Route::delete('student/delete/{nim?}', [StudentController::class, 'destroy']);

	Route::get('course', [CourseController::class, 'index']); // kepake
	Route::get('course/{id?}', [CourseController::class, 'show']);
	Route::post('course/store', [CourseController::class, 'store']);
	Route::post('course/update', [CourseController::class, 'update']);
	Route::delete('course/{id?}', [CourseController::class, 'destroy']);	

	Route::get('section', [SectionController::class, 'index']);
	Route::get('section/{id?}', [SectionController::class, 'show']);
	Route::get('section/course/{id?}', [SectionController::class, 'showByCourseId']); // kepake
	Route::post('section/store', [SectionController::class, 'store']);
	Route::post('section/update', [SectionController::class, 'update']);
	Route::delete('section/{id?}', [SectionController::class, 'destroy']);

	Route::get('subsection', [SubsectionController::class, 'index']);
	Route::get('subsection/{id?}', [SubsectionController::class, 'show']);
	Route::post('subsection/store', [SubsectionController::class, 'store']);
	Route::post('subsection/update', [SubsectionController::class, 'update']);
	Route::delete('subsection/{id?}', [SubsectionController::class, 'destroy']);
	Route::get('subsection/section/{id?}', [SubsectionController::class, 'showBySectionId']); // kepake

	Route::get('exercise', [ExerciseController::class, 'index']);
	Route::get('exercise/{id?}', [ExerciseController::class, 'show']);
	Route::get('exercise/section/{id?}', [ExerciseController::class, 'showBySectionId']);
	Route::post('exercise/store', [ExerciseController::class, 'store']);
	Route::post('exercise/status', [ExerciseController::class, 'status']); // kepake
	Route::post('exercise/update', [ExerciseController::class, 'update']);
	Route::post('exercise/log', [ExerciseController::class, 'storeLog']); //
	Route::delete('exercise/{id?}', [ExerciseController::class, 'destroy']);
	Route::get('crown', [ExerciseController::class, 'count']); // kepake

	Route::get('question', [QuestionController::class, 'index']);
	Route::get('question/{id?}', [QuestionController::class, 'show']);
	Route::get('question/exercise/{id?}', [QuestionController::class, 'showByExerciseId']); // kepake
	Route::get('question/tournament/{id?}', [QuestionController::class, 'showByTournamentId']); // kepake
	Route::post('question/store', [QuestionController::class, 'store']);
	Route::post('question/update', [QuestionController::class, 'update']);
	Route::delete('question/{id?}', [QuestionController::class, 'destroy']);

	Route::get('subquestion', [SubquestionController::class, 'index']);
	Route::get('subquestion/{id?}', [SubquestionController::class, 'show']);
	Route::post('subquestion/store', [SubquestionController::class, 'store']);
	Route::post('subquestion/update', [SubquestionController::class, 'update']);
	Route::post('subquestion/log', [SubquestionController::class, 'storeLog']); //
	Route::delete('subquestion/{id?}', [SubquestionController::class, 'destroy']);

	Route::get('tournaments', [TournamentController::class, 'index']); //
	Route::get('tournaments/active', [TournamentController::class, 'active']); //
	Route::get('tournaments/{id?}', [TournamentController::class, 'show']); //
	Route::post('tournaments/store', [TournamentController::class, 'store']); //
	Route::post('tournaments/update/{id?}', [TournamentController::class, 'update']); //
	Route::delete('tournaments/{id?}', [TournamentController::class, 'destroy']);

	Route::get('tournaments/period', [TournamentController::class, 'indexPeriod']);
	Route::get('tournaments/period/{id?}', [TournamentController::class, 'showPeriod']);
	Route::post('tournaments/period/store', [TournamentController::class, 'storePeriod']);
	Route::post('tournaments/period/update/{id?}', [TournamentController::class, 'updatePeriod']);
	Route::delete('tournaments/period/{id?}', [TournamentController::class, 'destroyPeriod']);

	Route::get('tournaments/group', [TournamentController::class, 'indexGroup']);
	Route::get('tournaments/group/{id?}', [TournamentController::class, 'showGroup']);
	Route::post('tournaments/group/store', [TournamentController::class, 'storeGroup']); //
	Route::post('tournaments/group/update/{id?}', [TournamentController::class, 'updateGroup']);
	Route::delete('tournaments/group/{id?}', [TournamentController::class, 'destroyGroup']);

	Route::post('tournaments/answer/log', [TournamentController::class, 'storeLog']);
	Route::get('tournaments/group/check/{id?}', [TournamentController::class, 'checkTournamentGroup']); //
	Route::post('points/tournament', [TournamentController::class, 'updateTourneyPoint']); //

	Route::get('groups', [GroupController::class, 'index']); //
	Route::get('groups/{id?}', [GroupController::class, 'show']); //
	Route::post('groups/store', [GroupController::class, 'store']); //
	Route::post('groups/update/{id?}', [GroupController::class, 'update']); //
	Route::post('groups/join', [GroupController::class, 'joinGroup']); //
	Route::delete('groups/{id?}', [GroupController::class, 'destroy']);

	Route::get('groups/member/check', [GroupController::class, 'checkGroup']); //

	Route::get('groups/member', [GroupController::class, 'indexMember']);
	Route::get('groups/member/{id?}', [GroupController::class, 'showMember']);
	Route::post('groups/member/store', [GroupController::class, 'storeMember']);
	Route::post('groups/member/update/{id?}', [GroupController::class, 'updateMember']);
	Route::delete('groups/member/{id?}', [GroupController::class, 'destroyMember']); //
	Route::post('groups/member/leave', [GroupController::class, 'leaveGroup']); //

	Route::get('members/point', [GroupController::class, 'indexPoint']);
	Route::get('members/point/{id?}', [GroupController::class, 'showPoint']);
	Route::post('members/point/store', [GroupController::class, 'storePoint']);
	Route::post('members/point/update/{id?}', [GroupController::class, 'updatePoint']);
	Route::delete('members/point/{id?}', [GroupController::class, 'destroyPoint']);

	Route::post('groups/leader/{id?}', [GroupController::class, 'changeLeader']); //
	Route::get('groups/tournament/{id?}', [GroupController::class, 'showTournament']); //

	Route::get('badges', [BadgeController::class, 'getUserBadgeOwned']); // kepake
	Route::post('badges/check', [BadgeController::class, 'checkStudentBadge']); // kepake

	Route::get('user_logs', [UserLogsController::class, 'index']);
	Route::get('user_logs/{id?}', [UserLogsController::class, 'show']);
	Route::post('user_logs/store', [UserLogsController::class, 'store']); // kepake
	Route::get('user_logs/student', [UserLogsController::class, 'showByNim']);

	Route::post('refresh', [AuthController::class, 'refreshToken']); //
	Route::post('logout', [AuthController::class, 'logout']); //
});








