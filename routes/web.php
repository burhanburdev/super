<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\BadgeController;
use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\ExerciseController;
use App\Http\Controllers\Admin\GroupController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\MemberController;
use App\Http\Controllers\Admin\QuestionController;
use App\Http\Controllers\Admin\SectionController;
use App\Http\Controllers\Admin\StudentController;
use App\Http\Controllers\Admin\TournamentController;
use App\Http\Controllers\Admin\ActivityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
	Route::get('/', [HomeController::class, 'index']);
	Route::get('home', [HomeController::class, 'index'])->name('home');
	Route::get('badges', [BadgeController::class, 'index'])->name('badges');

	// mata kuliah
	Route::get('courses', [CourseController::class, 'index'])->name('courses');
	Route::get('courses/{id?}', [CourseController::class, 'show'])->name('show.course');
	Route::get('courses/edit/{id?}', [CourseController::class, 'edit'])->name('edit.course');
	Route::post('courses', [CourseController::class, 'store'])->name('store.course');
	Route::put('courses/{id?}', [CourseController::class, 'update'])->name('update.course');
	Route::delete('courses/{id?}', [CourseController::class, 'destroy'])->name('destroy.course');

	// bab
	Route::get('sections/edit/{id?}', [SectionController::class, 'edit'])->name('edit.section');
	Route::put('sections/{id?}', [SectionController::class, 'update'])->name('update.section');
	Route::post('sections', [SectionController::class, 'store'])->name('store.section');
	Route::delete('sections/{id?}', [SectionController::class, 'destroy'])->name('destroy.section');

	// latihan
	// Route::get('exercises/{subsectionId?}', [ExerciseController::class, 'showBySubsectionId'])->name('exercises');
	Route::get('exercises/question/list/{id?}', [ExerciseController::class, 'show'])->name('show.exercise');
	// Route::get('exercises/edit/{id?}', [ExerciseController::class, 'edit'])->name('edit.exercise');
	// Route::post('exercises', [ExerciseController::class, 'store'])->name('store.exercise');
	// Route::put('exercises/{id?}', [ExerciseController::class, 'update'])->name('update.exercise');
	Route::get('exercises/{id?}', [ExerciseController::class, 'destroy'])->name('destroy.exercise');

	// bank soal berdasarkan latihan
	Route::get('questions/create/{exerciseId?}', [ExerciseController::class, 'createQuestion'])->name('create.question');
	Route::get('questions/edit/{id?}', [ExerciseController::class, 'editQuestion'])->name('edit.question');
	Route::post('questions', [ExerciseController::class, 'storeQuestion'])->name('store.question');
	Route::post('questions/{id?}', [ExerciseController::class, 'duplicateQuestion'])->name('duplicate.question');
	Route::put('questions/{id?}', [ExerciseController::class, 'updateQuestion'])->name('update.question');
	Route::delete('questions/{id?}', [ExerciseController::class, 'destroyQuestion'])->name('destroy.question');
	Route::get('questions/subquestion/{id?}', [ExerciseController::class, 'destroySubQuestion'])->name('destroy.subquestion');

	// bank soal general
	Route::get('questionbank', [QuestionController::class, 'index'])->name('questionbank');
	Route::get('questionbank/create', [QuestionController::class, 'createQuestion'])->name('create.questionbank');
	Route::get('questionbank/{id?}', [QuestionController::class, 'editQuestion'])->name('edit.questionbank');
	Route::post('questionbank/{id?}', [QuestionController::class, 'duplicateQuestion'])->name('duplicate.questionbank');
	Route::post('questionbank', [QuestionController::class, 'storeQuestion'])->name('store.questionbank');
	Route::put('questionbank/{id?}', [QuestionController::class, 'updateQuestion'])->name('update.questionbank');
	Route::delete('questionbank/{id?}', [QuestionController::class, 'deleteQuestion'])->name('delete.questionbank');

	// grup turnamen
	Route::get('groups', [GroupController::class, 'index'])->name('groups');
	Route::get('groups/{id?}', [GroupController::class, 'show'])->name('show.group');
	Route::get('groups/edit/{id?}', [GroupController::class, 'edit'])->name('edit.group');
	Route::post('groups', [GroupController::class, 'store'])->name('store.group');
	Route::put('groups/{id?}', [GroupController::class, 'update'])->name('update.group');
	Route::delete('groups/{id?}', [GroupController::class, 'delete'])->name('destroy.group');

	Route::post('members', [GroupController::class, 'addMember'])->name('add.member');
	Route::delete('members/{id?}', [GroupController::class, 'kickMember'])->name('kick.member');

	// log aktivitas
	Route::get('activities/user', [ActivityController::class, 'index'])->name('activities');
	Route::get('activities/exercise', [ActivityController::class, 'exercise'])->name('exercise.activity');
	Route::get('activities/tournament', [ActivityController::class, 'tournament'])->name('tournament.activity');

	// soal aktif
	Route::get('riddles/tournament/{id?}', [QuestionController::class, 'list'])->name('tournament.riddles');
	Route::post('riddles/tournament/deactive', [QuestionController::class, 'exclude'])->name('exclude.riddle');
	Route::post('riddles/tournament/active', [QuestionController::class, 'include'])->name('include.riddle');

	Route::get('riddles/tournament/active/{id?}', [QuestionController::class, 'active'])->name('ajax.question.active');
	Route::get('riddles/tournament/deactive/{id?}', [QuestionController::class, 'deactive'])->name('ajax.question.deactive');

	// peserta
	Route::get('students', [StudentController::class, 'index'])->name('students');
	Route::get('students/{nim?}', [StudentController::class, 'show'])->name('show.student');
	Route::get('students/edit/{nim?}', [StudentController::class, 'edit'])->name('edit.student');
	Route::get('students/password/{nim?}', [StudentController::class, 'editPassword'])->name('edit.password');
	Route::put('students/{nim?}', [StudentController::class, 'update'])->name('update.student');
	Route::put('students/password/{nim?}', [StudentController::class, 'updatePassword'])->name('update.password');
	Route::post('students', [StudentController::class, 'store'])->name('store.student');
	Route::delete('students/{nim?}', [StudentController::class, 'destroy'])->name('destroy.student');

	// turnamen
	Route::get('tournaments', [TournamentController::class, 'index'])->name('tournaments');
	Route::get('tournaments/{id?}', [TournamentController::class, 'show'])->name('show.tournament');
	Route::get('tournaments/edit/{id?}', [TournamentController::class, 'edit'])->name('edit.tournament');
	Route::post('tournaments', [TournamentController::class, 'store'])->name('store.tournament');
	Route::put('tournaments/{id?}', [TournamentController::class, 'update'])->name('update.tournament');
	Route::put('tournaments/status/{id?}', [TournamentController::class, 'updateStatusActive'])->name('update.tournament.status');
	Route::delete('tournaments/{id?}', [TournamentController::class, 'delete'])->name('destroy.tournament');

	Route::post('tournaments/group', [TournamentController::class, 'storeGroup'])->name('store.group.tournament');
	Route::delete('tournaments/group/delete/{id?}', [TournamentController::class, 'deleteGroup'])->name('delete.group.tournament');

	// leave log

	Route::get('leaves', [MemberController::class, 'index'])->name('leaves');
	Route::post('leaves/approval', [MemberController::class, 'approval'])->name('leaves.approval');

	// membuat kode grup
	Route::get('/generate', function () {
		$data = generateRandomString();

		return $data;
	})->name('generate.string');
});